#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

$skip_plural_search=[162,659,660,661,519,662,663,664,665,666,667,668,669,474,633,670,671,672] ;

if ( !isset ( $argv[1] ) ) {
	print "Needs argument : catalog_id\n" ;
	exit ( 0 ) ;
}

$catalog = $argv[1] ;
$main_language_only = false ;

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$mnm = new MixNMatch ;

function getSearch ( $query ) {
	return json_decode ( file_get_contents ( "http://www.wikidata.org/w/api.php?action=query&list=search&format=json&srsearch=" . urlencode ( $query ) ) ) ;
}


#print "Processing catalog $catalog\n" ;

$main_language = 'en' ; // Fallback

$found = false ;
$sql = "SELECT * FROM catalog WHERE id=$catalog" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	if ( $main_language_only and $o->search_wp != '' ) $main_language = $o->search_wp ;
	if ( $o->active != 1 ) exit ( 0 ) ;
	$found = true ;
}
if ( !$found ) exit ( 0 ) ; // Catalog does not exist


$types = array() ;
$name2ids = array() ;
$sql = "SELECT id,ext_name AS name,`type` FROM entry WHERE catalog=$catalog and q IS NULL" ;
$sql .= " AND NOT EXISTS (SELECT * FROM `log` WHERE log.entry=entry.id AND log.action='remove_q')" ;
#$sql .= " AND `id`=52418152" ; # TESTING
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$n = trim($o->name) ;
	$n = preg_replace ( '/^([Ss]ir|[Ll]ady|[Dd]ame) /' , '' , $n ) ;
	if ( $catalog == 1151 and preg_match ( '/^(\S+) (\S+)$/' , $name , $m ) ) $name = $m[2] . ' ' . $m[1] ; # HARDCODED japanese names, "last first" => "first last"
	$name2ids[$n][] = $o->id ;
	
	if ( preg_match ( '/-/' , $n ) ) $name2ids[str_replace('-',' ',$n)][] = $o->id ;
	if ( preg_match ( '/\(/' , $n ) ) $name2ids[preg_replace('/\s+\(.*?\)/','',$n)][] = $o->id ; // ()

	if ( preg_match ( '/\s*[A-Z]\.{0,1}\s+/' , $n ) ) {
		$name2ids[trim(preg_replace('/\s*[A-Z]\.{0,1}\s+/',' ',$n))][] = $o->id ; // Single letter
#		print "$name\t" . trim(preg_replace('/\s*[A-Z]\.{0,1}\s+/',' ',$n)) . "\n" ; exit(0);
	}

	
	if ( $o->type != 'Q5' and preg_match ( '/^(.+)s$/' , $n , $m ) and !in_array($catalog,$skip_plural_search) ) { // Plural => singular, for non-people
		$n2 = $m[1] ;
		if ( preg_match ( '/^(.+)ies$/' , $n , $m ) ) $n2 = $m[1].'y' ;
		$name2ids[$n2][] = $o->id ;
		if ( preg_match ( '/-/' , $n2 ) ) $name2ids[str_replace('-',' ',$n2)][] = $o->id ;
	}
	
	if ( $o->type != '' ) $types[$o->type]++ ;
}


$sql = "SELECT limiter FROM catalog WHERE id=$catalog" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	if ( trim($o->limiter) == '' ) break ;
	$j = json_decode ( $o->limiter ) ;
	foreach ( $j AS $type ) $types[$type]++ ;
}

$linkto = array() ;

if ( $argv[2] != 'notype' ) {
	$types = array_keys($types) ;
	if ( count($types) == 1 and array_pop(array_values()) == 'Q5' ) {
		$linkto[] = 'Q5' ;
	} else if ( count($types) > 0 ) {
	#	print_r ( $types ) ;
		$sparql = "SELECT ?q { VALUES ?types { wd:" . implode ( ' wd:' , $types ) . " } ?q wdt:P279* ?types }" ;
	#	print "$sparql\n" ;
		$items = getSPARQLitems ( $sparql ) ;
		foreach ( $items AS $i ) $linkto[] = "Q$i" ;
	#	print_r ( $linkto ) ;
	}
}

$dbwd = openDB ( 'wikidata' , true ) ;
$names = array() ;
foreach ( $name2ids AS $k => $v ) {
	$names[] = $dbwd->real_escape_string ( $k ) ;
}
if ( count($names) == 0 ) exit(0) ;


$multimatch = array() ;
while ( count($names) > 0 ) {
	$candidates = array() ;
	$names2 = array() ;
	while ( count($names) > 0 and count($names2) < 100 ) $names2[] = array_pop ( $names ) ;
	$dbwd = openDB ( 'wikidata' , true ) ;
	$sql = "SELECT term_text AS name,term_entity_id AS q FROM wb_terms AS terms WHERE " ; // ,count(DISTINCT term_entity_id) AS cnt
	$sql .= " term_type IN ('label','alias') and term_text IN ('" . implode("','",$names2) . "') and term_entity_type='item'" ;
	if ( $main_language_only ) $sql .= " AND term_language='" . $dbwd->real_escape_string ( $main_language ) . "' " ;
/*	
	if ( count($linkto) > 0 ) {
		$sql .= " AND term_entity_id IN (select epp_entity_id from wb_entity_per_page,pagelinks WHERE pl_from=epp_page_id AND epp_entity_type='item' AND pl_namespace=0 AND pl_title IN ( '".implode("','",$linkto)."' ))" ;
	}
*/	
	$sql .= " group by name,q" ;
#	$sql .= " HAVING cnt=1" ;
#	print "$sql\n" ; exit(0);

	
	$name2q = array() ;
	$qlist = array() ;
	$result = getSQL ( $dbwd , $sql ) ;
	while($o = $result->fetch_object()){
		$name2q[$o->name][$o->q] = $o->q ;
		$qlist[$o->q] = $o->q ;
	}
	
	if ( count($qlist) == 0 ) continue ; // Nothing to do
	
	// Filter disambiguation candidates
	$sql = "SELECT DISTINCT page_title FROM page,pagelinks WHERE page_namespace=0 AND page_title IN ('Q" . implode("','Q",$qlist) . "') AND pl_from=page_id AND pl_title IN ('Q4167410','Q11266439','Q4167836','Q13406463')" ;
	$result = getSQL ( $dbwd , $sql ) ;
	while($o = $result->fetch_object()){
		$q = preg_replace ( '/\D/' , '' , $o->page_title ) ;
		unset ( $qlist[$q] ) ;
		foreach ( $name2q AS $name => $qs ) {
			if ( !isset($qs[$q]) ) continue ;
			unset ( $name2q[$name][$q] ) ;
			if ( count($name2q[$name]) == 0 ) unset ( $name2q[$name] ) ;
		}
	}
	
	
	if ( count($qlist) == 0 ) continue ; // Nothing to do

	if ( count($linkto) > 0 ) { // Filter by type
		$goodq = array() ;
		$sql = "SELECT DISTINCT page_title FROM page,pagelinks WHERE page_namespace=0 AND page_title IN ('Q" . implode("','Q",$qlist) . "') AND pl_from=page_id AND pl_title IN ( '".implode("','",$linkto)."' )" ;
		$result = getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()){
			$q = preg_replace ( '/^Q/' , '' , $o->page_title ) ;
			$goodq[$q] = $q ;
		}
		
		foreach ( $name2q AS $name => $qs ) {
			if ( !isset($name2ids[$name]) ) continue ;
			$maybe = [] ;
			foreach ( $qs AS $q ) {
				if ( isset($goodq[$q]) ) $maybe[$q] = $q ;
			}
			if ( count($maybe) != 1 ) {
				if ( count($maybe) > 1 ) { foreach ( $name2ids[$name] AS $id ) { foreach($maybe AS $m) $multimatch[$id][$m] = $m ; } }
				continue ;
			}
			foreach ( $maybe AS $q ) {
				foreach ( $name2ids[$name] AS $id ) $candidates[''.$id] = $q ;
			}
		}
	} else {
		foreach ( $name2q AS $name => $qs ) {
			if ( !isset($name2ids[$name]) ) continue ;
			if ( count($qs) != 1 ) {
				foreach ( $name2ids[$name] AS $id ) { foreach($qs AS $m) $multimatch[$id][$m] = $m ; }
				continue ;
			}
			foreach ( $qs AS $q ) {
				foreach ( $name2ids[$name] AS $id ) $candidates[''.$id] = $q ;
			}
		}
	}

	foreach ( $candidates AS $entry => $q ) {
#		print "$entry=>$q\n" ;
		$mnm->setMatchForEntryID ( $entry , $q , 0 , true , false ) ;
	}

	
}


# Multimatch
#$sql = "DELETE FROM multi_match WHERE catalog=$catalog" ;
#$mnm->getSQL ( $sql ) ;
foreach ( $multimatch AS $entry => $list ) {
	if ( count($list) >= 10 ) continue ; # Too many to be useful
	$sql = "INSERT IGNORE INTO multi_match (entry_id,catalog,candidates,candidate_count) VALUES ($entry,$catalog,'" . implode(',',$list) . "'," . count($list) . ")" ;
	$mnm->getSQL ( $sql ) ;
}


# Unnecessary, but just in case...
$mnm->updateSingleCatalog ( $catalog ) ;

?>
