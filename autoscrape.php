#!/usr/bin/php
<?PHP

ini_set('memory_limit','3500M');
require_once ( '/data/project/mix-n-match/autoscrape.inc' ) ;


// __________________________________________________________________________________________________________________________________________________________________



$cmd = $argv[1] ;

$as = new AutoScrape ;

if ( $cmd == 'update_next' ) {
	$catalog = $as->getNextCatalogToUpdate() ;
} else {
	if ( !isset ( $argv[2] ) ) {
		print "Needs arguments : command catalog_id\n" ;
		exit ( 0 ) ;
	}
	$catalog = $argv[2] ;
}

$ok_only = (isset($argv[3]) and $argv[3]=='force')?false:true ;
if ( $cmd == 'test' ) $ok_only = false ;
if ( !$as->loadByCatalog($catalog,$ok_only) ) die ( $as->error."\n" ) ;

$as->max_urls_requested = 1 ;
if ( $cmd == 'test' ) $as->runTest() ;
else if ( $cmd == 'vtest' ) {
	$as->verbose = true ;
	$as->runTest() ;
} else if ( $cmd == 'run' or $cmd == 'update_next' ) {
	$as->scrapeAll() ;
} else print "Unknown command $cmd\n" ;


?>