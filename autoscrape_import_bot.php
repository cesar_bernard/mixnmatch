#!/usr/bin/php
<?PHP

/* Bot start:
jstart -mem 8g -N as_import -cwd -continuous ./autoscrape_import_bot.php
jstart -mem 8g -N as_import -cwd ./autoscrape_import_bot.php
*/

require_once ( 'scripts/mixnmatch.php' ) ;
require_once ( '/data/project/mix-n-match/autoscrape.inc' ) ;


// __________________________________________________________________________________________________________________________________________________________________

function startAutoscrape ( $catalog ) {
	global $running , $mnm ;
	$as = new AutoScrape ;
	if ( !$as->loadByCatalog($catalog) ) {
		$sql = "UPDATE autoscrape SET `status`='IMPORT FAILED [1]' WHERE catalog=$catalog" ;
		$mnm->getSQL ( $sql ) ;
		return ;
	}
	$running = true ;
	if ( !$as->scrapeAll() ) {
		$err = "IMPORT FAILED [2]: " . $mnm->escape ( $as->error ) ;
		$sql = "UPDATE autoscrape SET `status`='$err' WHERE catalog=$catalog" ;
		$mnm->getSQL ( $sql ) ;
	} else { // Set auto update to ON if <3000 URLs scraped
		$sql = "UPDATE autoscrape SET do_auto_update=1 WHERE catalog=$catalog AND `status`='OK' AND last_run_urls<3000 AND do_auto_update=0" ;
		$mnm->getSQL ( $sql ) ;
	}
	$running = false ;
}

$mnm = new MixNMatch () ;

if ( 0 ) { // Continuous

	$running = false ;

	while ( 1 ) {
		sleep ( 5 ) ;
		if ( $running ) continue ;
		$imports = [] ;
		$sql = "SELECT * FROM autoscrape WHERE `status`='IMPORT' LIMIT 1" ;
		$result = $mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $imports[] = $o ;
		if ( count($imports) == 0 ) continue ;
		$o = $imports[0] ;
		startAutoscrape ( $o->catalog ) ;
	}

} else { // One-off

	$imports = [] ;
	$sql = "SELECT * FROM autoscrape WHERE `status`='IMPORT' LIMIT 1" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $imports[] = $o ;
	if ( count($imports) == 0 ) exit(0) ;
	$o = $imports[0] ;
	startAutoscrape ( $o->catalog ) ;

}

?>