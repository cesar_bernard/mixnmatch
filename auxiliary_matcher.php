#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$mnm = new MixNMatch ;

$catalog = $argv[1] ;
#$batch = 10000 ;
#$use_rand = 1 ;


$r = rand()/getrandmax()  ;
$sql = "select entry_id,aux_p,aux_name,catalog,auxiliary.id AS aux_id from entry,auxiliary where entry.id=entry_id and (q is null or user=0)";
if ( $use_rand ) $sql .= " and random>=$r" ;
if ( isset($catalog) ) $sql .= " and catalog IN ($catalog)" ;
$sql .= " AND catalog!=506" ; # Why?
if ( $use_rand ) $sql .= " order by random" ;
if ( isset($batch) ) $sql .= " limit $batch" ;


$results = array() ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$results[] = $o ;
}

$batchsize = 30 ;
$grouped = array() ;
foreach ( $results AS $o ) {
	$p = $o->aux_p ;
	if ( !isset($grouped[$o->catalog]) ) $grouped[$o->catalog] = array () ;
	if ( !isset($grouped[$o->catalog][$p]) ) $grouped[$o->catalog][$p] = array ( array () ) ;
	$lg = count($grouped[$o->catalog][$p])-1 ;
	if ( count($grouped[$o->catalog][$p][$lg]) >= $batchsize ) $grouped[$o->catalog][$p][++$lg] = array() ;
	$grouped[$o->catalog][$p][$lg][] = $o ;
}
#print_r ( $grouped ) ; exit ( 0 ) ;


$catalogs = array() ;
$ts = date ( 'YmdHis' ) ;

foreach ( $grouped AS $catalog => $prop_groups ) {
	foreach ( $prop_groups AS $prop => $groups ) {
		foreach ( $groups AS $group ) {
			$ids = array() ;
			$aux2entry = array() ;
			foreach ( $group AS $o ) {
				if ( preg_match ( '/"/' , $o->aux_name ) ) continue ;
				$ids[] = '"' . trim($o->aux_name) . '"' ;
				$aux2entry[trim($o->aux_name)] = $o->entry_id ;
			}
			$sparql = "SELECT ?q ?id WHERE { VALUES ?id { " . implode(' ',$ids) . " } ?q wdt:P$prop ?id }" ;
			$j = getSPARQL ( $sparql ) ;
			foreach ( $j->results->bindings AS $b ) {
				$id = $b->id->value ;
				$q = preg_replace ( '/^.+\/Q/' , '' , $b->q->value ) ;

				if ( !isset($aux2entry[$id]) ) {
					print "Failed lookup aux2entry for $id\n" ;
					continue ;
				}
				$mnm->setMatchForEntryID ( $aux2entry[$id] , $q , 4 , true , false ) ;
			}
		}
	}

}

foreach ( $grouped AS $catalog => $dummy ) {
	$mnm->updateSingleCatalog ( $catalog ) ;
}

?>