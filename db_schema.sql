# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: tools-db (MySQL 5.5.5-10.0.33-MariaDB)
# Database: s51434__mixnmatch_p
# Generation Time: 2018-02-05 08:45:46 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table autoscrape
# ------------------------------------------------------------

CREATE TABLE `autoscrape` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catalog` int(11) NOT NULL,
  `json` mediumtext NOT NULL,
  `last_run_min` int(11) DEFAULT NULL,
  `last_run_urls` int(11) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT '',
  `owner` int(11) NOT NULL DEFAULT '2',
  `notes` mediumtext NOT NULL,
  `do_auto_update` tinyint(1) NOT NULL,
  `last_update` varchar(14) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `catalog` (`catalog`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auxiliary
# ------------------------------------------------------------

CREATE TABLE `auxiliary` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) unsigned NOT NULL,
  `aux_p` int(10) unsigned NOT NULL,
  `aux_name` varchar(255) NOT NULL DEFAULT '',
  `in_wikidata` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entry_id_2` (`entry_id`,`in_wikidata`),
  KEY `entry_id` (`entry_id`,`aux_p`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table catalog
# ------------------------------------------------------------

CREATE TABLE `catalog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `desc` tinytext,
  `type` varchar(64) NOT NULL DEFAULT '',
  `wd_prop` int(11) DEFAULT NULL,
  `wd_qual` int(11) DEFAULT NULL,
  `search_wp` varchar(16) NOT NULL DEFAULT 'en',
  `autosync` varchar(64) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `betamatch_hint` varchar(64) NOT NULL DEFAULT ' ',
  `owner` int(11) NOT NULL DEFAULT '2',
  `limiter` tinytext NOT NULL,
  `note` tinytext NOT NULL,
  `source_item` int(11) DEFAULT NULL,
  `has_person_date` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `wd_prop` (`wd_prop`,`wd_qual`,`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table common_names
# ------------------------------------------------------------

CREATE TABLE `common_names` (
  `name` varchar(128) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `cnt` bigint(21) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PAGE_CHECKSUM=1;



# Dump of table common_names_human
# ------------------------------------------------------------

CREATE TABLE `common_names_human` (
  `name` varchar(128) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `cnt` bigint(21) NOT NULL DEFAULT '0',
  `entry_ids` varchar(255) NOT NULL DEFAULT '',
  `dates` varchar(20) NOT NULL DEFAULT '',
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table entry
# ------------------------------------------------------------

CREATE TABLE `entry` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catalog` int(10) unsigned NOT NULL,
  `ext_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `ext_url` varchar(255) NOT NULL DEFAULT '',
  `ext_name` varchar(128) NOT NULL DEFAULT '',
  `ext_desc` varchar(255) NOT NULL DEFAULT '',
  `q` int(11) DEFAULT NULL,
  `user` int(10) unsigned DEFAULT NULL,
  `timestamp` varchar(16) DEFAULT NULL,
  `random` float DEFAULT NULL,
  `type` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ext_id` (`ext_id`,`catalog`),
  UNIQUE KEY `catalog` (`catalog`,`ext_id`,`q`),
  KEY `q` (`q`,`user`),
  KEY `timestamp` (`timestamp`),
  KEY `catalog_2` (`catalog`,`q`,`user`),
  KEY `random` (`random`),
  KEY `ext_name` (`ext_name`),
  KEY `type` (`type`),
  FULLTEXT KEY `ft_ext_name` (`ext_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table fast_external
# ------------------------------------------------------------

CREATE TABLE `fast_external` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entry` int(11) NOT NULL,
  `external_id` varchar(64) NOT NULL DEFAULT '',
  `type` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `entry` (`entry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table frs
# ------------------------------------------------------------

CREATE TABLE `frs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `rsid` varchar(64) DEFAULT NULL,
  `formal_name` varchar(255) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rsid` (`rsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table isni
# ------------------------------------------------------------

CREATE TABLE `isni` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `isni` varchar(32) NOT NULL DEFAULT '',
  `name` tinytext NOT NULL,
  `alt_names` tinytext NOT NULL,
  `locality` tinytext NOT NULL,
  `admin_area_level_1_short` tinytext NOT NULL,
  `post_code` tinytext NOT NULL,
  `country_code` tinytext NOT NULL,
  `urls` tinytext NOT NULL,
  `q` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `isni` (`isni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table journals
# ------------------------------------------------------------

CREATE TABLE `journals` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `JournalTitle` varchar(255) DEFAULT NULL,
  `MedAbbr` varchar(255) DEFAULT NULL,
  `ISSN_print` varchar(255) DEFAULT NULL,
  `ISSN_online` varchar(255) DEFAULT NULL,
  `IsoAbbr` varchar(255) DEFAULT NULL,
  `NlmId` varchar(255) DEFAULT NULL,
  `q` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `NlmId` (`NlmId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table kindred
# ------------------------------------------------------------

CREATE TABLE `kindred` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kindred_id` varchar(16) NOT NULL DEFAULT '',
  `has_full` tinyint(1) NOT NULL DEFAULT '0',
  `has_primary` tinyint(1) NOT NULL DEFAULT '0',
  `json` mediumtext NOT NULL,
  `has_error` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `kindred_id` (`kindred_id`),
  KEY `full` (`has_full`),
  KEY `primary_2` (`has_primary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table location
# ------------------------------------------------------------

CREATE TABLE `location` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entry` int(11) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entry` (`entry`),
  KEY `lat` (`lat`,`lon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table log
# ------------------------------------------------------------

CREATE TABLE `log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(16) NOT NULL DEFAULT '',
  `entry` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `timestamp` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `timestamp` (`timestamp`),
  KEY `entry` (`entry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table multi_match
# ------------------------------------------------------------

CREATE TABLE `multi_match` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL,
  `catalog` int(11) NOT NULL,
  `candidates` tinytext NOT NULL,
  `candidate_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entry_id` (`entry_id`),
  KEY `catalog` (`catalog`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table overview
# ------------------------------------------------------------

CREATE TABLE `overview` (
  `catalog` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `total` int(11) NOT NULL DEFAULT '0',
  `noq` int(11) NOT NULL DEFAULT '0',
  `autoq` int(11) NOT NULL DEFAULT '0',
  `na` int(11) NOT NULL DEFAULT '0',
  `manual` int(11) NOT NULL DEFAULT '0',
  `nowd` int(11) NOT NULL DEFAULT '0',
  `multi_match` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`catalog`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table person_dates
# ------------------------------------------------------------

CREATE TABLE `person_dates` (
  `entry_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `born` varchar(10) NOT NULL DEFAULT '',
  `died` varchar(10) NOT NULL DEFAULT '',
  `in_wikidata` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table potential_mismatch
# ------------------------------------------------------------

CREATE TABLE `potential_mismatch` (
  `entry_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `reason` tinytext NOT NULL,
  `q` int(11) NOT NULL,
  PRIMARY KEY (`entry_id`),
  KEY `q` (`q`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table thorough_match_candidates
# ------------------------------------------------------------

CREATE TABLE `thorough_match_candidates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL,
  `random` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entry_id` (`entry_id`),
  KEY `random` (`random`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user
# ------------------------------------------------------------

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tusc_username` varchar(255) NOT NULL DEFAULT '',
  `tusc_wiki` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tusc_username` (`tusc_username`,`tusc_wiki`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_edits
# ------------------------------------------------------------

CREATE TABLE `user_edits` (
   `tusc_username` VARCHAR(255) NOT NULL DEFAULT '',
   `cnt` BIGINT(21) NOT NULL DEFAULT '0'
) ENGINE=MyISAM;



# Dump of table vw_catalogs2sandra
# ------------------------------------------------------------

CREATE TABLE `vw_catalogs2sandra` (
   `id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
   `mnm_url` VARCHAR(59) NOT NULL DEFAULT '',
   `name` VARCHAR(128) NULL DEFAULT NULL,
   `desc` TINYTEXT NULL DEFAULT NULL,
   `wd_prop` INT(11) NULL DEFAULT NULL,
   `wd_qual` INT(11) NULL DEFAULT NULL,
   `type` VARCHAR(64) NOT NULL DEFAULT '',
   `source_q` VARCHAR(12) NULL DEFAULT NULL
) ENGINE=MyISAM;



# Dump of table vw_common_names
# ------------------------------------------------------------

CREATE TABLE `vw_common_names` (
   `name` VARCHAR(128) NOT NULL DEFAULT '',
   `cnt` BIGINT(21) NOT NULL DEFAULT '0'
) ENGINE=MyISAM;



# Dump of table vw_overview
# ------------------------------------------------------------

CREATE TABLE `vw_overview` (
   `catalog` INT(10) UNSIGNED NOT NULL,
   `total` BIGINT(21) NOT NULL DEFAULT '0',
   `noq` DECIMAL(23) NULL DEFAULT NULL,
   `autoq` DECIMAL(23) NULL DEFAULT NULL,
   `na` DECIMAL(23) NULL DEFAULT NULL,
   `manual` DECIMAL(23) NULL DEFAULT NULL,
   `nowd` DECIMAL(23) NULL DEFAULT NULL,
   `multi_match` BIGINT(21) NULL DEFAULT NULL
) ENGINE=MyISAM;





# Replace placeholder table for vw_catalogs2sandra with correct view syntax
# ------------------------------------------------------------

DROP TABLE `vw_catalogs2sandra`;

CREATE ALGORITHM=UNDEFINED DEFINER=`s51434`@`%` SQL SECURITY DEFINER VIEW `vw_catalogs2sandra`
AS SELECT
   `catalog`.`id` AS `id`,concat('https://tools.wmflabs.org/mix-n-match/#/catalog/',`catalog`.`id`) AS `mnm_url`,
   `catalog`.`name` AS `name`,
   `catalog`.`desc` AS `desc`,
   `catalog`.`wd_prop` AS `wd_prop`,
   `catalog`.`wd_qual` AS `wd_qual`,
   `catalog`.`type` AS `type`,concat('Q',`catalog`.`source_item`) AS `source_q`
FROM `catalog` where (`catalog`.`active` = 1) order by `catalog`.`type`,`catalog`.`name`;


# Replace placeholder table for user_edits with correct view syntax
# ------------------------------------------------------------

DROP TABLE `user_edits`;

CREATE ALGORITHM=UNDEFINED DEFINER=`s51434`@`%` SQL SECURITY DEFINER VIEW `user_edits`
AS SELECT
   `user`.`tusc_username` AS `tusc_username`,count(0) AS `cnt`
FROM (`user` join `entry`) where (`entry`.`user` = `user`.`id`) group by `user`.`id` order by count(0) desc;


# Replace placeholder table for vw_overview with correct view syntax
# ------------------------------------------------------------

DROP TABLE `vw_overview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`s51434`@`%` SQL SECURITY DEFINER VIEW `vw_overview`
AS SELECT
   `entry`.`catalog` AS `catalog`,count(0) AS `total`,sum((case when isnull(`entry`.`q`) then 1 else 0 end)) AS `noq`,sum((case when (`entry`.`user` = 0) then 1 else 0 end)) AS `autoq`,sum((case when (`entry`.`q` = 0) then 1 else 0 end)) AS `na`,sum((case when ((`entry`.`q` > 0) and (`entry`.`user` > 0)) then 1 else 0 end)) AS `manual`,sum((case when (`entry`.`q` = -(1)) then 1 else 0 end)) AS `nowd`,(select count(0)
FROM `multi_match` where (`multi_match`.`catalog` = `entry`.`catalog`)) AS `multi_match` from `entry` group by `entry`.`catalog`;


# Replace placeholder table for vw_common_names with correct view syntax
# ------------------------------------------------------------

DROP TABLE `vw_common_names`;

CREATE ALGORITHM=UNDEFINED DEFINER=`s51434`@`%` SQL SECURITY DEFINER VIEW `vw_common_names`
AS SELECT
   `entry`.`ext_name` AS `name`,count(distinct `entry`.`catalog`) AS `cnt`
FROM `entry` where (isnull(`entry`.`q`) and `entry`.`catalog` in (select `catalog`.`id` from `catalog` where (`catalog`.`active` = 1))) group by `entry`.`ext_name` having (`cnt` in (3,4,5,6)) order by count(distinct `entry`.`catalog`) desc,`entry`.`ext_name`;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
