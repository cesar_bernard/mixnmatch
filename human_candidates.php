#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once ( 'public_html/php/common.php' ) ;
require_once ( 'scripts/mixnmatch.php' ) ;

$bad_catalogs = [ 70 ] ;

$mnm = new MixNMatch () ;
$catalogs = array() ;
$sql = "SELECT id FROM catalog WHERE active=1 AND id NOT IN (" . implode(',',$bad_catalogs) . ")" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $catalogs[] = $o->id ;
$catalogs = implode ( ',' , $catalogs ) ;

$data = array() ;
$sql = "SELECT SQL_NO_CACHE id,ext_name,born,died FROM entry,person_dates WHERE q is null AND type='Q5' AND entry_id=entry.id AND catalog IN ($catalogs) AND born!='' AND died!=''" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$name_key = trim(preg_replace('/[-]/',' ',strtolower($o->ext_name))) ;
	if ( !preg_match('/^(\d{3,4})/' , $o->born , $by ) ) continue ;
	if ( !preg_match('/^(\d{3,4})/' , $o->died , $dy ) ) continue ;
	$date_key = $by[1].'-'.$dy[1] ;
	$data[$name_key][$date_key][] = $o->id ;
#	if ( !preg_match ( '/(\d{4}).+(\d{4})/' , $o->ext_desc , $m ) ) continue ;
#	$data[trim(preg_replace('/[-]/',' ',strtolower($o->ext_name)))][$m[1].'-'.$m[2]][] = $o->id ;
}

$sql = "TRUNCATE common_names_human" ;
$result = $mnm->getSQL ( $sql ) ;
foreach ( $data AS $name => $dates ) {
	if ( preg_match ( '/untitled/' , $name ) ) continue ;
//	if ( count ( explode ( ' ' , $name ) ) > 3 ) continue ;
	foreach ( $dates AS $k => $ids ) {
		if ( count($ids) < 4 ) continue ;
		$sql = "INSERT INTO common_names_human (name,cnt,entry_ids,dates) VALUES ('" . $mnm->escape($name) . "',".count($ids).",'".implode(',',$ids)."','" . $mnm->escape($k) . "')" ;
		$mnm->getSQL ( $sql ) ;
	}
}

?>