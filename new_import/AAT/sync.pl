#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

if ( 1 ) {
	`curl -o full.zip http://vocab.getty.edu/dataset/aat/full.zip` ;
#	`unzip full.zip` ;
}

open OUT, "> out.json" or die $! ;
print OUT '{"catalog":48,"entries":' ;
print OUT "[\n" ;

my $first = 1 ;
my $out ;
open IN , 'unzip -p full.zip AATOut_Full.nt |' ;
while ( <IN> ) {
	if ( $_ =~ m|^<(http://vocab.getty.edu/aat/)(\d+)>.*core#prefLabel> "(.+)"\@en .| ) {
		if ( not defined $out->{id} or $2 != $out->{id} ) {
			$out = get_blank() ;
			$out->{id} = $2 ;
			$out->{url} = "$1$2" ;
		}
		$out->{name} = $3 unless defined $out->{name} ;
	} elsif ( $_ =~ m|#parentString> "(.+)" .$| ) {
		$out->{desc} = $1 ;
		if ( defined $out->{id} ) {
			$out->{name} = decode_json ( '"' . $out->{name} . '"' ) ;
			$out->{desc} = decode_json ( '"' . $out->{desc} . '"' ) ;
			if ( $first ) { $first = 0 ; }
			else { print OUT ",\n" ; }
			print OUT encode_json ( $out ) ;
		}
		$out = {} ;
	}
}
close IN ;

print OUT "\n]}" ;

`rm *.zip *.nt` ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => '' } ;
}