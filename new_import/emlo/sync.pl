#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my @aux_list = (
	{ 'pattern' => '(http://en.wikipedia.org/wiki/)(.+?)' , 'catalog' => 'enwiki' } ,
	{ 'pattern' => '(http://thesaurus.cerl.org/cgi-bin/record.pl\?rid=)(.+?)' , 'catalog' => 'cerl' } ,
	{ 'pattern' => '(http://www.prdl.org/author_view.php\?a_id=)(.+?)' , 'catalog' => 'prdl' } ,
	{ 'pattern' => '(http://www.oxforddnb.com/view/article/)(\d+).*?' , 'prop' => 'P1415' } ,
	{ 'pattern' => '(http://d-nb.info/gnd/)([0-9a-zA-Z]+?)' , 'prop' => 'P227' } ,
	{ 'pattern' => '(http://www.geonames.org/)(\d+)/.*?' , 'prop' => 'P1566' } ,
	{ 'pattern' => '(http://www.getty.edu/vow/TGNHierarchy?find=)(\d+?).*?' , 'prop' => 'P1667' }
) ;


open OUT, "> out.json" or die $! ;
#binmode(OUT, ":utf8");

my $first = 1 ;
print OUT '{"catalog":35,"entries":' ;
print OUT "[\n" ;
run ( { 'url' => 'http://emlo.bodleian.ox.ac.uk/browse/people?letter=' , 'type' => 'person' } ) ;
run ( { 'url' => 'http://emlo.bodleian.ox.ac.uk/browse/locations?letter=' , 'type' => 'location' } ) ;
run ( { 'url' => 'http://emlo.bodleian.ox.ac.uk/browse/organisations?letter=b' , 'type' => 'organisation' } ) ;
print OUT "\n]}" ;

0 ;

sub run {
	my ( $p ) = @_ ;
	
	foreach my $letter ( 'a' .. 'z' ) {
		my $url = $p->{url} . $letter ;
		my $content = get $url;
		my @rows = split "\n" , $content ;

		my $tr = 0 ;
		my $td = 0 ;
		my $in_related = 0 ;
		my $out = {} ;
		foreach my $row ( @rows ) {
			if ( $row =~ m|<tr| ) { $tr = 1 ; $td = 0 ; $in_related = 0 ; $out = {} ; next ; }
			if ( $row =~ m|</tr| ) {
				#TODO dump
				if ( defined $out->{id} ) {
					$out->{desc} =~ s|; $|| ;
					if ( $first ) { $first = 0 ; }
					else { print OUT ",\n" ; }
					print OUT encode_json ( $out ) ;
				}
				$tr = 0 ;
				next ;
			}
			next if $tr == 0 ;
			if ( $row =~ m|<td class="normalcell">| ) { $td++ ; next ; }
			next if $row =~ m|</td>| ;
			
			if ( $td == 2 ) { # Name
				if ( $row =~ m|^\s*<a.*?href="(/profile/[^/]+/)(.+?)".*?">(.+?)</a>| ) {
					$out = {
						'name' => $3 ,
						'id' => $2 ,
						'url' => "http://emlo.bodleian.ox.ac.uk$1$2" ,
						'desc' => '' ,
						'type' => $p->{type} ,
						'aux' => []
					} ;
					if ( $out->{name} =~ m|^(.+), ([^,]*\d{3,}.*)$| ) { # Split dates to desc
						$out->{desc} = "$2; " ;
						$out->{name} = $1 ;
					}
					if ( $out->{name} =~ m|^(.+), ([^,]+)$| ) { # Re-order
						$out->{name} = "$2 $1" ;
					}
				}
			}
			
			if ( $td == 3 ) {
				$row =~ s|^\s+|| ;
				$row =~ s|<br\s*/>|| ;
				next if $row =~ m|^\s*$| ;
				if ( $row =~ m|Related resource:| ) { $in_related = 1 ; next ; }
				
				my $skip = 0 ;
				foreach my $aux ( @aux_list ) {
					my $p = $aux->{pattern} ;
					if ( $row =~ m|<a.+?href="$p"| ) {
						my $naux = { 'id' => $2 , 'url' => "$1$2" } ;
						$naux->{catalog} = $aux->{catalog} if defined $aux->{catalog} ;
						$naux->{prop} = $aux->{prop} if defined $aux->{prop} ;
						push @{$out->{aux}} , $naux ;
						$skip = 1 ;
						last ;
					}
				}
				next if $skip ;
				$out->{desc} .= "$row; " unless $in_related ;
			}

		}

	}
}
