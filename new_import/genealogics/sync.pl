#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::UserAgent;
use Data::Dumper ;
use URI::Escape ;
use JSON ;

my $ua = LWP::UserAgent->new ( 'agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:37.0) Gecko/20100101 Firefox/37.0' ) ;
#$ua->timeout(10);

my $main = '' ;
if ( 0 ) {
	$main = my_get ( 'http://www.genealogics.org/surnames-all.php?tree=' ) ;
} else {
	open FILE , 'test.html' ; while ( <FILE> ) { $main .= $_ } close FILE ;
}


open OUT, "> out.json" or die $! ;
print OUT '{"catalog":53,"entries":' ;
print OUT "[\n" ;

my $first = 1 ;
my $out ;
while ( $main =~ m|^\d+.\s+<a href="(search\.php\?mylastname=.+?&amp;lnqualify=equals&amp;mybool=AND)">.+?</a>\s*\((\d+)\)|gm ) {
	my $url = "http://www.genealogics.org/".$1 ;
	if ( $2 == 1 ) {
		add_from_url ( $url ) ;
	} else {
		my $p2 = my_get ( $url ) ;
		while ( $p2 =~ m|<a href="(getperson.php\?personID=.+?)" class="pers" id=".+?">|gm ) {
			$url = $1 ;
			$url =~ s/&amp;/&/g ;
			$url = "http://www.genealogics.org/$url" ;
			add_from_url ( $url ) ;
		}
	}
}

print OUT "\n]}" ;

0 ;

sub add_from_url {
	my ( $url ) = @_ ;
	my $page = my_get ( $url ) ;
	$page =~ s/\s+/ /g ;
	my $out = get_blank() ;
#	print "$url\n" ;
	return unless $page =~ m|<h1 class="header[^>]*?>([^<]+).*?</h1><span class="normal"[^>]*>(.*?)</span>|m ;
	$out->{name} = $1 ;
	$out->{desc} = $2 ;
	$out->{name} =~ s/[\)\(]//g ;
	$out->{name} =~ s/\\//g ;
	$out->{desc} =~ s|<.+?>|| ;
	
	if ( $out->{name} =~ m|^(.+) '(.+)'$| ) {
		$out->{desc} = "$2; " . $out->{desc} ;
		$out->{name} = $1 ;
		$out->{desc} =~ s|[; ]+$|| ;
	}
	
	return unless $page =~ m|<li><a id="a0" href="getperson.php\?personID=(.+?)&amp;tree=LEO" class="here">|m ;
	$out->{id} = $1 ;
	$out->{url} = "http://www.genealogics.org/getperson.php?personID=$1&tree=LEO" ;

	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	print OUT encode_json ( $out ) ;
}

sub my_get {
	my ( $url ) = @_ ;
	my $ret = '' ;
	open IN , "wget -q -O - \"$url\" |" ;
	while ( <IN> ) { $ret .= $_ ; }
	close IN ;
	return $ret ;
}

sub get_blank {
	return { desc => '' , aux => [] , type => '' } ;
}
