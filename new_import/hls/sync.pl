#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;
use HTML::Entities;

my $catalog_id = 73 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

foreach my $letter ( 'a' .. 'z' ) {
	continuous_parse ( 'http://www.hls-dhs-dss.ch/d/alle-artikel/'.$letter.'/$1' , 0 , 59 ) ;
}

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => '' } ;
}

sub batch_parse {
	my $index_page_list = generate_pagelist () ;
	foreach my $url ( @{$index_page_list} ) {
		my $page = get $url ;
		parse_page ( $page , $url ) ;
	}
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########

sub parse_page {
	my ( $page , $url ) = @_ ;
	my $ret = 0 ;
	while ( $page =~ m|\s*<a rel="popup" href="http://www.hls-dhs-dss.ch/textes/d/D(.+?).php">(.+?)</a><br>\s*|g ) {

		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{url} = "http://www.hls-dhs-dss.ch/textes/d/D$1.php" ;
		$out->{name} = decode_entities ( $2 ) ;
		
		next if $out->{name} =~ m|==>| ; # Redirect
		
		if ( $out->{name} =~ m|^(.+)\s*\[(.+)\]$| ) {
			$out->{desc} = $2 ;
			$out->{name} = $1 ;
			
		}
		
		if ( $out->{name} =~ m|^([^,]+), ([^,]+)$| ) {
			$out->{name} = "$2 $1" ;
			$out->{type} = 'person' ;
		}
		
		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$ret++ ;
	}
	return $ret ;
}

