#!/usr/bin/php
<?PHP

$s = exec ( "curl -s 'http://kaiserhof.geschichte.lmu.de/response.php' -H 'Host: kaiserhof.geschichte.lmu.de' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:38.0) Gecko/20100101 Firefox/38.0' -H 'Accept: application/xml' -H 'Accept-Language: en' --compressed -H 'DNT: 1' -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' -H 'Referer: http://kaiserhof.geschichte.lmu.de/' -H 'Connection: keep-alive' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data 'Q%5B8%5D%5B1%5D%5Bm%5D=is&Q%5B8%5D%5B1%5D%5Bs%5D=%25&Q%5B6%5D%5B1%5D%5Bm%5D=is&Q%5B6%5D%5B1%5D%5Bs%5D=1&K%5B%5D=SEARCH&K%5B%5D=0&K%5B%5D=0'" ) ;

preg_match_all ( '/<tr x="(\d+)"><td>(.+?)<\/td><\/tr>/' , $s , $w ) ;

$out = array ( 'catalog' => 54 , 'entries' => array() ) ;

for ( $p = 0 ; $p < count ( $w[0] ) ; $p++ ) {
	$id = $w[1][$p] ;
	$url = "http://kaiserhof.geschichte.lmu.de/$id" ;
	$name = $w[2][$p] ;
	$desc = '' ;
	if ( preg_match ( '/^(.+)\s\((.+?)\),*\s*(.*)$/' , $name , $d ) ) {
		$name = trim($d[1]) ;
		$desc = trim($d[2]) . ", " . trim($d[3]) ;
	}
	if ( preg_match ( '/^(.+), (.+)$/' , $name , $d ) ) {
		$name = $d[2] . " " . $d[1] ;
	}

	$out['entries'][] = array (
		'id' => $id ,
		'name' => $name ,
		'desc' => $desc ,
		'url' => $url ,
		'type' => 'person' ,
		'aux' => array()
	) ;
}


$fh = fopen ( "out.json" , 'w' ) ;
fwrite ( $fh , json_encode ( $out ) ) ;
fclose ( $fh ) ;


?>