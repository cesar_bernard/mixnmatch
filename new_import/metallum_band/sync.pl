#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 75 ;

open OUT, "> out.json" or die $! ;
my $first = 1 ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;
while ( <> ) {
	chomp ;
	next unless $_ =~ m|^(\d+)\s(.+)$| ;
	my $out = { id => $1 , name => $2 , desc => '' , aux => [] , type => 'band' } ;
	$out->{name} =~ s|\s+$|| ;
	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	print OUT encode_json ( $out ) ;
}
print OUT "\n]}" ;
close OUT ;

0 ;