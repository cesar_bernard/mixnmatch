#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 95 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

batch_parse () ;
#continuous_parse ( 'https://www.eerstekamer.nl/oud_leden?start=$1&s01=jv5dj6y41uz601&_charset_=UTF-8&u1a=&dlgid=jv5dj6ydgglmd&' , 0 , 50 ) ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}

sub batch_parse {
	my $index_page_list = generate_pagelist () ;
	foreach my $url ( @{$index_page_list} ) {
		my $page = get $url ;
		parse_page ( $page , $url ) ;
	}
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########

sub generate_pagelist {
	my @letters = ( 'a' .. 'z' ) ;
	my @pages ;
	foreach my $letter ( @letters ) {
		push @pages , "http://www.ngv.vic.gov.au/explore/collection/artist/?surname=$letter" ;
	}
	return \@pages ;
}

sub parse_page {
	my ( $page , $url ) = @_ ;
	
	$page =~ s/\s+/ /g ;
	
	while ( $page =~ m|<tr>\s*<td data-text=".+?">\s*<a href="(/explore/collection/artist/)(\d+)">(.+?)</a>\s*</td>\s*<td[^>]*>(.+?)</td>\s*<td[^>]*>(.+?)</td>\s*</tr>|g ) {

		my $out = get_blank() ;
		$out->{id} = $2 ;
		$out->{url} = "http://www.ngv.vic.gov.au$1$2" ;
		$out->{name} = $3 ;
		
		my $date = $4 ;
		$out->{desc} = $5 ;
		
		$date =~ s|<.+?>||g ;
		$date =~ s|&mdash;||g ;
		$date =~ s|^\s+|| ;
		$date =~ s|\s+$|| ;
		
		$out->{desc} =~ s|<.+?>||g ;
		$out->{desc} =~ s|&mdash;||g ;
		$out->{desc} =~ s|^\s+|| ;
		$out->{desc} =~ s|\s+$|| ;
		
		if ( $date ne '' ) {
			if ( $out->{desc} eq '' ) { $out->{desc} = "born $date" ; }
			else { $out->{desc} = "born $date; " . $out->{desc} ; }
		}

		
		$out->{name} =~ s|<.+?>||g ;
		$out->{name} =~ s/\s+/ / ;
		$out->{name} =~ s/^\s// ;
		$out->{name} =~ s/\s$// ;
		

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}

