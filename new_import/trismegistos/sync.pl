#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 76 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

open IN , "data.tab" ;
binmode IN, ':utf8';
while ( <IN> ) {
	chomp ;
	my ( $id , $name , $desc ) = split "\t" , $_ ;
	next unless defined $name ;
	my $out = get_blank() ;
	$out->{id} = $id ;
	$out->{name} = $name ;
	$out->{desc} = $desc ;
	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	print OUT encode_json ( $out ) ;
}
close IN ;
print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => '' } ;
}
