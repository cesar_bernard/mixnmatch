<?PHP

ini_set('memory_limit','2500M');
set_time_limit ( 60 * 2 ) ; // Seconds
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;
require_once ( "/data/project/mix-n-match/public_html/php/wikidata.php" ) ;

function micDrop ( $do_exit = false ) {
	global $callback , $out ;
	if ( $callback != '' ) print $callback.'(' ;
	print json_encode ( $out ) ;
	if ( $callback != '' ) print ')' ;
	myflush();
	ob_end_flush() ;
	if ( $do_exit ) exit(0) ;
}

function addExtendedEntryData () {
	global $out , $mnm ;
	if ( count ( $out['data']['entries'] ) == 0 ) return ;

	$keys = implode ( ',' , array_keys ( $out['data']['entries'] ) ) ;

	// Person birth/death dates
	$sql = "SELECT * FROM person_dates WHERE entry_id IN ($keys)" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		if ( $o->born != '' ) $out['data']['entries'][$o->entry_id]->born = $o->born ;
		if ( $o->died != '' ) $out['data']['entries'][$o->entry_id]->died = $o->died ;
	}

	// Location data
	$sql = "SELECT * FROM location WHERE entry IN ($keys)" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$out['data']['entries'][$o->entry]->lat = $o->lat ;
		$out['data']['entries'][$o->entry]->lon = $o->lon ;
	}

	// Multimatch
	$sql = "SELECT * FROM multi_match WHERE entry_id IN ($keys)" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$a = [] ;
		foreach ( explode(',',$o->candidates) AS $c ) $a[] = 'Q'.$c ;
		$out['data']['entries'][$o->entry_id]->multimatch = $a ;
	}

	// Aux
	$sql = "SELECT * FROM auxiliary WHERE entry_id IN ($keys)" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$out['data']['entries'][$o->entry_id]->aux[] = $o ;
	}
}

$bad_q = [] ;
function compareSets ( $s1 , $s2 ) {
	global $bad_q ;
	$ret = [] ;
	foreach ( $s1 AS $q => $v1 ) {
		if ( isset($bad_q[$q*1]) ) continue ; // Duplicate horrors, ignore
		foreach ( $v1 AS $extid ) {
			if ( !isset ( $s2[$q] ) ) {
				$ret[] = [ $q , utf8_encode($extid) ] ;
			} else if ( !in_array ( $extid , $s2[$q] ) ) {
				$ret[] = [ $q , utf8_encode($extid) ] ;
			}
		}
	}
	return $ret ;
}

function get_users ( $users ) {
	global $mnm ;
	if ( count ( $users ) == 0 ) return [] ;
	$ret = [] ;
	$sql = "SELECT * FROM user WHERE id IN (" . implode(',',array_keys($users)) . ")" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$ret[$o->id] = $o ;
	}
	return $ret ;
}


function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

function renderAtom ( &$out ) {
	global $mnm ;
	$time = new DateTime;
	$ts = $time->format(DateTime::ATOM);
	$xml = '<?xml version="1.0" encoding="utf-8"?>

	<feed xmlns="http://www.w3.org/2005/Atom">
	<title>Mix\'n\'match</title>
	<subtitle>Recent updates by humans (auto-matching not shown)</subtitle>
	<link href="https://tools.wmflabs.org/mix-n-match/api.php?query=rc_atom" rel="self" />
	<link href="https://tools.wmflabs.org/mix-n-match" />
	<id>urn:uuid:' . gen_uuid() . '</id>
	<updated>' . $ts . '</updated>' ;


	# Get catalogs
	$catalogs = [] ;
	foreach ( $out['data']['events'] AS $k => $e ) $catalogs[$e->catalog] = $e->catalog ;
	if ( count($catalogs) > 0 ) {
		$sql = "SELECT * FROM catalog WHERE id IN (" . implode(',',$catalogs) . ")" ;
		$catalogs = [] ;
		$result = $mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $catalogs[$o->id] = $o ;
	}

	# Get wikidata items
	$items = [] ;
	foreach ( $out['data']['events'] AS $k => $e ) {
		if ( $e->event_type == 'match' and $e->q > 0 ) $items['Q'.$e->q] = 'Q'.$e->q ;
	}
	$wil = new WikidataItemList ;
	$wil->loadItems ( $items ) ;

	foreach ( $out['data']['events'] AS $k => $e ) {
		$xml .= "<entry>\n" ;
		$xml .= '<title>' ;
		if ( $e->event_type == 'match' ) $xml .= "New match for " ;
		else if ( $e->event_type == 'remove_q' ) $xml .= "Match was removed for " ;
		$xml .= '"' . htmlspecialchars($e->ext_name, ENT_XML1, 'UTF-8') . '"' ;
		$xml .= "</title>\n" ;
		$xml .= "<link rel=\"alternate\" href=\"https://tools.wmflabs.org/mix-n-match/#/entry/{$e->id}\" />\n" ;
		$xml .= "<id>urn:uuid:" . gen_uuid() . "</id>\n" ;
		$ts = new DateTime ( $e->timestamp ) ;
		$xml .= '<updated>' . $ts->format(DateTime::ATOM) . '</updated>' ;
		$xml .= "<content type=\"xhtml\"><div xmlns=\"http://www.w3.org/1999/xhtml\">" ;
#		$xml .= "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/ANZAC_Parade_from_the_Australian_War_Memorial%2C_Canberra_ACT.jpg/320px-ANZAC_Parade_from_the_Australian_War_Memorial%2C_Canberra_ACT.jpg'/>" ;
		if ( $e->event_type == 'match' ) {
			if ( $e->q == 0 ) {
				$xml .= "<p>Entry was marked as <i>NOT ON WIKIDATA</i></p>" ;
			} else if ( $e->q == -1 ) {
				$xml .= "<p>Entry was marked as <i>NOT APPLICABLE</i></p>" ;
			} else {
				$img = '' ;
				$label = 'Q'.$e->q ;
				$i = $wil->getItem ( $label ) ;
				if ( isset($i) ) {
					$label = htmlspecialchars($i->getLabel(), ENT_XML1, 'UTF-8') ;

					if ( $i->hasClaims('P18') ) {
						$fn = myurlencode($i->getFirstString('P18')) ;
						$url = "https://commons.wikimedia.org/wiki/Special:Redirect/file/{$fn}?width=160&height=160" ;
						$url = htmlspecialchars($url, ENT_XML1, 'UTF-8') ;
						$page_url = htmlspecialchars("https://commons.wikimedia.org/wiki/File:{$fn}", ENT_XML1, 'UTF-8') ;
						$img = "<a href='{$page_url}'><img border='0' src='{$url}' /></a>" ;
#						$img = "<div style='float:right;margin-left:10px;'>{$img}</div>" ;
					}
				}
				$xml .= "<p>Entry was matched to <a href='https://www.wikidata.org/wiki/Q{$e->q}'>{$label}</a>" ;
				if ( $label != 'Q'.$o->q ) $xml .= " <small>[Q{$e->q}]</small>" ;
				$xml .= ".</p>" ;
				if ( $img != '' ) $xml .= "<p>{$img}</p>" ;
			}
		}
		if ( $e->ext_desc != '' ) {
			$xml .= '<p>Description: <i>' . htmlspecialchars($e->ext_desc, ENT_XML1, 'UTF-8') . "</i></p>\n" ;
		}
		$xml .= '<p>External ID: ' ;
		if ( $e->ext_url != '' ) $xml .= "<a href='{$e->ext_url}'>" ;
		$xml .= htmlspecialchars($e->ext_id, ENT_XML1, 'UTF-8') ;
		if ( $e->ext_url != '' ) $xml .= "</a>" ;
		$xml .= "</p>\n" ;
		if ( isset($catalogs[$e->catalog]) ) {
			$cat = $catalogs[$e->catalog] ;
			$xml .= "<p>In catalog #{$e->catalog}: <a href='https://tools.wmflabs.org/mix-n-match/#/catalog/{$e->catalog}'>{$cat->name}</a>" ;
			if ( isset($cat->wd_prop) and !isset($cat->wd_qual) ) $xml .= ", set as property <a href='https://www.wikidata.org/wiki/P{$cat->wd_prop}'>P{$cat->wd_prop}</a>" ;
			$xml .= "</p>" ;
		}
		$xml .= "</div></content>\n" ;

		if ( isset($out['data']['users'][$e->user]) ) {
			$username = $out['data']['users'][$e->user]->name ;
			$xml .= '<author>' ;
			$xml .= '<name>' . htmlspecialchars($username, ENT_XML1, 'UTF-8') . '</name>' ;
			$xml .= "<uri>https://www.wikidata.org/wiki/User:" . urlencode($username) . "</uri>\n" ;
			$xml .= '</author>' ;
		}
		$xml .= "</entry>\n" ;
	}

	$xml .= '</feed>' ;

	header('Content-type: application/atom+xml');
#	header('Content-type: text/plain');
	echo $xml ;
	exit(0) ;
}



$query = get_request ( 'query' , '' ) ;
$user = get_request ( 'tusc_user' , -1 ) ;
$callback = get_request ( 'callback' , '' ) ;


$mnm = new MixNMatch ;


if ( $query == 'download2' ) {

	$catalogs = preg_replace ( '/[^0-9,]/' , '' , get_request ( 'catalogs' , '' ) ) ;
	$format = get_request ( 'format' , 'tab' ) ;
	$columns = (object) json_decode(get_request('columns','{}')) ;
	$hidden = (object) json_decode(get_request('hidden','{}')) ;
	$as_file = get_request ( 'as_file' , 0 ) ;


	$sql = 'SELECT entry.id AS entry_id,entry.catalog,ext_id AS external_id' ;
	if ( $columns->exturl ) $sql .= ',ext_url AS external_url,ext_name AS `name`,ext_desc AS description,`type` AS entry_type,entry.user AS mnm_user_id' ;
	$sql .= ',(CASE WHEN q IS NULL THEN NULL else concat("Q",q) END) AS q' ;
	$sql .= ',`timestamp` AS matched_on' ;

	if ( $columns->username ) $sql .= ',user.name AS matched_by_username' ;
	if ( $columns->aux ) $sql .= ',(SELECT group_concat(concat("{`P",aux_p,"`,`",aux_name,"`,`",in_wikidata,"`}") separator "|") FROM auxiliary WHERE auxiliary.entry_id=entry.id GROUP BY auxiliary.entry_id) AS auxiliary_data' ;
	if ( $columns->dates ) $sql .= ',person_dates.born,person_dates.died,person_dates.in_wikidata AS dates_in_wikidata' ;
	if ( $columns->location ) $sql .= ',location.lat,location.lon' ;
	if ( $columns->multimatch ) $sql .= ',multi_match.candidates AS multi_match_candidates' ;

	$sql .= ' FROM entry' ;

	if ( $columns->dates ) $sql .= ' LEFT JOIN person_dates ON (entry.id=person_dates.entry_id)' ;
	if ( $columns->location ) $sql .= ' LEFT JOIN location ON (entry.id=location.entry)' ;
	if ( $columns->multimatch ) $sql .= ' LEFT JOIN multi_match ON (entry.id=multi_match.entry_id)' ;
	if ( $columns->username ) $sql .= ' LEFT JOIN user ON (entry.user=user.id)' ;

	$sql .= " WHERE entry.catalog IN ({$catalogs})" ;
	if ( $hidden->any_matched ) $sql .= " AND entry.q IS NULL" ;
	if ( $hidden->firmly_matched ) $sql .= " AND (entry.q IS NULL OR entry.user=0)" ;
	if ( $hidden->user_matched ) $sql .= " AND (entry.user IS NULL OR entry.user IN (0,3,4))" ;
	if ( $hidden->unmatched ) $sql .= " AND entry.q IS NOT NULL" ;
	if ( $hidden->no_multiple ) $sql .= " AND EXISTS (SELECT * FROM multi_match WHERE entry.id=multi_match.entry_id)" ;
	if ( $hidden->name_date_matched ) $sql .= " AND entry.user!=3" ;
	if ( $hidden->automatched ) $sql .= " AND entry.user!=0" ;
	if ( $hidden->aux_matched ) $sql .= " AND entry.user!=4" ;

	if ( $format == 'json' ) header('Content-type: application/json');
	else header('Content-type: text/plain');

	if ( $as_file ) {
		$filename = "mix-n-match.{$catalogs}." . date('YmdHis') . ".{$format}" ;
		header('Content-Disposition: attachment;filename="' . $filename . '"');
	}
	if ( $format == 'json' and isset($_REQUEST['callback']) ) print $_REQUEST['callback'] . '(' ;

	$first_row = true ;

	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_assoc()){
		if ( $first_row ) {
			if ( $format == 'tab' ) print '#' . implode ( "\t" , array_keys ( $o ) ) . "\n" ;
			if ( $format == 'json' ) print "[\n" ;
		} else {
			if ( $format == 'json' ) print ",\n" ;
		}
		$first_row = false ;

		if ( $format == 'json' ) {
			print json_encode ( $o ) ;
		} else { # Default: tab
			$p = [] ;
			foreach ( $o AS $k => $v ) $p[] = preg_replace ( '/\s/' , ' ' , $v ) ; # Ensure no tabs/newlines in value
			print implode ( "\t" , $p ) . "\n" ;
		}
	}

	if ( $first_row ) { // Nothing was written
		if ( $format == 'json' ) print "[\n" ;
	}

	if ( $format == 'json' ) print "\n]" ;
	if ( $format == 'json' and isset($_REQUEST['callback']) ) print ')' ;

	myflush();
	ob_end_flush() ;
	exit(0) ;
}


if ( $query == 'download' ) {
	$filename = '' ;
	$catalog = get_request ( 'catalog' , 0 ) * 1 ;
	$sql = "SELECT * FROM catalog WHERE id=$catalog" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$filename = str_replace ( ' ' , '_' , $o->name ) . ".tsv" ;
	}
	$users = [] ;
	$sql = "SELECT * FROM user" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$users[$o->id] = $o->{name} ;
	}
	header('Content-type: text/plain');
	header('Content-Disposition: attachment;filename="' . $filename . '"');
	print "Q\tID\tURL\tName\tUser\n" ;
	$sql = "SELECT * FROM entry WHERE catalog=$catalog AND q IS NOT NULL AND q > 0 AND user!=0" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$user = '' ;
		if(isset($o->user)){ // query ensures user != 0, but user can still be null
			$user = $users[$o->user] ;
		}
		$p = '' ;
		$p .= $o->{q} . "\t" ;
		$p .= $o->{ext_id} . "\t" ;
		$p .= $o->{ext_url} . "\t" ;
		$p .= $o->{ext_name} . "\t" ;
		$p .= $user . "\n" ;
		print $p ;
	}
	exit ( 0 ) ;
}



#$sql = "SET CHARACTER SET utf8" ;
#$result = $mnm->getSQL ( $sql ) ;


if ( $query == 'redirect' ) {
	$catalog = get_request ( 'catalog' , '0' ) * 1 ;
	$ext_id = get_request ( 'ext_id' , '' ) ;
	$url = '' ;
	$sql = "SELECT ext_url FROM entry WHERE catalog=$catalog AND ext_id='" . $mnm->escape ( $ext_id ) . "'" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $url = $o->ext_url ;
	header('Content-type: text/html');
	print '<html><head><META http-equiv="refresh" content="0;URL='.$url.'"></head><body></body></html>' ;
	exit ( 0 ) ;
}



if ( $query == 'proxy' ) header('Content-type: text/html');
else if ( $query == 'rc_atom' ) {}
else header('Content-type: application/json; charset=UTF-8');

$out = [
	'status' => 'OK' ,
	'data' => []
] ;

if ( $query == 'catalogs' ) {

	$mnm->generateOverview ( $out ) ;

} else if ( $query == 'edit_catalog' ) {

	$catalog = get_request ( 'catalog' , 0 ) * 1 ;
	if ( $catalog == 0 ) {
		$out['error'] = "No catalog" ;
		micDrop(true) ;
	}
	$data = json_decode ( get_request ( 'data' , '' ) ) ;
	if ( !isset($data) or $data == null or !isset($data->name) ) {
		$out['error'] = "Bad data" ;
		micDrop(true) ;
	}
	$username = get_request ( 'username' , '' ) ;
	$username = str_replace ( '_' , ' ' , $username ) ;
	$sql = "SELECT * FROM user WHERE name='".$mnm->escape($username)."' LIMIT 1" ;
	$result = $mnm->getSQL ( $sql ) ;
	$found = ($o = $result->fetch_object()) ;
	if ( !$found ) {
		$out['error'] = "No such user" ;
		micDrop(true) ;
	}
	if ( !$o->is_catalog_admin ) {
		$out['error'] = "{$username} is not a catalog admin" ;
		micDrop(true) ;
	}
	$sql = "UPDATE catalog SET " ;
	$sql .= "`name`='" . $mnm->escape($data->name) . "'," ;
	$sql .= "`url`='" . $mnm->escape($data->url) . "'," ;
	$sql .= "`desc`='" . $mnm->escape($data->desc) . "'," ;
	$sql .= "`type`='" . $mnm->escape($data->type) . "'," ;
	$sql .= "`search_wp`='" . $mnm->escape($data->search_wp) . "'," ;
	$sql .= "`wd_prop`=" . ((isset($data->wd_prop) and $data->wd_prop*1>0)?$data->wd_prop*1:'null') . "," ;
	$sql .= "`wd_qual`=" . ((isset($data->wd_qual) and $data->wd_qual*1>0)?$data->wd_qual*1:'null') . "," ;
	$sql .= "`active`='" . ($data->active?1:0) . "'" ;
	$sql .= " WHERE id={$catalog}" ;

#	$out['sql'] = $sql ;
	$mnm->getSQL ( $sql ) ;
	$mnm->updateCatalogs ( [$catalog] ) ;

} else if ( $query == 'get_user_info' ) {

	$username = get_request ( 'username' , '' ) ;
	$username = str_replace ( '_' , ' ' , $username ) ;
	$sql = "SELECT * FROM user WHERE name='".$mnm->escape($username)."' LIMIT 1" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		$out['data'] = $o ;
	}

} else if ( $query == 'prep_new_item' ) {

	require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;
	require_once ( '/data/project/mix-n-match/manual_lists/large_catalogs/shared.php' ) ;

	$qs = new QuickStatements () ;
	$lc = new largeCatalog ( 2 ) ;

	$entry_ids = explode ( ',' , get_request ( 'entry_ids' , '' ) ) ;
	$all_commands = [] ;
	foreach ( $entry_ids AS $num => $entry_id ) {
		$entry = $mnm->getEntryObjectFromID ( $entry_id ) ;
		$commands = $mnm->getCreateItemForEntryCommands ( $entry , $lc ) ; // QS commands
		array_shift ( $commands ) ; // Remove CREATE
$out['commands'][] = $commands ;
		foreach ( $commands AS $k => $v ) {
			if ( $num > 0 and preg_match ( '/^LAST\t[LADS]/' , $v ) ) continue ; // Labels etc. from first entry only
			$key = $v ;
			if ( preg_match ( '/^(LAST\t.+?\t.+?)(\t.+)$/' , $v , $m ) ) {
				$key = $m[1] ;
				if ( isset($all_commands[$key]) ) $v = $all_commands[$key] . $m[2] ;
			}
			$all_commands[$key] = $v ;
		}
	}
	$all_commands = array_unique ( $all_commands ) ;
	array_unshift ( $all_commands , 'CREATE' ) ; // Prefix CREATE
$out['all_commands'] = $all_commands ;
	$commands = implode ( "\n" , $all_commands ) ;
	$qs->use_command_compression = true ;
	$commands = $qs->importData ( $commands , 'v1' , false ) ;
	$commands = $qs->compressCommands ( $commands['data']['commands'] ) ;
	$out['data'] = json_encode ( $commands[0]['data'] , JSON_HEX_QUOT|JSON_HEX_APOS ) ;

} else if ( $query == 'save_scraper' ) {

	$scraper = json_decode ( get_request ( 'scraper' , '{}' ) ) ;
	$levels = json_decode ( get_request ( 'levels' , '{}' ) ) ;
	$options = json_decode ( get_request ( 'options' , '{}' ) ) ;
	$meta = json_decode ( get_request ( 'meta' , '{}' ) ) ;
	
	$user_id = $mnm->getOrCreateUserID ( $user ) ;
	
	if ( $mnm->isUserBlocked($user) ) {
		$out['status'] = "You are blocked on Wikidata" ;
	} else {
		$prop = preg_replace ( '/\D/' , '' , $meta->property ) ;
		$cid = trim($meta->catalog_id) ;
		$exists = false ;
		if ( !preg_match ( '/^\d+$/' , $cid ) ) { // Creat new catalog
			$type = $meta->type ;
			if ( $scraper->resolve->type->use == 'Q5' ) $type = 'biography' ;
			$sql = "INSERT INTO catalog (`name`,`url`,`desc`,`type`," ;
			if ( $prop != '' ) $sql .= "`wd_prop`," ;
			$sql .= "`search_wp`,`owner`,`note`) VALUES (" ;
			$sql .= '"' . $mnm->escape($meta->name) . '",' ;
			$sql .= '"' . $mnm->escape($meta->url) . '",' ;
			$sql .= '"' . $mnm->escape($meta->desc) . '",' ;
			$sql .= '"' . $mnm->escape($meta->type) . '",' ;
			if ( $prop != '' ) $sql .= '"' . $prop . '",' ;
			$sql .= '"' . $mnm->escape($meta->lang) . '",' ;
			$sql .= $user_id . ",'Created via scraper import')" ;
			$out['sql'] = $sql ;
			$result = $mnm->getSQL ( $sql ) ;
			$cid = $mnm->dbm->insert_id ;
			$exists = true ;
		} else { // Check if catalog exists
			$sql = "SELECT * FROM catalog WHERE id=$cid" ;
			$result = $mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $exists = true ;
		}

		if ( !$exists ) {
			$out['status'] = "Catalog #$cid does not exist" ;
		} else { // Add autoscrape
			$out['data']['catalog'] = $cid ;
			$j = [
				'levels' => $levels ,
				'options' => $options ,
				'scraper' => $scraper
			] ;
			$j = $mnm->escape(json_encode($j)) ;
			
			$existing = (object) ['bla'=>'test'] ;
			$sql = "SELECT * FROM autoscrape WHERE catalog=$cid" ;
			$result = $mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $existing = $o ;
			
			if ( isset($existing->owner) ) {
				if ( $existing->owner == $user_id ) {
					$sql = "UPDATE autoscrape SET `json`='$j',`status`='IMPORT' WHERE catalog=$cid" ;
					$result = $mnm->getSQL ( $sql ) ;
				} else {
					$out['status'] = "A different user created the existing scraper" ;
				}
			} else {
				$sql = "INSERT INTO autoscrape (`catalog`,`json`,`owner`,`notes`,`status`) VALUES ($cid," ;
				$sql .= '"' . $j . '",' ;
				$sql .= "$user_id,'Created via scraper import','IMPORT')" ;
				$sql .= " ON DUPLICATE KEY UPDATE json='$j',status='IMPORT'" ;
				$result = $mnm->getSQL ( $sql ) ;
			}
		}

	}


} else if ( $query == 'autoscrape_test' ) {
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR); // 

	require_once ( '/data/project/mix-n-match/autoscrape.inc' ) ;
	$json = get_request ( 'json' , '' ) ;
	$as = new AutoScrape ;
	
	if ( !$as->loadFromJSON ( $json , 0 ) ) {
		$out['status'] = $as->error ;
	} else {
		$as->max_urls_requested = 1 ;
		$as->log2array = true ;
		$as->runTest() ;
		if ( isset($as->error) AND $as->error != null ) $out['status'] = $as->error ;
		$out['data']['html'] = $as->last_html ;
		$out['data']['log'] = $as->logs ;
		$out['data']['results'] = $as->entries ;
		$out['data']['last_url'] = $as->getLastURL() ;
		$out['data']['html'] = utf8_encode ( $out['data']['html'] ) ;
		foreach ( $out['data']['results'] AS $k => $v ) {
		}
	}

} else if ( $query == 'update_overview' ) {

	$catalog = get_request ( 'catalog' , '' ) ;
	if ( $catalog != '' ) {
		$catalogs = explode ( ',' , $catalog ) ;
		$mnm->updateCatalogs ( $catalogs ) ;
	} else {
		$mnm->updateCatalogs ( $mnm->getAllCatalogIDs() ) ;
	}


} else if ( $query == 'sparql_list' ) {

	$out['data']['entries'] = [] ;
	$out['data']['users'] = [] ;

	$label2q = [] ;
	$labels = [] ;
	$sparql = get_request ( 'sparql' , '' ) ;
	$j = getSPARQL ( $sparql ) ;
	$vars = $j->head->vars ;
	$var1 = $vars[0] ;
	$var2 = $vars[1] ;
	foreach ( $j->results->bindings AS $b ) {
		$v1 = $b->$var1 ;
		$v2 = $b->$var2 ;
		if ( $v1->type == 'uri' and $v2->type == 'literal' ) {
			$q = preg_replace ( '/^.+\/Q/' , 'Q' , $v1->value ) ;
			$label = $v2->value ;
		} else if ( $v1->type == 'uri' and $v2->type == 'literal' ) {
			$q = preg_replace ( '/^.+\/Q/' , 'Q' , $v2->value ) ;
			$label = $v1->value ;
		} else continue ;
		$label2q[$label] = $q ;
		$labels[] = $mnm->escape ( $label ) ;
	}

	$sql = "SELECT * FROM entry WHERE (user=0 OR q is null) AND ext_name IN ('" . implode("','",$labels) . "')" ;
//	$out['sql'] = $sql ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		if ( !isset($label2q[$o->ext_name]) ) continue ; // Paranoia
		$o->user = 0 ;
		$o->q = substr ( $label2q[$o->ext_name] , 1 ) * 1 ;
		$o->timestamp = '20180304223800' ;
		$out['data']['entries'][$o->id] = $o ;
	}


//	addExtendedEntryData() ;

	$out['data']['users'] = get_users ( [0] ) ;

} else if ( $query == 'catalog' or $query == 'get_entry' ) {
	$entry = get_request ( 'entry' , -1 ) ;
	$id = get_request ( 'catalog' , -1 ) ;
	$meta = json_decode ( get_request ( 'meta' , '' ) ) ;
	foreach ( ['show_nowd'] AS $k ) {
		if ( !isset($meta->$k) ) $meta->$k = 0 ;
	}
	
	$out['data']['entries'] = [] ;
	$out['data']['users'] = [] ;
	
	$q = [] ;
	

	$sql = "SELECT * FROM entry WHERE " ;
	if ( $query == 'get_entry' ) {
		$ext_ids = get_request ( 'ext_ids' , '' ) ;
		if ( $ext_ids != '' ) {
			$ext_ids = json_decode ( $ext_ids ) ;
			$x = [] ;
			foreach ( $ext_ids AS $eid ) $x[] = $mnm->escape($eid) ;
			$ext_ids = '"' . implode ( '","' , $x ) . '"' ;
			$sql .= "catalog=" . $mnm->escape($id) . " AND ext_id IN ($ext_ids)" ;
		} else $sql .= "id IN (" . $mnm->escape($entry) . ")" ;
//		$sql .= " ORDER BY entry.id" ;
	}else {
		$sql .= "catalog=" . $mnm->escape($id) ;
		if ( $meta->show_multiple == 1 ) {
			$sql .= " AND EXISTS ( SELECT * FROM multi_match WHERE entry_id=entry.id ) AND ( user<=0 OR user is null )" ;
		} else if ( $meta->show_noq+$meta->show_autoq+$meta->show_userq+$meta->show_nowd == 0 and $meta->show_na == 1 ) {
			$sql .= " AND q=0" ;
		} else if ( $meta->show_noq+$meta->show_autoq+$meta->show_userq+$meta->show_na == 0 and $meta->show_nowd == 1 ) {
			$sql .= " AND q=-1" ;
		} else {
			if ( $meta->show_noq != 1 ) $sql .= " AND q IS NOT NULL" ;
			if ( $meta->show_autoq != 1 ) $sql .= " AND ( q is null OR user!=0 )" ;
			if ( $meta->show_userq != 1 ) $sql .= " AND ( user<=0 OR user is null )" ;
			if ( $meta->show_na != 1 ) $sql .= " AND ( q!=0 or q is null )" ;
//			if ( $meta->show_nowd != 1 ) $sql .= " AND ( q=-1 )" ;
		}

		if ( isset($_REQUEST['type']) ) {
			$sql .= " AND `type`='" . $mnm->escape($_REQUEST['type']) . "'" ;
		}

//		$sql .= " ORDER BY entry.id" ;
		$sql .= " LIMIT " . $mnm->escape($meta->per_page) ;
		$sql .= " OFFSET " . $mnm->escape($meta->offset) ;
	}
	
	$out['sql'][] = $sql ;
if ( isset($_REQUEST['testing']) ) { print_r($out) ; exit(0) ; }
	
	$users = [] ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$out['data']['entries'][$o->id] = $o ;
		if ( $o->q != null ) $q[] = $o->q ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}

	addExtendedEntryData() ;
	
	$out['data']['users'] = get_users ( $users ) ;

} else if ( $query == 'catalog_details' ) {

	$catalog = get_request ( 'catalog' , 0 ) * 1 ;
	
	$sql = "select type,count(*) as cnt from entry where catalog=$catalog group by type order by cnt desc" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $out['data']['type'][] = $o ;
	
	$sql = "select substring(timestamp,1,6) AS ym,count(*) as cnt from entry where catalog=$catalog and timestamp is not null and user!=0 group by ym order by ym" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $out['data']['ym'][] = $o ;
	
	$sql = "select name as username,entry.user AS uid,count(*) as cnt from entry,user where catalog=$catalog and entry.user=user.id and user!=0 and entry.user is not null group by uid order by cnt desc" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $out['data']['user'][] = $o ;

} else if ( $query == 'match_q' ) {
	$entry = $mnm->escape ( get_request ( 'entry' , -1 ) * 1 ) ;
	$q = '' . ( get_request ( 'q' , -1 ) * 1 ) ;

	$user_id = $mnm->getOrCreateUserID ( $user ) ;
	
	if ( $mnm->isUserBlocked($user) ) {
		$out['status'] = "You are blocked on Wikidata!" ;
	} else {
	
		if ( $mnm->setMatchForEntryID ( $entry , $q , $user_id , false ) ) {
			$sql = "SELECT *,entry.type AS entry_type FROM entry,catalog WHERE entry.id=$entry and entry.catalog=catalog.id" ;
			$result = $mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $out['entry'] = $o ;
		} else {
			$out['status'] = "Problem with setting the match: " . $mnm->last_error ;
		}

		micDrop(true) ;
	}

} else if ( $query == 'match_q_multi' ) {

	$data = json_decode ( get_request('data','[]') ) ;
	$catalog = get_request ( 'catalog' , -1 ) * 1 ;
	
	$user_id = $mnm->getOrCreateUserID ( $user ) ;

	if ( $mnm->isUserBlocked($user) ) {
		$out['status'] = "You are blocked on Wikidata!" ;
		micDrop(true) ;
	}

	$out['not_found'] = 0 ;
	$out['no_changes_written'] = [] ;
	foreach ( $data AS $d ) {
		if ( $mnm->setMatchForCatalogExtID ( $catalog , $d[1] , $d[0] , $user_id , true , false ) ) continue ;
		if ( preg_match ( '/^External ID .* not found\.$/' , $mnm->last_error ) ) { $out['not_found']++ ; continue ; } // Ignore this
		if ( preg_match ( '/^No changes written\.$/' , $mnm->last_error ) ) {
			$out['no_changes_written'][] = [ 'ext_id' => $d[1] , 'new_q' => $d[0] , 'entry' => $mnm->last_entry ] ;
			continue ;
		}
		$out['status'] = "Problem with setting the match: " . $mnm->last_error ;
	}

} else if ( $query == 'remove_q' ) {

	$entry = $mnm->escape ( get_request ( 'entry' , -1 ) * 1 ) ;
	$user_id = $mnm->getOrCreateUserID ( $user ) ;

	if ( $mnm->isUserBlocked($user) ) {
		$out['status'] = "You are blocked on Wikidata!" ;
		micDrop(true) ;
	}
	if ( !$mnm->removeMatchForEntryID ( $entry ,  $user_id ) ) {
		$out['status'] = $mnm->last_error ;
	}


} else if ( $query == 'rc' or $query == 'rc_atom' ) {

	$limit = 100 ;
	$ts = get_request ( 'ts' , '' ) ;
	$catalog = get_request ( 'catalog' , 0 ) * 1 ;
	$events = [] ;
	
	$sql = "SELECT * FROM entry WHERE 1=1" ;
	# WHERE user not in (0,3,4) and timestamp IS NOT NULL" ;
	if ( $ts != '' ) $sql .= " AND timestamp >= '" . $mnm->escape($ts) . "'" ;
	if ( $catalog != 0 ) $sql .= " AND catalog={$catalog}" ;
	$sql .= " HAVING user!=0 and user!=3 and user!=4 and timestamp is not null" ;
	$sql .= " ORDER BY timestamp DESC LIMIT $limit" ;
	$min_ts = '' ;
	$max_ts = $ts ;
	$result = $mnm->getSQL ( $sql ) ;
	$users = [] ;
	while($o = $result->fetch_object()){
		$o->event_type = 'match' ;
		$events[$o->timestamp.'-'.$o->id] = $o ;
		if ( $min_ts == '' ) $min_ts = $o->timestamp ;
		$max_ts = $o->timestamp ;
		$users[$o->user] = 1 ;
	}

	$sql = "SELECT entry.id AS id,catalog,ext_id,ext_url,ext_name,ext_desc,action AS event_type,log.user AS user,log.timestamp AS timestamp FROM log,entry WHERE log.entry=entry.id AND log.timestamp BETWEEN '$max_ts' AND '$min_ts'" ;
	if ( $catalog != 0 ) $sql .= " AND catalog={$catalog}" ;
	$out['sql'] = $sql ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$events[$o->timestamp.'-'.$o->id] = $o ;
		$users[$o->user] = 1 ;
	}
	
	krsort ( $events ) ;
	
	while ( count ( $events ) > $limit ) array_pop ( $events ) ;
	
	$out['data']['events'] = $events ;
	$out['data']['users'] = get_users ( $users ) ;

	if ( $query == 'rc_atom' ) renderAtom ( $out ) ;


} else if ( $query == 'search' ) {
	
	$max_results = get_request('max',100) * 1 ;
	$what = get_request ( 'what' , '' ) ;
	$description_search = get_request ( 'description_search' , 0 ) * 1 ;
	$no_label_search = get_request ( 'no_label_search' , 0 ) * 1 ;
	$exclude = preg_replace ( '/[^0-9,]/' , '' , get_request ( 'exclude' , '' ) ) ;
	$include = preg_replace ( '/[^0-9,]/' , '' , get_request ( 'include' , '' ) ) ;
	$out['data']['entries'] = [] ;
	$out['data']['users'] = [] ;
	
	if ( $exclude == '' ) $exclude = [] ;
	else $exclude = explode ( ',' , $exclude ) ;
	if ( $include == '' ) $include = [] ;
	else $include = explode ( ',' , $include ) ;
	$sql = "SELECT id FROM catalog WHERE `active`!=1" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $exclude[] = $o->id ;
	$exclude = implode ( ',' , $exclude ) ;
	$include = implode ( ',' , $include ) ;
	
	$s = [] ;
	$what = preg_replace ( '/[\-]/' , ' ' , $what ) ;
	$what2 = explode ( " " , $what ) ;
	foreach ( $what2 AS $w ) {
		$w = $mnm->escape ( trim ( $w ) ) ;
		if ( $w == '' ) continue ;
		if ( strlen($w)<3 or strlen($w)>84 ) continue ;
		$s[] = $w ;
	}
	
	$sql_parts = [] ;
	if ( !$no_label_search ) $sql_parts[] = "MATCH(ext_name) AGAINST('+".implode(",+",$s)."' IN BOOLEAN MODE)" ;
	if ( $description_search ) $sql_parts[] = "MATCH(ext_desc) AGAINST('+".implode(",+",$s)."' IN BOOLEAN MODE)" ;

	$sql = "SELECT * FROM entry WHERE ((" . implode(") OR (",$sql_parts) . "))" ;
	if ( $exclude != '' ) $sql .= " AND catalog not in ($exclude)" ;
	if ( $include != '' ) $sql .= " AND catalog in ($include)" ;
	$sql .= " LIMIT $max_results" ;
	
	if ( preg_match ( '/^\s*[Qq]{0,1}(\d+)\s*$/' , $what , $m ) ) {
		$sql = "SELECT * FROM entry WHERE q=".$m[1] ;
	}
	
	$out['sql'] = $sql ;
	$mnm->dbm->set_charset('utf8mb4');  // Say what?!?
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$out['data']['entries'][] = $o ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}


	$out['data']['users'] = get_users ( $users ) ;


} else if ( $query == 'create' ) {
	
	$out['total'] = [] ;
	$catalog = get_request ( 'catalog' , '' ) ;
	if ( 1 ) { // Blank
		$sql = "select ext_id,ext_name,ext_desc,ext_url,type FROM entry WHERE q=-1 AND user>0 AND catalog=" . $mnm->escape ( $catalog ) . " ORDER BY ext_name" ;
	} else { // non-assessed, careful!
		$sql = "select ext_id,ext_name,ext_desc,ext_url,type FROM entry WHERE q IS null AND catalog=" . $mnm->escape ( $catalog ) . " ORDER BY ext_name" ;
	}
	$out['sql'] = $sql ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$out['data'][] = $o ;
	}

} else if ( $query == 'sitestats' ) {
	
	$out['total'] = [] ;
	$catalog = get_request ( 'catalog' , '' ) ;
	
	$sql = "SELECT distinct catalog,q FROM entry WHERE user>0 and q is not null" ;
	if ( $catalog != '' ) $sql .= " AND catalog=" . $mnm->escape ( $catalog ) ;
	$catalogs = [] ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$catalogs[$o->catalog][] = $o->q ;
	}
	
	$dbwd = openDB ( 'wikidata' ) ;
	foreach ( $catalogs AS $cat => $qs ) {
		$qs = implode ( ',' , $qs ) ;
		$sql = "select ips_site_id,count(*) AS cnt from wb_items_per_site where ips_item_id IN ($qs) group by ips_site_id" ;
		$result = getSQL ( $dbwd , $sql , 5 ) ;
		while($o = $result->fetch_object()){
			$out['data'][$o->ips_site_id][$cat] = $o->cnt ;
		}
	}
/*
	// THIS IS TOO SLOW
	$sql = "select catalog,count(*) AS cnt from entry where q>0 and user>0 group by catalog" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$out['total'][$o->catalog] = $o->cnt ;
	}
*/
} else if ( $query == 'missingpages' ) {

	$catalog = $mnm->escape ( get_request ( 'catalog' , 0 ) ) ;
	
	$sql = "SELECT DISTINCT q FROM entry WHERE q>0 AND user>0 AND catalog=$catalog AND q is not null" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $qs[''.$o->q] = $o->q ;

	if ( count($qs) > 0 ) {
		$dbwd = openDB ( 'wikidata' ) ;
		$site = $dbwd->real_escape_string ( get_request ( 'site' , '' ) ) ;
		$sql = "SELECT DISTINCT ips_item_id FROM wb_items_per_site WHERE ips_item_id IN (" . implode(',',$qs) . ") AND ips_site_id='$site'" ;
		$result = getSQL ( $dbwd , $sql , 5 ) ;
		while($o = $result->fetch_object()) {
			unset ( $qs[''.$o->ips_item_id] ) ;
		}
	}

	$out['data']['entries'] = [] ;
	$out['data']['users'] = [] ;
	$users = [] ;
	if ( count($qs) > 0 ) {
		$sql = "SELECT * FROM entry WHERE user>0 AND catalog=$catalog AND q IN (" . implode(',',$qs) . ")" ;
		$result = $mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$out['data']['entries'][$o->id] = $o ;
			if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
		}
		$out['data']['users'] = get_users ( $users ) ;
	}


} else if ( $query == 'get_sync' ) {

	$catalog = $mnm->escape ( get_request ( 'catalog' , 0 ) * 1 ) ;
	$sql = "SELECT * FROM catalog WHERE id=$catalog" ;
	$result = $mnm->getSQL ( $sql ) ;
//	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$prop = $o->wd_prop ;
		$qual = $o->wd_qual ;
	}
	
	if ( !isset($prop) ) $prop = '' ;
	if ( !isset($qual) ) $qual = '' ;
	
	if ( $prop == '' ) {
		$out['status'] = 'No Wikidata property defined for this catalog' ;
		if ( $callback != '' ) print $callback.'(' ;
		print json_encode ( $out ) ;
		if ( $callback != '' ) print ')' ;
		exit ( 0 ) ;
	}
	
	$out['data']['prop'] = $prop ;
	$out['data']['qual'] = $qual ;
	
	// Get Wikidata state
	$dont_compare_values = false ;
	$query = '' ;
	$the_prop = '' ;
	if ( $qual == '' ) {
		$query = "SELECT ?q ?prop { ?q wdt:P$prop ?prop }" ;
		$the_prop = $prop ;
	} else {
		$query = "SELECT ?q ?prop { ?q p:P1343 ?statement . ?statement ps:P1343 wd:Q$qual . ?statement pq:P$prop ?prop }" ;
		$the_prop = 1343 ;
		$dont_compare_values = true ;
	}
	$out['test'] = ['prop'=>$prop , 'query'=>$query] ;
	
	$wd = [] ;
#print "$query\n" ; exit(0);
	$j = getSPARQL ( $query ) ;
	foreach ( $j->results->bindings AS $b ) {
		if ( $b->q->type != 'uri' ) continue ;
		if ( $b->prop->type != 'literal' ) continue ;
		if ( !preg_match ( '/entity\/Q(\d+)$/' , $b->q->value , $m ) ) continue ;
		$q = $m[1] * 1 ;
//		if ( $dont_compare_values ) $wd[$q][] = '' ;
//		else 
		$wd[$q][] = $b->prop->value ;
	}
	unset ( $j ) ;


	// Get Mix-n-match state
	$mm = [] ;
	$sql = "select ext_id,q from entry where q is not null and q>0 and user!=0 and user is not null and catalog=$catalog and ext_id not like 'fake_id_%'" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		if ( $o->q*1 == 0 ) continue ; // Paranoia
		$mm[$o->q*1][] = $o->ext_id ;
	}
	
	if ( $dont_compare_values ) {
		foreach ( $wd AS $q => $a ) {
			if ( isset($mm[$q]) ) $wd[$q] = $mm[$q] ;
			else unset ( $mm[$q] ) ;
		}
	}

	// Report
	$out['data']['mm_dupes'] = [] ;
	foreach ( $mm AS $q => $v1 ) {
		if ( count($v1) == 1 ) continue ;
		$b = [] ;
		foreach ( $v1 AS $v2 ) {
			$b[] = utf8_encode($v2);
		}
		$out['data']['mm_dupes'][] = [$q*1 , $b] ;
		$bad_q[$q*1] = 1 ;
	}

	$out['data']['different'] = [] ;
	foreach ( $wd AS $q => $v1 ) {
		if ( !isset ( $mm[$q] ) ) continue ;
		if ( count($v1) == 1 and count($mm[$q]) == 1 and ( $dont_compare_values or $v1[0] == $mm[$q][0] ) ) continue ; // Same value
		sort($v1) ;
		sort($mm[$q]) ;
		if ( implode('|',$v1) == implode('|',$mm[$q]) ) continue ;
		$out['data']['different'][] = [$q , utf8_encode($v1) , utf8_encode($mm[$q])] ;
	}
#	print json_encode  ( $out ) ; exit ( 0 ) ;

	$out['data']['wd_no_mm'] = compareSets ( $wd , $mm ) ;
	$out['data']['mm_no_wd'] = compareSets ( $mm , $wd ) ;

	if ( 0 ) {
		$do_remove = [] ;
		foreach ( $out['data']['mm_no_wd'] AS $num => $v ) {
			if ( $mnm->hasPropertyEverEditedInItem("Q{$v[0]}","P{$prop}" ) ) $do_remove[] = $num ;
		}
		while ( count($do_remove) > 0 ) {
			$offset = array_pop($do_remove) ;
			$out['data']['mm_no_wd'][$offset] = $out['data']['mm_no_wd'][count($out['data']['mm_no_wd'])-1] ;
			array_pop ( $out['data']['mm_no_wd'] ) ;
		}
	}

	$out['data']['mm_double'] = [] ;
	$sql = "select q,count(*) as cnt,group_concat(id) AS ids from entry where q>0 and catalog=$catalog and q is not null and user is not null and user>0 group by q having cnt>1" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$out['data']['mm_double'][$o->q] = explode ( ',' , $o->ids ) ;
	}
	

} else if ( $query == 'get_sync_all' ) {

	$catalogs = [] ;
	$sql = "SELECT * FROM catalog WHERE wd_prop IS NOT NULL and wd_qual IS NULL" ;
#$sql .= " AND wd_prop=1415" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$catalogs[$o->id] = $o->wd_prop ;
	}
	
	$human_issue = [] ;
	$deleted = [] ;

	$dbwd = openDB ( 'wikidata' ) ;
	foreach ( $catalogs AS $cat => $prop ) {
		$query = 'claim[' . $prop . ']' ;
		$url = "$wdq_internal_url?q=" . urlencode ( $query ) ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		
		$list = [] ;
		$qlist = [] ;
		$sql = "SELECT * FROM entry WHERE catalog=$cat AND q is not null and q>0 and user!=0" ;
		if ( count($j->items) > 0 ) $sql .= " AND q NOT IN (" . implode ( ',' , $j->items ) . ")" ;
		$result = $mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$list[] = $o ;
			$qlist[] = $o->q ;
		}

		if ( count ( $list ) == 0 ) continue ;
		

		$tmp = [] ;
		$sql = "select epp_entity_id from wb_entity_per_page where epp_entity_type='item' and epp_entity_id IN (" . implode ( ',' , $qlist ) . ")" ;
		$result = getSQL ( $dbwd , $sql , 5 ) ;
		while($o = $result->fetch_object()){
			$tmp[''.$o->epp_entity_id] = 1 ;
		}

		$potential_people = [] ;		
		$sql = "select distinct epp_entity_id from wb_entity_per_page where epp_entity_type='item' and epp_entity_id IN (" . implode ( ',' , $qlist ) . ") and not exists (select * from pagelinks where pl_from=epp_page_id and pl_namespace=0 and pl_title='Q5')" ;
		$result = getSQL ( $dbwd , $sql , 5 ) ;
		while($o = $result->fetch_object()) $potential_people[] = $o->epp_entity_id ;

		if ( count($potential_people) > 0 ) {
			$sql = "SELECT * from entry WHERE `type`='Q5' and q in (" . implode(',',$potential_people) . ")" ;
			$result = $mnm->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) $human_issue[''.$o->q] = $o->q ;
		}
		
		foreach ( $qlist AS $q ) {
			if ( !isset ( $tmp[$q] ) ) $deleted[''.$q] = $q ;
		}

		foreach ( $list AS $o ) {
			if ( isset($deleted[$o->q]) ) continue ;
			if ( isset($human_issue[$o->q]) ) continue ;
			print "Q" . $o->q . "\tP$prop\t\"" . $o->ext_id . "\"\n" ;
		}
	}

	foreach ( $human_issue AS $q ) {
		print "HUMAN ISSUE: Q$q\n" ;
	}

	$updated = false ;
	foreach ( $deleted AS $q ) {
		if ( isset ( $_REQUEST['delete'] ) ) {
			$sql = "UPDATE entry SET q=null,user=null,timestamp=null WHERE q=$q" ;
			$result = $mnm->getSQL ( $sql ) ;
			$updated = true ;
		} else {
			print "DELETED: Q$q\n" ;
		}
	}

	if ( $updated ) $mnm->updateSingleCatalog ( $o->catalog ) ;

	exit ( 0 ) ;
	
} else if ( $query == 'get_entries_by_q_or_value' ) {
	$q = get_request ( 'q' , '' ) ;
	$json = json_decode ( get_request ( 'json' , '{}' ) ) ;
	
	$out['data']['entries'] = [] ;
	$out['data']['catalogs'] = [] ;
	

	$sql_parts = [] ;
	$sql_parts[] = "q=" . preg_replace('/\D/','',$q) ;

	if ( count($json) > 0 ) {
		$props = implode ( ',' , array_keys((array)$json) ) ;
		$props = preg_replace ( '/[^0-9,]/' , '' , $props ) ;
		$sql = "SELECT id,wd_prop FROM catalog WHERE wd_qual IS NULL AND active=1 AND wd_prop IN ($props)" ;
#		$out['sql_catalogs_prop'] = $sql ;
		$prop2catalog = [] ;
		$result = $mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) $prop2catalog['P'.$o->wd_prop][] = $o->id ;
		
		
		foreach ( $json AS $prop => $values ) {
			if ( count($values) == 0 ) continue ;
			if ( !isset($prop2catalog[$prop]) or count($prop2catalog[$prop]) == 0 ) continue ;
			foreach ( $values AS $k => $v ) $values[$k] = $mnm->escape ( $v ) ;
			$sql_parts[] = "catalog IN (".implode(',',$prop2catalog[$prop]).") AND ext_id IN ('".implode("','",$values)."')" ;
		}
	}
	
	$sql = "SELECT * FROM entry WHERE ((" . implode ( ') OR (' , $sql_parts ) . '))' ;
	$sql .= ' AND catalog NOT IN (SELECT id FROM catalog WHERE active!=1)' ;
	$sql .= ' ORDER BY catalog,id' ;
	$result = $mnm->getSQL ( $sql ) ;
	$catalog = [] ;
	while ( $o = $result->fetch_object() ) {
		$catalogs[$o->catalog] = 1 ;
		$out['data']['entries'][$o->id] = $o ;
	}
#	$out['sql'] = $sql ;
	
	if ( count($catalogs) > 0 ) {
		$sql = "SELECT * FROM catalog WHERE id IN (" . implode(',',array_keys($catalogs)) . ")" ;
		$result = $mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) $out['data']['catalogs'][$o->id] = $o ;
#		$out['sql_catalogs'] = $sql ;
	}

} else if ( $query == 'get_wd_props' ) {

	$props = [] ;
	$sql = "SELECT DISTINCT wd_prop FROM catalog WHERE wd_prop IS NOT NULL AND wd_qual IS NULL AND active=1 ORDER BY wd_prop" ;
	$result = $mnm->getSQL ( $sql ) ;
	while ( $o = $result->fetch_object() ) $props[] = $o->wd_prop*1 ;
	print json_encode($props) ;
	exit(0);
	
} else if ( $query == 'random' ) {

	$catalogs = [] ;
	$sql = "SELECT id FROM catalog WHERE active=1" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $catalogs[$o->id] = $o->id ;

	$submode = get_request ( 'submode' , '' ) ;
	$catalog = get_request ( 'catalog' , 0 ) * 1 ;
	$type = $mnm->escape ( get_request ( 'type' , '' ) ) ;
	$cnt = 0 ;
	$fail = 0 ;
	while ( 1 ) {
		if ( $fail ) break ; // No more results
		$r = rand() / getrandmax() ;
		if ( $cnt > 10 ) {
			$r = 0 ;
			$fail = 1 ;
		}
		$sql = "SELECT * FROM entry" ;
		$sql .= " FORCE INDEX (random_2)" ;
		$sql .= " WHERE random>=$r " ;
		if ( $submode == 'prematched' ) $sql .= " AND user=0" ;
		else if ( $submode == 'no_manual' ) $sql .= " AND ( user=0 or q is null )" ;
		else $sql .= " AND q IS NULL" ; // Default: unmatched
		if ( $catalog > 0 ) $sql .= " AND catalog=$catalog" ;
		if ( $type != '' ) $sql = " AND `type`='$type'" ;
		$sql .= " ORDER BY random limit 1" ;
if ( isset($_REQUEST['id']) ) $sql = "SELECT * FROM entry WHERE id=" . ($_REQUEST['id']*1) ; // FOR TESTING
		$out['sql'] = $sql ;
		if ( isset($_REQUEST['testing']) ) break ;
		$result = $mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			if ( !isset($catalogs[$o->catalog]) ) continue ; // Make sure catalog is active
			$out['data'] = $o ;
		}
		if ( count ( $out['data'] ) > 0 ) break ;
		$cnt++ ;
	}

	if ( count($out['data']) > 0 ) {
		$id = $out['data']->id ;
		$sql = "SELECT * FROM person_dates WHERE entry_id=$id" ;
		$result = $mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			if ( $o->born != '' ) $out['data']->born = $o->born ;
			if ( $o->died != '' ) $out['data']->died = $o->died ;
		}
	}

} else if ( $query == 'same_names' ) {

	$out['data']['entries'] = [] ;
	$out['data']['users'] = [] ;
	$users = [] ;
	$sql = "select ext_name,count(*) as cnt,SUM(if(q is not null or q=0, 1, 0)) AS matched from entry " ;
	if ( rand(0,10) > 5 ) $sql .= " where ext_name>'M' " ; // Hack to get more results
	$sql .= " group by ext_name having cnt>1 and cnt<10 and matched>0 and matched<cnt limit 10000" ;
	$result = $mnm->getSQL ( $sql ) ;
	$tmp = [] ;
	while($o = $result->fetch_object()) {
		$tmp[] = $o ;
	}
	
	$ext_name = $tmp[array_rand($tmp)]->ext_name ;
	$out['data']['name'] = $ext_name ;
	
	$sql = "SELECT * FROM entry WHERE ext_name='" . $mnm->escape($ext_name) . "'" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		$out['data']['entries'][] = $o ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}
	$out['data']['users'] = get_users ( $users ) ;


} else if ( $query == 'top_missing' ) {

	$out['data'] = [] ;
	$catalogs = get_request ( 'catalogs' , '' ) ;
	$catalogs = preg_replace ( '/[^0-9,]/' , '' , $catalogs ) ;
	if ( $catalogs == '' ) {
		$out['status'] = 'NO CATALOGS GIVEN' ;
		micDrop(true) ;
	}

	$sql = "/*top_missing*/ SELECT ext_name,count(DISTINCT catalog) AS cnt
			FROM entry
			WHERE catalog IN ({$catalogs})
			AND (q IS NULL or user=0)
			GROUP BY ext_name
			HAVING cnt>1
			ORDER BY cnt DESC
			LIMIT 500" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		$out['data'][] = $o ;
	}

} else if ( $query == 'creation_candidates' ) {

	$tries = 0 ;
	while ( 1 ) {
		$out['data'] = [] ;
		$tries++ ;
		if ( $tries > 250 ) { $out['status'] = 'NO RESULTS' ; break ; }
		$min = get_request ( 'min' , 0 ) * 1 ;
		$mode = trim ( get_request ( 'mode' , '' ) ) ;
		$ext_name_required = trim ( get_request ( 'ext_name' , '' ) ) ;
		$require_unset = get_request ( 'require_unset' , 0 ) * 1 ;
		$require_catalogs = preg_replace ( '/[^0-9,]/' , '' , get_request('require_catalogs','') ) ;
		$catalogs_required = get_request ( 'min_catalogs_required' , 0 ) * 1 ;
		$out['data']['entries'] = [] ;
		$out['data']['users'] = [] ;
		$users = [] ;
		
		$table = 'common_names' ;
		if ( $mode != '' ) $table .= '_' .  $mnm->escape ( $mode ) ;

		if ( $require_catalogs != '' ) {
exit(1); // DEACTIVATED, TOO EXPENSIVE
			$sql = "SELECT ext_name,count(DISTINCT catalog) AS cnt FROM entry WHERE catalog IN ({$require_catalogs})
					AND (q IS NULL or user=0)
					GROUP BY ext_name
					HAVING cnt>=3
					ORDER BY rand()
					LIMIT 1" ;
		} else {
			$sql = "select name AS ext_name,cnt FROM $table WHERE" ;
			if ( $min > 0 ) $sql .= " cnt>=$min AND" ;
			$sql .= " cnt<15 order by rand() limit 1" ;
		}

		if ( $mode == 'test' ) $sql = "select 'thomas wright' AS ext_name,20 as cnt" ;

		if ( $ext_name_required != '' ) $sql = "SELECT '" . $mnm->escape ( $ext_name_required ) . "' AS ext_name,20 as cnt" ;


		$result = $mnm->getSQL ( $sql ) ;
		$tmp = [] ;
		while($o = $result->fetch_object()) {
			$tmp[] = $o ;
		}
		
		$ext_name = $tmp[array_rand($tmp)]->ext_name ;
		$out['data']['name'] = $ext_name ;

		$names = [ $ext_name ] ;
		if ( preg_match ( '/^(\S+) (.+) (\S+)$/' , $ext_name , $m ) ) {
			$names[] = $m[1].'-'.$m[2].' '.$m[3] ;
			$names[] = $m[1].' '.$m[2].'-'.$m[3] ;
		}
		foreach ( $names AS $k => $v ) $names[$k] = $mnm->escape ( $v ) ;
		
		$sql = "/*creation_candidates*/ SELECT * FROM entry WHERE catalog IN (SELECT id FROM catalog WHERE catalog.active=1)" ;
		$sql .= " AND ext_name IN ('" . implode("','",$names) . "')" ;
		$sql .= " AND (q is null OR q!=-1)" ;
		if ( $mode == 'taxon' ) $sql .= " AND `type`='Q16521'" ;
		$result = $mnm->getSQL ( $sql ) ;
		$required_catalogs_found = [] ;
		$found_unset = 0 ;
		while($o = $result->fetch_object()) {
			$out['data']['entries'][$o->id] = $o ;
			if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
			if ( in_array($o->catalog, explode(',',$require_catalogs)) ) $required_catalogs_found[$o->catalog]++ ;
			if ( $o->user == 0 or !isset($o->q) or $o->q === null ) $found_unset++ ;
		}
		if ( $ext_name_required == '' ) {
			if ( $found_unset < $require_unset ) continue ;
			if ( count($required_catalogs_found) < $catalogs_required ) continue ;
		}
		$out['data']['users'] = get_users ( $users ) ;
		addExtendedEntryData() ;
		
		$out['data']['entries'] = array_values ( $out['data']['entries'] ) ;
		if ( $min == 0 or count($out['data']['entries']) >= $min ) break ;
		if ( $ext_name != '' ) break ; // Only one possible query to run
	}

} else if ( $query == 'disambig' ) {

	$catalog = get_request ( 'catalog' , 0 ) ;
	if ( $catalog != '' ) $catalog *= 1 ;
	
	$qs = '' ;
	$sql = "SELECT DISTINCT q FROM entry WHERE q IS NOT NULL and q>0 and user!=0" ;
	if ( $catalog != '' ) $sql .= " AND catalog=$catalog" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		if ( $qs != '' ) $qs .= "," ;
		$qs .= $o->q ;
	}
	$out['data']['qs'] = count($qs) ;
	
	$out['data']['entries'] = [] ;
	$out['data']['users'] = [] ;
	$sql = "SELECT DISTINCT epp_entity_id FROM wb_entity_per_page,pagelinks WHERE epp_entity_type='item' and epp_entity_id IN ($qs) and pl_from=epp_page_id AND pl_namespace=0 AND pl_title='Q4167410' ORDER BY rand() LIMIT 50" ;

	$qs = [] ;
	$dbwd = openDB ( 'wikidata' ) ;
	$result = getSQL ( $dbwd , $sql , 5 ) ;
	while($o = $result->fetch_object()) $qs[] = $o->epp_entity_id ;
	$dbwd->close() ;
	
	$users = [] ;
	$sql = "SELECT * FROM entry WHERE q IN (" . implode(',',$qs) . ") and user!=0" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		$out['data']['entries'][] = $o ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}
	$out['data']['users'] = get_users ( $users ) ;


} else if ( $query == 'locations' ) {

	header('Content-type: text/plain');
	
	$bbox = get_request ( 'bbox' , '' ) ;
	$bbox = preg_replace ( '/[^0-9,\.\-]/' , '' , $bbox ) ;
	$bbox = explode ( ',' , $bbox ) ;
	$out['bbox'] = $bbox ;
	
	if ( count($bbox) == 4 ) {
		$sql = "SELECT * FROM entry,location WHERE location.entry=entry.id" ;
		$sql .= " AND lon>={$bbox[0]}" ;
		$sql .= " AND lon<={$bbox[2]}" ;
		$sql .= " AND lat>={$bbox[1]}" ;
		$sql .= " AND lat<={$bbox[3]}" ;
		$sql .= " LIMIT 5000" ;
		$out['sql'] = $sql ;
		$out['data'] = [] ;
		$result = $mnm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$out['data'][] = $o ;
		}
	}

} else if ( $query == 'get_entry_by_extid' ) {

	$catalog = get_request ( 'catalog' , 0 ) * 1 ;
	$ext_id = $mnm->escape ( get_request ( 'extid' , '' ) ) ;

	$out['data']['entries'] = [] ;
	$sql = "SELECT * FROM entry WHERE catalog={$catalog} AND ext_id='{$ext_id}'" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		$out['data']['entries'][$o->id] = $o ;
	}
	addExtendedEntryData() ;

} else if ( $query == 'suggest' ) {

	header('Content-type: text/plain');
	$ts = date ( 'YmdHis' ) ;
	$catalog = get_request('catalog',0) * 1 ;
	$overwrite = get_request('overwrite',0) * 1 ;
	$suggestions = get_request ( 'suggestions' , '' ) ;
	$suggestions = explode ( "\n" , $suggestions ) ;
	$cnt = 0 ;
	foreach ( $suggestions AS $s ) {
		if ( trim($s) == '' ) continue ;
		$s = explode ( '|' , $s ) ;
		if ( count($s) != 2 ) {
			print "Bad row : " . implode('|',$s) . "\n" ;
			continue ;
		}
		$extid = trim ( $s[0] ) ;
		$q = preg_replace ( '/\D/' , '' , $s[1] ) ;
		$sql = "UPDATE entry SET q=$q,user=0,`timestamp`='$ts' WHERE catalog=$catalog AND ext_id='" . $mnm->escape($extid) . "'" ;
		if ( $overwrite == 1 ) $sql .= " AND (user=0 OR q IS NULL)" ;
		else $sql .= " AND (q IS NULL)" ;
		$result = $mnm->getSQL ( $sql ) ;
		$cnt += $mnm->dbm->affected_rows ;
	}
	print "$cnt entries changed" ;
	
	exit(0) ;

} else if ( $query == 'proxy' ) {
/*
	$url = get_request ( 'url' , '' ) ;
	$h = file_get_contents ( $url ) ;
	$h = str_replace ( '</head>' , '<base href="'.$url.'"></head>' , $h ) ;
	$h = str_replace ( 'http:' , 'https:' , $h ) ;
	print $h ;
	exit ( 0 ) ;
*/
} else {
	$out['status'] = 'Unknown action "' . $query . '"' ;
}

micDrop() ;


?>
