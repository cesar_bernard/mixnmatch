// ENFORCE HTTPS
if (location.protocol != 'https:') location.href = 'https:' + window.location.href.substring(window.location.protocol.length);

var current_language = 'en' ;
var api = './api.php' ;
var all_catalogs = {} ;
var item2group = {} ;
var widar ;


var itemGroupsMixin = {
	methods : {
		getItemGroupName : function ( s ) {
			if ( current_language != 'en' ) return s ;
			var ret = s.replace ( /^Wikidata property (to|for|related to) (a |an |the |link |to |indicate |identify |identification |in |items |about )*\s*/ , '' ) ;
			return ret ;
		} ,
		getCatalogProperties : function ( callback ) {
			var me = this ;
			if ( typeof item2group['ig_no_property'] != 'undefined' ) { // Already loaded
				callback() ;
				return ;
			}
			var prop_cache = {} ;
			item2group = {} ;
			item2group['ig_no_property'] = { catalogs:[] , label:'Catalogs without Wikidata property' } ;
			$.each ( all_catalogs , function ( k , v ) {
				if ( typeof v.wd_prop == 'undefined' || v.wd_prop == null || v.wd_prop == '' || v.wd_qual !== null ) {
					item2group['ig_no_property'].catalogs.push ( v.id ) ;
					return ;
				}
				var p = 'P'+v.wd_prop ;
				if ( typeof prop_cache[p] == 'undefined' ) prop_cache[p] = { catalogs:[] } ; // Yeah that's too complicated now, but too lazy to change it
				prop_cache[p].catalogs.push ( v.id ) ;
			} ) ;
			var tmp = [] ;
			$.each ( prop_cache , function ( p , v ) { tmp.push(p) } ) ;
			if ( tmp.length == 0 ) { // Paranoia
				fin();
				return;
			}
			var sparql = 'SELECT ?p ?v ?vLabel { VALUES ?p { wd:' + tmp.join(' wd:') + ' } . ?p rdf:type wikibase:Property ; wdt:P31 ?v SERVICE wikibase:label { bd:serviceParam wikibase:language "' + current_language + '" } }' ;
			$.post ( 'https://query.wikidata.org/sparql' , {
				format:'json',
				query:sparql
			} , function ( d ) {
				$.each ( d.results.bindings , function ( dummy , b ) {
					if ( b.p.type != 'uri' ) return ;
					if ( b.v.type != 'uri' ) return ;
					var p = b.p.value.replace(/^.+\//,'') ;
					var q = b.v.value.replace(/^.+\//,'') ;
					if ( typeof prop_cache[p] == 'undefined' ) return ; // Paranoia
					$.each ( prop_cache[p].catalogs , function ( dummy , cat ) {
						var group_name = me.getItemGroupName ( b.vLabel.value ) ;
						var group_key = 'ig_' +group_name.toLowerCase().replace(/\s+/g,'_') ;
						if ( typeof item2group[group_key] == 'undefined' ) item2group[group_key] = { catalogs:[] , label:group_name } ;
						item2group[group_key].catalogs.push ( cat ) ;
						// NOTE: q gets lost, no real use for it...
					} ) ;
				} ) ;

/*				
				// Fix plural (apparently, no such pairs to merge; leaving code in here just in case...)
				if ( current_language != 'en' ) return ; // Works for English only
				$.each ( item2group , function ( k , v ) {
					if ( !/s$/.test(k) ) return ;
					var k2 = k.replace(/s$/,'') ;
					if ( typeof item2group[k2] == 'undefined' ) return ;
					console.log ( k + ' / ' + k2 ) ;
				} ) ;
*/
			} , 'json' ) . always ( function () { callback() } ) ;

		}
	}
} ;

var entryMixin = {
	methods : {
		filteredName : function () {
			var ret = this.entry.ext_name ;
			ret = ret.replace ( /^(Sir|Madam|Madame|Saint) / , '' ) ;
			ret = ret.replace ( /\s*\(.+?\)\s*/ , ' ' ) ;
			return ret ;
		} ,
		getSearchString : function ( add_date_if_possible = true ) {
			var ret = this.filteredName() ;
			if ( typeof ret == 'undefined' ) return '' ;
			ret = ret.replace ( /\s*\(.+?\)\s*/g , ' ' ) ;
			ret = ret.replace ( /\s*\[.+?\]\s*/g , ' ' ) ;
			ret = ret.replace ( /\s+([A-Z]\s+)+/g ,' ' ) . replace ( /^[A-Z]\.{0,1} / , '' ) ;
			if ( this.entry.type == 'Q5' ) {
				if ( add_date_if_possible ) {
					var m = this.entry.ext_desc.match ( /\b\d{3,4}\b/g ) ;
					if ( m != null ) {
						$.each ( m , function ( k , v ) { m[k] = v*1 } ) ;
						m = m.sort ( function ( a , b ) { return a-b } ) ;
						while ( m.length > 1 && m[0]+150 < m[1] ) m.shift() ;
						if ( m.length == 1 ) ret += ' ' + m[0] ;
						else if ( m.length > 1 ) ret += ' ' + m[0] + ' ' + m[m.length-1] ;
					}
				}
			}
			return encodeURIComponent(ret) ;
		} ,
	}
} ;

var entryDisplayMixin = {
	filters : {
		decodeEntities : function(encodedString) {
			return $('<textarea />').html(encodedString).text() ;
		} ,
		removeTags : function(input) {
			return input.replace ( /<.+?>/g , '' ) ;
		} , 
		miscFixes : function(input) {
			return input.replace ( /\\\\/g , '' ) ;
		}
	} ,
} ;

var editEntryMixin = {
	data : function () { return { last_created_q:0 } } ,
	methods : {
		digits : function ( v , digits ) {
			v = '' + v ;
			while ( v.length < digits ) v = '0' + v ;
			return v ;
		} ,
		setEntryEdit : function ( entry ) {
			var d = new Date() ;
			var ts = ''+d.getFullYear()+
				this.digits(d.getMonth()+1,2)+
				this.digits(d.getDate(),2)+
				this.digits(d.getHours(),2)+
				this.digits(d.getMinutes(),2)+
				this.digits(d.getSeconds(),2);
			
			entry.username = widar.getUserName() ;
			entry.user = null ; // Dummy, not really used but checked for null/0/3/4
			entry.timestamp = ts ;
		} ,
		setEntryQ : function ( entry , q , skip_wikidata_edit , callback ) {
			var me = this ;
			q = ((''+q).replace(/\D/g,''))*1 ; // Ensure number
			if ( q <= 0 ) skip_wikidata_edit = true ;
	//			console.log ( this.entry.id + " => " + q ) ;
		
			var running = 1 ;
			function fin () {
				running-- ;
				if ( running > 0 ) return ;
				me.setEntryEdit ( entry ) ;
				entry.q = q ;
				if ( typeof callback != 'undefined' ) callback ( q ) ;
			}

			$.post ( api , {
				query:'match_q' ,
				tusc_user:widar.getUserName() ,
				entry:entry.id ,
				q:q
			} , function ( d ) {
				if ( d.status != 'OK' ) {
					alert ( d.status ) ;
					return ;
				}
				fin() ;
			} ) ;
		
			if ( skip_wikidata_edit ) return false ;
			var catalog = all_catalogs[entry.catalog] ;
			if ( catalog.wd_prop == null ) return false ;
			if ( catalog.wd_qual != null ) return false ;
		
			running++ ;
			var summary = 'Matched to [[:toollabs:mix-n-match/#/entry/' + entry.id + '|' + entry.ext_name + ' (#' + entry.id + ')]]' ;
			var params = { botmode:1 , action:'set_string' , id:'Q'+q , prop:'P'+catalog.wd_prop , text:entry.ext_id , summary:summary } ;
			widar.run ( params , function ( d ) {
				if ( d.error != 'OK' ) {
					alert ( d.error ) ;
					return ;
				}
				fin() ;
			} ) ;
		
		} ,
		
		date2statement : function ( prop , d ) {
			var precision = 9 ; // 11=day, 10=month , 9=year
			var m ;
			
			var year ;
			var month = '01' ;
			var day = '01' ;
			
			m = d.match ( /^(\d+)-(\d+)-(\d+)$/ ) ;
			if ( m != null ) {
				precision = 11 ;
				year = '' + (m[1]*1) ;
				month = '' + (m[2]*1) ;
				day = '' + (m[3]*1) ;
			} else {
				m = d.match ( /^(\d+)-(\d+)$/ ) ;
				if ( m != null ) {
					precision = 10 ;
					year = '' + (m[1]*1) ;
					month = '' + (m[2]*1) ;
				} else {
					m = d.match ( /^(\d+)$/ ) ;
					if ( m != null ) {
						precision = 9 ;
						year = '' + (m[1]*1) ;
					} else return ; // No match
				}
			}
			
			if ( month.length == 1 ) month = '0' + month ;
			if ( day.length == 1 ) day = '0' + day ;
			var t = '+' + year + '-' + month + '-' + day + 'T00:00:00Z' ;
			
			
			return {
				mainsnak: {
					snaktype : 'value' ,
					property : prop ,
					datavalue : {
						value : {
							'time' : t ,
							'timezone' : 0 ,
							'before' : 0 ,
							'after' : 0 ,
							'precision' : precision ,
							'calendarmodel' : 'http://www.wikidata.org/entity/Q1985727'
						} ,
						type : 'time'
					} ,
					datatype : 'time' // Needed?
				} ,
				type : 'statement' ,
				rank : 'normal'
			} ;
		} ,

		getItemClaim : function ( p , q ) {
			return {
					mainsnak: {
						snaktype : 'value' ,
						property : 'P'+(''+p).replace(/\D/g,'') ,
						datavalue : {
							value : {
								'entity-type' : 'item' ,
								'numeric-id' : (''+q).replace(/\D/g,'')*1 ,
								'id' : 'Q'+(''+q).replace(/\D/g,'')
							} ,
							type : 'wikibase-entityid'
						} ,
						datatype : 'wikibase-item' // Needed?
					} ,
					type : 'statement' ,
					rank : 'normal'
				} ;
		} ,

		newItemForEntry : function ( entry , callback , other_statements = {} ) {
			var me = this ;
			let entry_ids = [] ;
			let first_entry ;
			if ( typeof entry.id == 'undefined' ) {
				$.each ( entry , function ( k , e ) {
					entry_ids.push ( e.id ) ;
					if ( typeof first_entry == 'undefined' ) first_entry = e ;
				} ) ;
			} else {
				entry_ids.push ( entry.id ) ;
				first_entry = entry ;
			}
			$.get ( './api.php' , {
				query:'prep_new_item',
				entry_ids:entry_ids.join(',')
			} , function ( d ) {
				if ( d.status != 'OK' || typeof d.data == 'undefined' ) {
					alert ( "Problem creating item" ) ;
					return ;
				}
				var params = { 
					action:'wbeditentity',
					'new':'item',
					data:d.data
				} ;
				var summary = 'New item based on [[:toollabs:mix-n-match/#/entry/' + first_entry.id + '|' + first_entry.ext_name + ' (#' + first_entry.id + ')]]' ;
				params = {
					action : 'generic' ,
					summary : summary ,
					json : JSON.stringify ( params )
				}

				widar.run ( params , function ( d ) {
					if ( d.error != 'OK' ) {
						alert ( d.error ) ;
						return ;
					}
					var q = d.last_res.entity.id.replace ( /\D/g , '' ) ;
					if ( typeof q == 'undefined' || q == 0 || q === null ) return ;
					me.last_created_q = q ;
					me.setEntryQ ( first_entry , q , true , callback ) ;
				} ) ;

			} , 'json' ) ;
		} ,
		confirmEntryQ : function ( entry , callback ) {
			var do_skip = all_catalogs[entry.catalog].wd_prop==null || all_catalogs[entry.catalog].wd_qual!=null ;
			this.setEntryQ ( entry , entry.q , do_skip , callback ) ;
		} ,
		removeEntryQ : function ( entry , callback ) {
			$.post ( api , {
				query:'remove_q' ,
				tusc_user:widar.getUserName() ,
				entry:entry.id
			} , function ( d ) {
				if ( d.status != 'OK' ) {
					alert ( d.status ) ;
				} else {
					entry.q = null ;
					entry.user = null ;
					entry.username = null ;
					entry.timestamp = null ;
				}
				if ( typeof callback != 'undefined' ) callback() ;
			} , 'json' ) ;
		}
	}
} ;


//________________________________________________________________________________________________________________________________________________

Vue.component ( 'catalog-entry-multi-match' , {
	props : [ 'entry' ] ,
	data : function () { return { loaded:false , ld:{} , display:false } } ,
	mixins: [entryDisplayMixin,editEntryMixin,entryMixin] ,
	created : function () {
		var me = this ;
		me.loaded = false ;
		me.display = false ;
		if ( typeof me.entry.multimatch == 'undefined' || me.entry.multimatch.length == 0 ) return ;
		if ( typeof me.entry.q != 'undefined' && me.entry.user > 0 ) return ; // Already set
		me.display = true ;
		var titles = [] ;

		wd.getItemBatch ( me.entry.multimatch , function () {
			me.ld = {} ;
			var language = all_catalogs[me.entry.catalog].search_wp ;
			$.each ( me.entry.multimatch , function ( dummy , q ) {
				let v = wd.getItem(q) ;
				if ( typeof v == 'undefined' ) return ;
				v = v.raw ;

				var o = { label:q , description:'' } ;
				$.each ( ['label','description'] , function ( dummy , key ) {
					var key2 = key + 's' ;
					if ( typeof v[key2] == 'undefined' ) return ;
					$.each ( v[key2] , function ( lang , v2 ) { if ( key == 'label' ) o[key] = v2.value ; } ) ; // All languages for label
					$.each ( v[key2] , function ( lang , v2 ) { if ( $.inArray(lang,['en','de','es','it','fr','nl']) !== -1 ) o[key] = v2.value ; } ) ; // Major languages
					$.each ( v[key2] , function ( lang , v2 ) { if ( lang == language ) o[key] = v2.value ; } ) ; // Catalog language
					$.each ( v[key2] , function ( lang , v2 ) { if ( lang == tt.language ) o[key] = v2.value ; } ) ; // Interface language
				} ) ;
				if ( typeof v.claims != 'undefined' ) {
					var dates = ['',''] ;
					if ( typeof v.claims.P569 != 'undefined' && typeof v.claims['P569'][0].mainsnak.datavalue != 'undefined' ) dates[0] = (v.claims['P569'][0].mainsnak.datavalue.value.time.match(/^[\+]{0,1}(.\d+)/))[1] ;
					if ( typeof v.claims.P570 != 'undefined' && typeof v.claims['P570'][0].mainsnak.datavalue != 'undefined' ) dates[1] = (v.claims['P570'][0].mainsnak.datavalue.value.time.match(/^[\+]{0,1}(.\d+)/))[1] ;
					if ( dates[0] + dates[1] != '' ) o.description += ' (' + dates[0] + '–' + dates[1] + ')' ;
				}
				me.ld[q] = o ;
			} ) ;
			me.loaded = true ;
		} ) ;
	} ,
	updated : function () { tt.updateInterface(this.$el) } ,
	methods : {
		setQ : function ( q , skip_wikidata_edit ) {
			var me = this ;
			me.editing = true ;
			me.setEntryQ ( me.entry , q , skip_wikidata_edit , me.stopEditing ) ;
			me.entry.username = widar.getUserName() ;
			me.entry.q = q ;
			me.display = false ;
			return false ;
		} ,
		setUserQ : function ( e , mq ) {
			var me = this ;
			e.preventDefault() ;
			var q = mq.replace ( /\D/g , '' ) ;
			if ( q != '' ) me.setQ ( q ) ;
			return false ;
		} ,
	} ,
	template : '#catalog-entry-multi-match-template'
} ) ;

Vue.component ( 'catalog-actions-dropdown' , {
	props : [ 'catalog' ] ,
//	created : function () { tt.updateInterface(this.$el) } ,
//	updated : function () { tt.updateInterface(this.$el) } ,
//	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		getSearchCatalogPath : function () {
			var me = this ;
			return "/search/?include="+me.catalog.id ;
/*			var exclude = [] ;
			$.each ( all_catalogs , function ( k , v ) {
				if ( v.id != me.catalog.id ) exclude.push ( v.id ) ;
			} ) ;
			return "/search/ /"+exclude.join(',') ;*/
		} ,
		click : function ( mode , mode2 ) {
			alert ( "Not implemented yet!" ) ; return false ;
/*
			var path ;
			if ( typeof mode2 == 'undefined' ) path ='/' + mode + '/' + catalog.id ;
			else path = '/' + mode + '/' + catalog.id + '/' + mode2 ;
			router.push ( path ) ;
			return false ;
*/
		}
	} ,
	template : '#catalog-actions-dropdown-template'
} ) ;

Vue.component ( 'catalog-list-item' , {
	props : [ 'cid' ] ,
	data : function () { return { catalog:{} } } ,
	created : function () {
		this.catalog = all_catalogs[this.cid] ;
	} ,
	methods : {
		click : function ( id , mode , mode2 ) {
			if ( typeof mode2 == 'undefined' ) router.push ( '/' + mode + '/' + id ) ;
			else router.push ( '/' + mode + '/' + id + '/' + mode2 ) ;
			return false ;
		} ,
		renderPercentage : function ( v ) {
			var me = this ;
			var ret = Math.floor(100*v/me.catalog.total) ;
			if ( ret == 0 ) return '' ;
			return ret + "%" ;
		}
	} ,
	template : '#catalog-list-item-template' ,
} ) ;


Vue.component ( 'entry-list-item' , {
	mixins: [entryDisplayMixin,editEntryMixin,entryMixin] ,
	props : [ 'entry' , 'show_catalog' , 'show_permalink' , 'show_checkbox' , 'rc' , 'twoline' , 'hide_remove_on_automatch' ] ,
	data : function () { return { editing:false } } ,
	updated : function () { tt.updateInterface(this.$el) } ,
	methods : {
	
		canCreateNewWikidataItem : function () {
			var me = this ;
			var cat = all_catalogs[me.entry.catalog] ;
			if ( typeof cat.wd_prop != 'undefined' && cat.wd_qual == null && !me.entry.ext_id.match(/^fake_id_/) ) return true ;
			return false ;
		} ,
		stopEditing : function () {
			this.editing = false ;
		} ,
		setQ : function ( q , skip_wikidata_edit ) {
			var me = this ;
			me.editing = true ;
			me.setEntryQ ( me.entry , q , skip_wikidata_edit , me.stopEditing ) ;
			return false ;
		} ,
		confirmQ : function () {
			var me = this ;
			var q = (''+me.entry.q).replace ( /\D/g , '' ) ;
			if ( q == '' ) return false ;
			me.setQ ( q ) ;
			return false ;
		} ,
		setUserQ : function ( e ) {
			var me = this ;
			e.preventDefault() ;
			var reply = prompt ( tt.t('enter_q_number') , "" ) ;
			if ( reply === null ) return false ;
			q = reply.replace ( /\D/g , '' ) ;
			if ( q == '' ) return false ;
			me.setQ ( q ) ;
			return false ;
		} ,
		setNA : function ( e ) {
			e.preventDefault() ;
			return this.setQ(e,0) ;
		} ,
		newItem : function ( e ) {
			e.preventDefault() ;
			var me = this ;
			me.editing = true ;
			me.newItemForEntry ( me.entry , me.stopEditing ) ;
			return false ;
		} ,
		removeQ : function ( e ) {
			e.preventDefault() ;
			var me = this ;
			me.editing = true ;
			me.removeEntryQ ( me.entry , me.stopEditing ) ;
			return false ;
		} ,
		wikipediaSearch : function () {
			var lang = all_catalogs[this.entry.catalog].search_wp ;
			return "https://"+lang+".wikipedia.org/w/index.php?title=Special%3ASearch&search=" + this.getSearchString(false) ;
		} ,
		wikidataSearch: function () {
			let query = this.entry.ext_name ;
			if ( this.entry.type == 'Q5' ) query += " haswbstatement:P31=Q5" ;
			let ret = "https://www.wikidata.org/w/index.php?button=&title=Special%3ASearch&search="+encodeURIComponent(query) ;
			return ret ;
		}
	} ,
	template : '#entry-list-item-template' ,
} ) ;



Vue.component ( 'catalog-header' , {
	props : [ 'catalog' , 'nolink' ] ,
	template : '#catalog-header-template'
} ) ;


Vue.component ( 'userlink' , {
	props : [ 'username' ] ,
	template : '<span>'+
	'<span v-if="username==\'Auxiliary data matcher\'">Auxiliary data matcher</span>'+
	'<span v-else-if="username==\'automatic\'">Automatic, preliminary matcher</span>'+
	'<span v-else-if="username==\'Automatic name/date matcher\'">Automatic name/date matcher</span>'+
	'<a v-else :href="\'https://www.wikidata.org/wiki/User:\'+encodeURIComponent(username.replace(/ /g,\'_\'))" target="_blank" class="wikidata">{{username}}</a>'+
	'</span>'
} ) ;

Vue.component ( 'widar' , {
	data : function () { return { is_logged_in:false , userinfo:{} , widar_api:'/widar/index.php' , loaded:false , is_catalog_admin:false } } ,
	created : function () {
		widar = this ;
		this.checkLogin()
	} ,
	updated : function () { tt.updateInterface(this.$el) } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		checkLogin : function () {
			var me = this ;
			$.get ( me.widar_api , {
				action:'get_rights',
				botmode:1
			} , function ( d ) {
				me.loaded = true ;
				if ( typeof d.result.query != 'undefined' && typeof d.result.query.userinfo != 'undefined' ) {
					me.is_logged_in = true ;
					me.userinfo = d.result.query.userinfo ;
					$.get ( './api.php' , {
						query:'get_user_info',
						username:me.getUserName()
					} , function ( d ) {
						if ( typeof d.data != 'undefined' && typeof d.data.is_catalog_admin != 'undefined' ) me.is_catalog_admin = d.data.is_catalog_admin ? true : false ;
					} , 'json' ) ;
				} else {
				}
			} , 'json' ) ;
		} ,
		run : function ( params , callback ) {
			var me = this ;
			params.tool_hashtag = "mix'n'match" ;
			params.botmode = 1 ;
			$.post ( me.widar_api , params , function ( d ) {
				if ( d.error != 'OK' ) {
					console.log ( params ) ;
					if ( null != d.error.match(/Invalid token/) || null != d.error.match(/happen/) || null != d.error.match(/Problem creating item/) || ( params.action!='create_redirect' && null != d.error.match(/failed/) ) ) {
						console.log ( "ERROR (re-trying)" , params , d ) ;
						setTimeout ( function () { me.run ( params , callback ) } , 500 ) ; // Again
					} else {
						console.log ( "ERROR (aborting)" , params , d ) ;
//						var h = "<li style='color:red'>ERROR (" + params.action + ") : " + d.error + "</li>" ;
//						$('#out ol').append(h) ;
						callback ( d ) ; // Continue anyway
					}
				} else {
					callback ( d ) ;
				}
			} , 'json' ) . fail(function() {
				console.log ( "Again" , params ) ;
				me.run ( params , callback ) ;
			}) ;
		} ,
		getUserName : function () { return this.userinfo.name }
	} ,
	template : '#widar-template'
} ) ;


Vue.component ( 'catalog-slice' , {
	props : [ 'catalogs' , 'title' , 'order','section' ] ,
	methods : {
		getTopMissingURL : function () {
			var me = this ;
			return '/top_missing/' + me.catalogs.join(',') ;
		}
	} ,
	template : '#catalog-slice-template'
} ) ;

Vue.component ( 'catalog-list-nav' , {
	template : '#catalog-list-nav-template' ,
	props : [ "id" , "start" , "mode" , "max" , "batch" , "base" , "type" ] ,
	data : function () { return { show:[] , type_key:'|' } } ,
	created : function () { this.update() } ,
	methods : {
		update : function () {
			var me = this ;
			if ( typeof me.type == 'undefined' ) me.type_key = '|' ;
			else me.type_key = me.type ;
			me.show = [] ;
			if ( typeof me.start == 'undefined' ) me.start = 0 ;
			me.start *= 1 ;
			me.batch *= 1 ;
			me.max *= 1 ;
			var s = [] ;
			var current_page = me.start*1 ;
			var last_page = Math.floor ( (me.max-1)/me.batch ) ;
			for ( var p = 0 ; p <= last_page ; p++ ) {
				let url = '/'+me.base+'/'+me.id+'/'+(me.mode||'')+'/'+p ;
				if ( me.type_key != '|' ) url += '?type='+me.type ;
				var o = {
					url : url ,
					label : (p+1) , // p*me.batch+'/'+me.start ,
					p:p,
					'class' : current_page==p ? 'page-item disabled' : 'page-item'
				} ;
				s[p] = o ;
			}
			if ( last_page > 15 ) {
				var side = 5 ;
				if ( current_page+side+1 < last_page ) {
					s.splice ( current_page+side , last_page-(current_page+side) , { label:'...',class:'page-item disabled' } ) ;
				}
				if ( current_page >= side ) {
					s.splice ( 1 , current_page-side , { label:'...',class:'page-item disabled' } ) ;
				}
			}
			me.show = s ;
		} ,
		getOptionLabel : function ( t ) {
			let me = this ;
			if ( t == '' ) return tt.t('no_type_specified');
			if ( !/^Q\d+$/.test(t) ) return t ;
			wd.getItemBatch ( [t] , function () {
				let label = t ;
				if ( typeof wd.getItem(t) != 'undefined' ) label = wd.getItem(t).getLabel() +' ['+t+']' ;
				$('option[value="'+t+'"]').text(label) ;
			} ) ;
			return t ;
		} ,
		changeType : function () {
			var me = this ;
			let path = '/'+me.base+'/'+me.id+'/'+(me.mode||'') ;
			if ( typeof me.start != 'undefined' ) path += '/'+me.start ;
			path += me.type_key == '|' ? '' : '?type='+me.type_key ;
			me.$router.push({path:path}) ;
		} ,
		jumpTo : function ( page ) {
			this.$emit('navlink-clicked',page) ;
		}
	} ,
	watch: {
		'$route' (to, from) { this.update() } ,
		'start' (to, from) { this.update() }
	} ,
} ) ;



var CatalogGroup = Vue.extend ( {
	mixins : [itemGroupsMixin] ,
	props : [ "key" , "order" ] ,
	data : function () { return { slices:[] , types:[] } } ,
	created : function () {
		var me = this ;
		me.getCatalogProperties ( me.init ) ;
	} ,
	updated : function () { tt.updateInterface(this.$el) ; } ,
	mounted : function () { tt.updateInterface(this.$el) ; } ,
	methods : {
		init : function () {
			var me = this ;
			me.slices = me.calculateSlices() ;
		} ,
		isItemGroup : function () {
			return /^ig_/.test ( this.key ) ;
		} ,
		calculateSlices : function () {
			var me = this ;
			var slices = [] ;
			me.types = ['all'] ;
		
			var by_type = {} ;
			if ( me.key == 'all' ) by_type.all = [] ;
			$.each ( all_catalogs , function ( k , v ) {
				if ( v.type != me.key && me.key!='all' ) return ;
				if ( typeof by_type[v.type] == 'undefined' ) by_type[v.type] = [] ;
				by_type[v.type].push ( v.id ) ;
				if ( me.key == 'all' ) by_type.all.push ( v.id ) ;
			} ) ;
			
			var tmp_types = {} ;
			$.each ( all_catalogs , function ( k , v ) { tmp_types[v.type] = 1 } ) ;
			$.each ( tmp_types , function ( k , v ) { me.types.push ( k ) } ) ;
			me.types.sort() ;
			
			var order = [] ;
			$.each ( by_type , function ( k , v ) {
				order.push ( k ) ;
			} ) ;
			order = order.sort ( function ( a , b ) {
				return by_type[b].length - by_type[a].length ;
			} ) ;
		
			slices = [] ;
			if ( me.isItemGroup() ) {
				slices.push ( { title:me.ucFirst(item2group[me.key].label) , catalogs:item2group[me.key].catalogs } ) ;
			} else {
				$.each ( order , function ( dummy , type ) {
					slices.push ( { title:me.ucFirst(type) , catalogs:by_type[type] } ) ;
				} ) ;
			}

			var order = me.order ;
			if ( typeof me.order == 'undefined' || me.order == '' ) order = 'order_easy' ; // Default
			$.each ( slices , function ( k , slice ) {
				slice.catalogs = slice.catalogs.sort ( function ( a , b  ) {
					if ( order=='order_id' ) return a*1-b*1 ;
					else if ( order == 'order_easy' ) return me.getEasy(all_catalogs[a])-me.getEasy(all_catalogs[b]) ;
					else if ( order == 'order_alpha' ) return all_catalogs[a].name.toUpperCase().localeCompare ( all_catalogs[b].name.toUpperCase() ) ;
				} ) ;
			} ) ;
			
			if ( me.completed == 'completed_section' ) {
				var completed = [] ;
				$.each ( slices , function ( k , v ) {
					var keep = [] ;
					$.each ( v.catalogs , function ( k2 , v2 ) {
						if ( me.isComplete ( all_catalogs[v2] ) ) completed.push ( v2 ) ;
						else keep.push ( v2 ) ;
					} ) ;
					slices[k].catalogs = keep ;
				} ) ;
				slices.push ( { title:'Completed' , catalogs:completed } ) ;
			}

			// Final touches
			$.each ( slices , function ( k , slice ) {
				slices[k].order = order ;
				slices[k].key = slice.title+'/'+order ;
				slices[k].section = 'section'+k ;
			} ) ;
		
			return slices ;
		} ,
		isComplete : function ( catalog ) {
			return ( catalog.noq + catalog.autoq ) == 0 ;
		} ,
		getEasy : function ( catalog ) { // calculate easyness to complete
			var ret = catalog.noq*2 + catalog.autoq*1 ;
			if ( ret == 0 ) ret = catalog.total * 100000 ; // Completed, go to bottom
			return ret ;
		} ,
		setGroupFromSelect : function () {
			var me = this ;
			var new_group = $('#select_catalog_group').val() ;
			var path = '/group/' + new_group ;
			if ( typeof me.order != 'undefined' ) path += '/' + me.order ;
			me.$router.push({path:path}) ;
		} ,
		ucFirst : function ( s ) {
			return s.charAt(0).toUpperCase() + s.slice(1).replace(/_/g,' ') ;
		}
	} ,
	watch: {
		'$route' (to, from) { this.init() }
	} ,
	template: '#catalog-group-template'
} ) ;

var MainPage = Vue.extend ( {
	mixins : [itemGroupsMixin] ,
	data : function () { return { types:[] , type2count:{} , search_query:'' , catalog_search_results:[] , item_groups_list:[] , lastest_catalogs:[] } } ,
	created : function () {
		var me = this ;
		me.getCatalogProperties ( me.init ) ;
	} ,
	updated : function () { tt.updateInterface(this.$el) ; } ,
	mounted : function () { tt.updateInterface(this.$el) ; $('#catalog_search_query_input').focus(); } ,
	methods : {
		init : function () {
			var me = this ;
			me.lastest_catalogs = [] ;
			$.each ( all_catalogs , function ( id , dummy ) { me.lastest_catalogs.push ( id ) } ) ;
			me.lastest_catalogs.sort ( function ( a , b ) {
				return b - a ;
			} ) ;
			while ( me.lastest_catalogs.length > 10 ) me.lastest_catalogs.pop() ;
			me.item_groups_list = [] ;
			$.each ( item2group , function ( k , v ) { me.item_groups_list.push ( k ) } ) ;
			me.item_groups_list.sort ( function (x,y) {
				return item2group[y].catalogs.length - item2group[x].catalogs.length ;
			} ) ;
			me.slices = me.calculateSlices() ;
		} ,
		calculateSlices : function () {
			var me = this ;
			me.types = [] ;
			me.type2count = {} ;
			$.each ( all_catalogs , function ( k , v ) { if(typeof me.type2count[v.type]=='undefined')me.type2count[v.type]=1;else me.type2count[v.type]++; } ) ;
			$.each ( me.type2count , function ( k , v ) { me.types.push ( k ) } ) ;
			me.types.sort() ;
		} ,
		updateSearchResults : function () {
			var me = this ;
			if ( $.trim(me.search_query) == '' ) {
				me.catalog_search_results = [] ;
				return ;
			}
			var max_results = 10 ;
			var patt = new RegExp ( me.search_query , 'i' ) ;
			var results = [] ;
			$.each ( all_catalogs , function ( k , v ) { // Search names first
				if ( results.length >= max_results ) return false ;
				if ( !patt.test ( v.name ) ) return ;
				if ( $.inArray(v.id, results) === -1 ) results.push ( v.id ) ;
			} ) ;
			$.each ( all_catalogs , function ( k , v ) { // Search descriptions if necessary
				if ( results.length >= max_results ) return false ;
				if ( !patt.test ( v.desc ) ) return ;
				if ( $.inArray(v.id, results) === -1 ) results.push ( v.id ) ;
			} ) ;
			me.catalog_search_results = results ;
		} ,
		ucFirst : function ( s ) {
			return s.charAt(0).toUpperCase() + s.slice(1).replace(/_/g,' ') ;
		}

	} ,
	watch: {
		'$route' (to, from) { this.init() } ,
		'search_query' () { this.updateSearchResults() } ,
	} ,
	template: '#main-page-template'
} ) ;


var CatalogDetails = Vue.extend ( {
	template : '#catalog-details-template' ,
	props : [ 'id' ] ,
	data : function () { return { catalog:{} , meta:{} , loaded:false , deactivated:false } } ,
	created : function () {
		this.catalog = all_catalogs[this.id] ; // Seed
		this.loadCatalog ( this.id ) ;
	} ,
	updated : function () { tt.updateInterface(this.$el) } ,
	mounted : function () { tt.updateInterface(this.$el) ; } ,
	methods : {
		loadCatalog : function ( id ) {
			var me = this ;
			me.loaded = false ;
			if ( typeof all_catalogs[id] == 'undefined' ) {
				me.deactivated = true ;
				return ;
			}
			me.deactivated = false ;
			me.doLoadCatalog ( id ) ;
		} ,
		doLoadCatalog : function ( id ) {
			var me = this ;
			$.get ( api , { query:'catalog_details' , catalog:id } , function ( d ) {
				me.loaded = true ;
				me.meta = d.data ;
				me.catalog = all_catalogs[id] ;
			} ) .fail(function() {
				setTimeout ( function () { me.doLoadCatalog(id) } , 200 ) ;
			} ) ;
		} ,
		updateStats : function () {
			var me = this ;
			$.get ( api , {
				query:'update_overview',
				catalog:me.id
			} , function ( d ) {
				loadOverview ( function () {
					me.loadCatalog ( me.id ) ;
				} ) ;
			} , 'json' ) ;
		}
	} ,
	watch: {
		'$route' (to, from) {
			this.loadCatalog ( to.params.id ) ;
		}
	}
} ) ;



var Entry = Vue.extend ( {
	template : '#entry-template' ,
	props : [ 'id' ] ,
	data : function () { return { entry:{} , catalog:{} , loaded:false } } ,
	created : function () {
		this.loadEntry ( this.id ) ;
	} ,
	updated : function () { tt.updateInterface(this.$el) } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		loadEntry : function ( id ) {
			var me = this ;
			$.get ( api , { query:'get_entry' , entry:id } , function ( d ) {
				if ( typeof d.data.entries[id] == 'undefined' ) {
					console.log ( "Entry " + id + " not found" ) ;
					return ;
				}
				$.each ( d.data.entries , function ( k , v ) {
					if ( typeof d.data.users[v.user] == 'undefined' ) return ;
					d.data.entries[k].username = d.data.users[v.user].name ;
				} ) ;
				me.entry = d.data.entries[id] ;
				me.catalog = all_catalogs[me.entry.catalog] ;
				me.loaded = true ;
			} ) ;
		}
	} ,
	watch: {
		'$route' (to, from) {
			this.loadEntry ( to.params.id ) ;
		}
	}
} ) ;

var SparqlList = Vue.extend ( {
	template : '#sparql-list-template' ,
	props : [ 'sparql' ] ,
	data : function () { return { entries:{} , entries_subset:{} , catalog:{} , sparql_query:'' , loaded:false , per_page:50 , start:0 , number_of_entries:0 , loading:false } } ,
	created : function () { this.loadSparqlList ( this.sparql ) } ,
	updated : function () { tt.updateInterface(this.$el) } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		loadSparqlList : function ( sparql ) {
			var me = this ;
			me.loaded = false ;
			if ( typeof sparql != 'undefined' ) me.sparql_query = me.sparql ;
			if ( me.sparql_query == '' ) return ;

			me.loading = true ;
			$.get ( api , { query:'sparql_list' , sparql:me.sparql_query } , function ( d ) {
				me.start = 0 ;
				me.number_of_entries = 0 ;
				$.each ( d.data.entries , function ( k , v ) {
					if ( typeof d.data.users[v.user] == 'undefined' ) return ;
					d.data.entries[k].username = d.data.users[v.user].name ;
					me.number_of_entries++ ;
				} ) ;
				me.entries = d.data.entries ;
				me.showSubset ( 0 ) ;
			} ) .fail ( function ( a,b,c ) {
				console.log ( 'Fail' , a , b , c )
			} ) . always ( function () {
				me.loaded = true ;
				me.loading = false ;
				tt.updateInterface(me.$el);
			} );

		} ,
		showSubset : function ( page ) {
			var me = this ;
			var subset = {} ;
			var cnt = 0 ;
			me.start = page ;
			var first = me.per_page * me.start ;
			$.each ( me.entries , function ( k , v ) {
				cnt++ ;
				if ( (cnt-1)<first ) return ;
				if ( (cnt-1)>=first+me.per_page ) return false ;
				subset[k] = v ;
			} ) ;
			me.entries_subset = subset ;
		} ,
		setPage : function ( event ) {
			var me = this ;
			me.showSubset ( event*1 ) ;
		} ,
		onRun : function () {
			var me = this ;
			router.push ( '/sparql/' + encodeURIComponent(me.sparql_query) ) ;
		}
	} ,
	watch: {
		'$route' (to, from) {
			this.loadSparqlList ( to.params.sparql ) ;
		}
	}
} ) ;



var CatalogList = Vue.extend ( {
	template : '#catalog-list-template' ,
	props : [ 'id' , 'mode' , 'start' ] ,
	data : function () { return { entries:{} , catalog:{} , loaded:false , per_page:50 , 
		mode2prop : { manual:'manual' , auto:'autoq' , unmatched:'unmatched' , nowd:'nowd' , na:'na' , multi_match:'multi_match' , type:undefined } ,
		modes: {
			manual:{show_noq:0,show_autoq:0,show_userq:1,show_na:0,show_multiple:0},
			auto:{show_noq:0,show_autoq:1,show_userq:0,show_na:0,show_multiple:0},
			unmatched:{show_noq:1,show_autoq:0,show_userq:0,show_na:0,show_multiple:0},
			nowd:{show_noq:0,show_autoq:0,show_userq:0,show_na:0,show_nowd:1,show_multiple:0},
			na:{show_noq:0,show_autoq:0,show_userq:0,show_na:1,show_multiple:0},
			multi_match:{show_noq:0,show_autoq:0,show_userq:0,show_na:0,show_multiple:1},
	} } } ,
	created : function () {
//		console.log ( this.id,this.mode);
		this.loadCatalogList ( this.id , this.mode ) ;
	} ,
	updated : function () { tt.updateInterface(this.$el) } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		loadCatalogList : function ( id , mode ) {
			var me = this ;
			me.type = me.$route.query.type ;
			me.loaded = false ;
			if ( typeof me.start == 'undefined' ) me.start = 0 ;
			var meta = $.extend ( {offset:me.start*me.per_page,per_page:me.per_page} , me.modes[mode] ) ;
//			console.log ( meta ) ;
			let params = { query:'catalog' , catalog:id , meta:JSON.stringify(meta) } ;
			if ( typeof me.type != 'undefined' ) params.type = me.type ;
			$.get ( api , params , function ( d ) {
//				console.log ( d ) ;
				let qs = [] ;
				$.each ( d.data.entries , function ( k , v ) {
					if ( typeof v.q != 'undefined' && v.q > 0 ) qs.push ( 'Q'+v.q ) ;
					$.each ( (v.multimatch||[]) , function ( dummy , q ) { qs.push ( q ) } ) ;
					if ( typeof d.data.users[v.user] == 'undefined' ) return ;
					d.data.entries[k].username = d.data.users[v.user].name ;
				} ) ;
				me.catalog = all_catalogs[id] ;
				$.each ( (me.catalog.types||'').split(/,/) , function(dummy,q) { qs.push(q) } ) ;
				wd.getItemBatch ( qs , function () {
					me.entries = d.data.entries ;
					me.loaded = true ;
				} ) ;
			} ) ;
		}
	} ,
	watch: {
		'$route' (to, from) {
			this.loadCatalogList ( to.params.id , to.params.mode ) ;
		}
	}
} ) ;

var RecentChanges = Vue.extend ( {
	template : '#recent-changes-template' ,
	props : [ 'catalog' ] ,
	data : function () { return { entries:[] } } ,
	created : function () { this.loadData() ; tt.updateInterface(this.$el) } ,
	updated : function () { tt.updateInterface(this.$el) } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		loadData : function () {
			var me = this ;
			let params = { query:'rc' } ;
			if ( typeof me.catalog != 'undefined' ) params.catalog = me.catalog*1 ;
			$.get ( api , params , function ( d ) {
				$.each ( d.data.events , function ( k , v ) {
					if ( typeof d.data.users[v.user] == 'undefined' ) return ;
					d.data.events[k].username = d.data.users[v.user].name ;
				} ) ;
				me.entries = d.data.events ;
			} ) ;
		}
	} ,
	watch: {
		'$route' (to, from) {
			this.loadData() ;
		}
	}
} ) ;


var TopMissingEntries = Vue.extend ( {
	template : '#top-missing-entries-template' ,
	mixins: [editEntryMixin] ,
	props : [ 'catalogs' ] ,
	data : function () { return { data:[] , require_catalogs_string:'' , loading:false , has_loaded:false } } ,
	created : function () {
		let me = this ;
		me.require_catalogs_string = (me.catalogs||'').split(',').filter (catalog=>typeof all_catalogs[catalog*1]!='undefined').join(',') ;
		me.loadData() ;
		tt.updateInterface(me.$el) ;
	} ,
	updated : function () { tt.updateInterface(this.$el) } , //  $('.next_cc_set').focus() ;
	mounted : function () { tt.updateInterface(this.$el) ; $('.next_cc_set').focus() ; } ,
	methods : {
		loadData : function () {
			var me = this ;
			if ( me.require_catalogs_string == '' ) {
				me.require_catalogs = [] ;
				return ;
			}
			me.require_catalogs = me.require_catalogs_string.split(',') ;
			var params = {
				query:'top_missing' , 
				catalogs:me.require_catalogs_string ,
			} ;
			me.loading = true ;
			$.get ( api , params , function ( d ) {
				me.data = d.data ;
				me.has_loaded = true ;
				me.loading = false ;
			} ) ;
		} , 
		reloadWithParameters : function () {
			var me = this ;
			var path = '/top_missing/' + me.require_catalogs_string ;
			router.push ( path ) ;
			me.loadData() ;
		}
	}
} ) ;


var DownloadPage = Vue.extend ( {
	template : '#download-page-template' ,
	props : [ 'catalogs' ] ,
	data : function () { return { format:'tab' , as_file:1 ,
		columns:{exturl:1,username:1,aux:0,dates:0,location:0,multimatch:1} , 
		hidden:{any_matched:0,firmly_matched:0,user_matched:0,unmatched:0,automatched:0,name_date_matched:0,aux_matched:0,no_multiple:0}
	} } ,
	created : function () { this.init() ; tt.updateInterface(this.$el) } ,
	updated : function () { tt.updateInterface(this.$el) } , //  $('.next_cc_set').focus() ;
	mounted : function () { tt.updateInterface(this.$el) ; $('.next_cc_set').focus() ; } ,
	methods : {
		init : function () {
			var me = this ;
		} ,
		generateDownloadURL : function () {
			var me = this ;
			$.each ( me.columns , function ( k , v ) { me.columns[k] = v?1:0 } ) ;
			$.each ( me.hidden , function ( k , v ) { me.hidden[k] = v?1:0 } ) ;
			var url = 'http://tools.wmflabs.org/mix-n-match/api.php?query=download2' ;
			url += '&catalogs=' + me.catalogs.replace(/[^0-9,]/g,'') ;
			url += '&columns=' + encodeURIComponent ( JSON.stringify ( me.columns ) ) ;
			url += '&hidden=' + encodeURIComponent ( JSON.stringify ( me.hidden ) ) ;
			url += "&format=" + me.format ;
			if ( me.as_file != false ) url += "&as_file=1" ;
			return url ;
		}
	}
} ) ;

var CreationCandidates = Vue.extend ( {
	template : '#creation-candidates-template' ,
	mixins: [editEntryMixin] ,
	props : [ 'mode' ] ,
	data : function () { return { entries:[] , edits_todo:[] , grouped_entries:[] , loaded_wd:false , wd_entries:[] , require_catalogs:[] , min_catalogs_required:0 , require_catalogs_string:'' , ext_name:'' } } ,
	created : function () { this.loadData() ; tt.updateInterface(this.$el) } ,
	updated : function () { tt.updateInterface(this.$el) } , //  $('.next_cc_set').focus() ;
	mounted : function () { tt.updateInterface(this.$el) ; $('.next_cc_set').focus() ; } ,
	methods : {
		loadData : function () {
			var me = this ;
			let min = 0 ;
			if ( me.mode == 'human' ) min = 4 ;
			if ( typeof me.$route.query.min != 'undefined' ) min = me.$route.query.min ;
			if ( typeof me.$route.query.ext_name != 'undefined' ) me.ext_name = me.$route.query.ext_name ;
			else me.ext_name='' ;
			if ( typeof me.$route.query.require_catalogs != 'undefined' ) {
				me.require_catalogs = me.$route.query.require_catalogs.replace(/[^0-9,]/,'').split(/,/) ;
				me.require_catalogs_string = me.require_catalogs.join(',') ;
				me.min_catalogs_required = 1 ; // Default
			}
			if ( typeof me.$route.query.min_catalogs_required != 'undefined' ) {
				me.min_catalogs_required = me.$route.query.min_catalogs_required * 1 ;
			} 
			if ( me.edits_todo.length > 0 ) {
				alert ( "Edits still running!" ) ;
				return ;
			}
			me.loaded_wd = false ;
			var params = {
				query:'creation_candidates' , 
				mode:(typeof me.mode=='undefined'?'':me.mode) , 
				require_unset:1,
				min:min,
				require_catalogs:me.require_catalogs.join(',') ,
				min_catalogs_required:me.min_catalogs_required
			} ;
			if ( me.ext_name != '' ) params.ext_name = me.ext_name ;
			$.get ( api , params , function ( d ) {
				if ( d.status != 'OK' ) {
					alert ( "No results, parameters might be too restrictive" ) ;
					return ;
				}
				$.each ( d.data.entries , function ( k , v ) {
					if ( typeof d.data.users[v.user] == 'undefined' ) return ;
					d.data.entries[k].username = d.data.users[v.user].name ;
				} ) ;
				me.entries = d.data.entries ;
				me.groupEntries() ;
				me.loadDataWikidata() ;
				$('.next_cc_set').focus() ;
			} ) ;
		} ,
		reloadWithParameters : function () {
			var me = this ;
			var path = '/creation_candidates/' ;
			if ( typeof me.mode != 'undefined' && me.mode != '' ) path += me.mode + '/' ;
			if ( me.require_catalogs_string != '' ) {
				path += "?require_catalogs=" + me.require_catalogs_string ;
				if ( me.min_catalogs_required*1 == 0 ) me.min_catalogs_required = 1 ; // No point in requiring catalogs if you don't require a single one of them
				path += "&min_catalogs_required=" + me.min_catalogs_required ;
				me.require_catalogs = me.require_catalogs_string.replace(/[^0-9,]/,'').split(/,/) ;
			} else {
				me.require_catalogs = [] ;
				me.min_catalogs_required = 0 ;
			}
			router.push ( path ) ;
			me.loadData() ;
		} ,
		groupEntries : function () {
			var me = this ;
			var ge = {} ;
			var min_group_length = 2 ;
			var default_group_key = '_other' ;

			// Split entry list into groups
			if ( me.mode == 'human' || me.mode == 'test' ) {
				$.each ( me.entries , function ( k , entry ) {
					var group_key = (entry.born||'?').replace(/-.*$/,'') + '-' + (entry.died||'?').replace(/-.*$/,'') ;
					if ( group_key == '?-?' ) group_key = default_group_key ;
					if ( typeof ge[group_key] == 'undefined' ) ge[group_key] = [] ;
					ge[group_key].push ( entry ) ;
				} ) ;
			} else {
				ge[default_group_key] = me.entries ;
			}

			// Join smaller lists into default_group_key
			$.each ( ge , function ( group_key , group ) {
				if ( group_key == default_group_key ) return ;
				if ( group.length >= min_group_length ) return ; // Keep
				if ( typeof ge[default_group_key] == 'undefined' ) ge[default_group_key] = [] ;
				$.each ( group , function ( dummy , entry ) {
					ge[default_group_key].push ( entry ) ;
				} ) ;
				delete ge[group_key] ;
			} ) ;

			ge = Object.values ( ge ) ;
			ge.sort ( function ( a , b ) {
				return b.length - a.length ;
			} ) ;

			me.grouped_entries = ge ;
		} ,
		filteredName : function () {
			var ret = this.entries[0].ext_name ;
			ret = ret.replace ( /^(Sir|Madam|Madame|Saint) / , '' ) ;
			ret = ret.replace ( /\s*\(.+?\)\s*/ , ' ' ) ;
			ret = ret.replace ( /\s*\b[A-Z]\.\s*/ , ' ' ) ;
			return ret ;
		} ,
		loadDataWikidata : function () {
			var me = this ;
			me.loaded_wd = false ;
			me.wd_entries = [] ;
			if ( me.entries.length == 0 ) return ; // Paranoia
			$.getJSON ( 'https://www.wikidata.org/w/api.php?callback=?' , {
				action:'wbsearchentities',
				search:me.filteredName(),
				language:'en',
				limit:20,
				type:'item',
				format:'json'
			} , function ( d ) {
				$.each ( d.search , function ( k , v ) {
					if ( v.repository != '' ) return ;
					me.wd_entries.push ( v ) ;
				} ) ;
			} ) . always ( function () { me.loaded_wd = true } ) ;
		} ,
		toggleCheckboxes : function ( event ) {
			var o = $(event.target) ;
			var card = $(o.parents('div.card').get(0)) ;
			card.find('input.entry-list-item-checkbox').click() ;
		} ,
		createNewItem : function () {
			var me = this ;
			var p31 = '' ;
			if ( me.mode == 'human' ) p31 = 'Q5' ;
			var born = '' ;
			var died = '' ;
			var other_statements = {} ;
			
			$.each ( me.entries , function ( num , e ) {
				var is_checked = $('input.entry-list-item-checkbox[entry='+e.id+']').is(':checked') ;
				if ( !is_checked ) return ;
//				if ( e.wd_prop == null || e.wd_qual != null ) return ;
				if ( p31 == '' && e.type != '' ) p31 = e.type=='person'?'Q5':e.type ;
				if ( born.length < (''+(e.born||'')).length ) born = e.born ;
				if ( died.length < (''+(e.died||'')).length ) died = e.died ;
				if ( /\bfemale\b/.test(e.ext_desc) ) other_statements.P21 = 'Q6581072' ;
				if ( /\bmale\b/.test(e.ext_desc) ) other_statements.P21 = 'Q6581097' ;
				if ( /\b(taxon|species|genus)\b/.test(e.ext_desc) ) p31 = 'Q16521' ;
				if ( /\b(species)\b/.test(e.ext_desc) ) other_statements.P105 = 'Q7432' ;
				if ( /\b(author|writer)\b/.test(e.ext_desc) ) other_statements.P106 = 'Q36180' ;
				if ( /\bpainter\b/.test(e.ext_desc) ) other_statements.P106 = 'Q1028181' ;
				me.edits_todo.push ( e ) ;
				$('div.entry_row[entry='+e.id+']').addClass('inactive') ;
//				console.log ( e.has_person_date ) ;
			} ) ;

			if ( p31 != 'Q5' ) delete other_statements.P106 ;
			if ( p31 != '' ) other_statements.P31 = p31 ;

			if ( me.edits_todo.length == 0 ) {
				alert ( "Please select at least one entry to base the new item on" ) ;
				return ;
			}
			
			// TODO use born,died,p31
			me.newItemForEntry ( me.edits_todo , me.doNextEdit , other_statements ) ;
		} ,
		doNextEdit : function () {
			var me = this ;
			$('div.entry_row[entry='+me.edits_todo[0].id+']').removeClass('inactive') ;
			me.edits_todo.shift() ;
			if ( me.edits_todo.length == 0 ) return ; // All done
			me.setEntryQ ( me.edits_todo[0] , me.last_created_q , false , me.doNextEdit ) ;
		}
	} ,
	watch: {
		'$route' (to, from) {
			console.log("!") ;
			this.loadData() ;
		}
	}
} ) ;


var SiteStats = Vue.extend ( {
	template : '#site-stats-template' ,
	props : [ "id" ] ,
	data : function () { return { catalog:{} , sites:[] , loaded:false } } ,
	created : function () { this.loadData() ; tt.updateInterface(this.$el) } ,
	updated : function () { tt.updateInterface(this.$el) ; $('.next_cc_set').focus() } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		loadData : function () {
			var me = this ;
			me.loaded = false ;
			me.catalog = all_catalogs[me.id] ;
			$.get ( api , { query:'sitestats' , catalog:me.id } , function ( d ) {
				var sites = [] ;
				$.each ( d.data , function ( k , v ) {
					sites.push ( { site:k , articles:v[me.id] } ) ;
				} ) ;
				me.sites = sites ;
				me.loaded = true ;
			} ) ;
		}
	} ,
	watch: {
		'$route' (to, from) {
			this.loadData() ;
		}
	}
} ) ;

var MissingArticles = Vue.extend ( {
	template : '#missing-articles-template' ,
	props : [ "id" , "site" , "start" ] ,
	data : function () { return { catalog:{} , entry_groups:[] , loaded:false , page:0 , total:0 , last_key:'' } } ,
	created : function () { this.loadData() ; tt.updateInterface(this.$el) } ,
	updated : function () { tt.updateInterface(this.$el) ; $('.next_cc_set').focus() } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		loadData : function () {
			var me = this ;
			me.loaded = false ;
			me.catalog = all_catalogs[me.id] ;
			if ( typeof me.start == 'undefined' ) me.page = 0 ;
			else me.page = me.start ;
			
			var key = me.id+':'+me.site ;
			if ( key == me.last_key ) {
				me.loaded = true ;
				return ;
			}
			me.last_key = key ;
			
			$.get ( api , { query:'missingpages' , catalog:me.id , site:me.site } , function ( d ) {
				$.each ( d.data.entries , function ( k , v ) {
					if ( typeof d.data.users[v.user] == 'undefined' ) return ;
					d.data.entries[k].username = d.data.users[v.user].name ;
				} ) ;
				var eg = [ [] ] ;
				var last = 0 ;
				me.total = 0 ;
				$.each ( d.data.entries , function ( k , v ) {
					if ( eg[last].length >= 50 ) {
						eg.push ( [] ) ;
						last++ ;
					}
					eg[last].push ( v ) ;
					me.total++ ;
				} ) ;
				me.entry_groups = eg ;
				me.loaded = true ;
			} ) ;
		}
	} ,
	watch: {
		'$route' (to, from) {
			this.loadData() ;
		}
	}
} ) ;

var RandomEntry = Vue.extend ( {
	mixins: [entryDisplayMixin] ,
	template : '#random-entry-template' ,
	props : [ "id" ] ,
	data : function () { return { catalog:{} , entry:{} , loaded:false } } ,
	created : function () { this.loadData() ; tt.updateInterface(this.$el) } ,
	updated : function () { tt.updateInterface(this.$el) ; } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		loadData : function () {
			var me = this ;
			me.loaded = false ;
			me.catalog = all_catalogs[me.id] ;
			$.get ( api , { query:'random' , catalog:(me.id||0) , submode:'unmatched' } , function ( d ) {
				me.entry = d.data ;
				me.catalog = me.entry.catalog ;
				me.loaded = true
			} ) ;
		}
	}
} ) ;


Vue.component ( 'timestamp' , {
	template : '#timestamp-template' ,
	props : [ 'ts' ]
} ) ;

Vue.component ( 'entry-link' , {
	template : '#entry-link-template' ,
	props : [ 'entry' ]
} ) ;


Vue.component ( 'entry-details' , {
	mixins: [editEntryMixin,entryMixin] ,
	template : '#entry-details-template' ,
	props : [ 'entry' , 'random' , 'random_base' , 'show_catalog' ] ,
	data : function () { return { catalog:{} } } ,
	created : function () {
		this.catalog = all_catalogs[this.entry.catalog] ;
		tt.updateInterface(this.$el) ;
	} ,
	updated : function () { tt.updateInterface(this.$el) ; } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		loadRandomData : function () {
			this.$emit('random_entry_button_clicked') ;
		}
	}
} ) ;

Vue.component ( 'match-entry' , {
	mixins: [entryDisplayMixin,editEntryMixin,entryMixin] ,
	template : '#match-entry-template' ,
	props : [ 'entry' ] ,
	data : function () { return { catalog:{} , mnm_entries:[] , wp_entries:[] , wd_entries:[] , loaded_wp:false , loaded_mnm:false , loaded_wd:false , loaded_sparql:false , sparql_entries:[] , last_id:'' } } ,
	created : function () { this.loadData() ; tt.updateInterface(this.$el) } ,
	updated : function () { tt.updateInterface(this.$el) ; } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		loadDataWikipedia : function () {
			var me = this ;
			me.loaded_wp = false ;
			me.wp_entries = [] ;
			$.getJSON ( 'https://'+me.catalog.search_wp+'.wikipedia.org/w/api.php?callback=?' , {
				action:'query',
				list:'search',
				format:'json',
				srsearch:me.filteredName()
			} , function ( d ) {
				me.wp_entries = d.query.search ;
				
				var titles = [] ;
				$.each ( me.wp_entries , function ( k , v ) {
					titles.push ( v.title ) ;
				} ) ;
				
				if ( titles.length == 0 ) return ;
				
				// Get matching Wikidata items
				var site = me.catalog.search_wp+'wiki' ;
				$.getJSON ( 'https://www.wikidata.org/w/api.php?callback=?' , {
					action:'wbgetentities' ,
					props:'sitelinks' ,
					titles:titles.join('|') ,
					sites:site ,
					format:'json'
				} , function ( d ) {
					$.each ( d.entities , function ( q , v ) {
						if ( q*1 < 0 ) return ;
						if ( typeof v.sitelinks == 'undefined' || typeof v.sitelinks[site] == 'undefined' ) return ;
						var title = v.sitelinks[site].title ;
						var a = "<a target='_blank' class='wikidata' href='https://www.wikidata.org/wiki/" + q + "'>" + q + "</a>" ;
						a += " [<a href='#' q='"+q+"' class='set_q' tt_title='manually_set_q'>&uarr;</a>]" ;
						$('td.wd_loading[wp_title="'+me.normaliseTitle(title)+'"]').removeClass('wd_loading').html ( a ) ;
						$('a.set_q[q="'+q+'"]').click ( function (e) {
							$('#q_input').val ( q ) ;
							me.setUserQ(e);
							return false
						} ) ;
					} ) ;
				} ) . always ( function () {
					$('td.wd_loading').removeClass('wd_loading').html ( '&mdash;' ) ;
					tt.updateInterface(me.$el) ;
				} ) ;
				
			} ) . always ( function () { me.loaded_wp = true } ) ;
		} ,
		checkSPARQL : function () {
			var me = this ;
			me.loaded_sparql = false ;
			me.sparql_entries = [] ;
			if ( me.catalog.wd_prop == null ) return ;
			if ( me.catalog.wd_qual != null ) return ;
			var sparql = 'SELECT ?q ?qLabel ?description { ?q wdt:P' + me.catalog.wd_prop + ' "' + me.entry.ext_id + '" ' ;
			sparql += 'OPTIONAL { ?q schema:description ?description filter(lang(?description)="en") } ' ;
			sparql += 'SERVICE wikibase:label { bd:serviceParam wikibase:language "en" } ' ;
			sparql += '}' ;
			
			var url = 'https://query.wikidata.org/sparql?format=json&query=' + encodeURIComponent ( sparql ) ;
			$.get ( url , function ( d ) {
				var qs = [] ;
				$.each ( d.results.bindings , function ( k , v ) {
					if ( v.q.type != 'uri' ) return ;
					var url = v.q.value ;
					var o = { id:url.replace ( /^.+\/Q/ , 'Q' ) , url:url } ;
					if ( (v.qLabel||{}).type == 'literal' ) o.label = v.qLabel.value ;
					if ( (v.description||{}).type == 'literal' ) o.description = v.description.value ;
					qs.push ( o ) ;
				} ) ;
				if ( qs.length == 0 ) return ;
				me.sparql_entries = qs ;
				me.loaded_sparql = true ;
			} , 'json' ) ;
		} ,
		normaliseTitle : function ( t ) {
			return encodeURIComponent ( t.replace(/ /g,'_') ) ;
		} ,
		loadData : function () {
			var me = this ;
			me.catalog = all_catalogs[me.entry.catalog] ;
			
//			if ( me.entry.id == me.last_id ) return ;
			me.last_id = me.entry.id ;

			me.loaded_mnm = false ;
			me.mnm_entries = [] ;
			$.get ( api , { query:'search' , what:me.filteredName() , max:20 } , function ( d ) {
				$.each ( d.data.entries , function ( k , v ) {
					if ( typeof d.data.users[v.user] == 'undefined' ) return ;
					d.data.entries[k].username = d.data.users[v.user].name ;
				} ) ;
				$.each ( d.data.entries , function ( k , v ) {
					if ( v.id == me.entry.id ) return ;
					me.mnm_entries.push ( v ) ;
				} ) ;
			} , 'json' ) . always ( function () { me.loaded_mnm = true } ) ;
			
			me.loaded_wd = false ;
			me.wd_entries = [] ;
			$.getJSON ( 'https://www.wikidata.org/w/api.php?callback=?' , {
				action:'wbsearchentities',
				search:me.filteredName(),
				language:me.catalog.search_wp,
				limit:20,
				type:'item',
				format:'json'
			} , function ( d ) {
				$.each ( d.search , function ( k , v ) {
					if ( v.repository != '' ) return ;
					me.wd_entries.push ( v ) ;
				} ) ;
			} ) . always ( function () { me.loaded_wd = true } ) ;
			
			me.loadDataWikipedia() ;
			me.checkSPARQL() ;

		} ,
		wikipediaSearch : function () {
			var lang = this.catalog.search_wp ;
			return "https://"+lang+".wikipedia.org/w/index.php?title=Special%3ASearch&search=" + this.getSearchString(false) ;
		} ,
		qWasSet : function () {
			$('button.load-random-entry').click() ; // Load next entry
		} ,
		setUserQ : function ( e ) {
			e.preventDefault() ;
			var q = $('#q_input').val() ;
			if ( !q.match(/\d/) ) return false ;
			this.setEntryQ ( this.entry , q ) ;
			this.qWasSet() ;
			return false ;
		} ,
		setUserNoWD : function ( e ) {
			e.preventDefault() ;
			this.setEntryQ ( this.entry , -1 , true ) ;
			this.qWasSet() ;
			return false ;
		} ,
		setUserNA : function ( e ) {
			e.preventDefault() ;
			this.setEntryQ ( this.entry , 0 , true ) ;
			this.qWasSet() ;
			return false ;
		} ,
		setUserNew : function ( e ) {
			e.preventDefault() ;
			this.newItemForEntry ( this.entry , function ( q ) {
				var url = "https://www.wikidata.org/wiki/Q" + q ;
				var win = window.open(url, '_blank');
//				win.focus() ;
			} ) ;
			this.qWasSet() ;
			return false ;
		} ,
		
	} ,
	watch: {
		'$route' (to, from) {
			this.loadData() ;
		},
		'entry.q' (to, from) {
			if ( to != null ) return ;
			this.loadData() ;
		}
	}
} ) ;


var SyncCatalog = Vue.extend ( {
	template : '#sync-catalog-template' ,
	props : [ "id" ] ,
	data : function () { return { catalog:{} , data:{} , entries:{} , loaded:false , mnm2wd:'' , wd_duplicates:[] , update_mnm_status:'' , update_mnm_result:{} } } ,
	created : function () { this.loadData() ; tt.updateInterface(this.$el) } ,
	updated : function () { tt.updateInterface(this.$el) ; } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		updateMNM : function () {
			var me = this ;
			me.update_mnm_status = 'updating' ;
			$.post ( api , {
				query:'match_q_multi' ,
				catalog:me.id,
				tusc_user:widar.getUserName() ,
				data:JSON.stringify(me.data.wd_no_mm)
			} , function ( d ) {
				me.update_mnm_result = d ;
				if ( d.status == 'OK' ) {
					me.update_mnm_status = 'done' ;
					$('#wd_no_mm').html ( "<span tt='done'></span>" ) ;
				} else {
					me.update_mnm_status = 'failed' ;
				}
			} ) ;
		} ,
		checkDoubleWD : function ( callback ) {
			var me = this ;
			me.wd_duplicates = [] ;
			if ( me.catalog.wd_prop == null ) return callback() ;
			if ( me.catalog.wd_qual != null ) return callback() ;
			var sparql = 'SELECT ?entry (count(?q) AS ?cnt) (group_concat(?q) AS ?qs ) { ?q wdt:P' + me.catalog.wd_prop + ' ?entry } GROUP BY ?entry HAVING (?cnt>1)' ;
			var url = 'https://query.wikidata.org/sparql?format=json&query=' + encodeURIComponent ( sparql ) ;
			$.get ( url , function ( d ) {
				var dupes = [] ;
				var ids = [] ;
				$.each ( d.results.bindings , function ( k , v ) {
					var o = { ext_id:v.entry.value , qs:[] } ;
					o.qs = v.qs.value.match ( /(Q\d+)\b/g ) ;
					dupes.push ( o ) ;
					ids.push ( o.ext_id ) ;
				} ) ;
				$.post ( api , { query:'get_entry', ext_ids:JSON.stringify(ids) , catalog:me.catalog.id } , function ( d ) {
					$.each ( d.data.entries , function ( k , v ) {
						me.entries[k] = v ;
						$.each ( dupes , function ( k1 , v1 ) {
							if ( v.ext_id == v1.ext_id ) dupes[k1].id = v.id ;
						} ) ;
						if ( typeof d.data.users[v.user] == 'undefined' ) return ;
						d.data.entries[k].username = d.data.users[v.user].name
					} ) ;
					me.wd_duplicates = dupes ;
					callback() ;
				} ) ;
			} ) ;
		} ,
		loadData : function () {
			var me = this ;
			me.loaded = false ;
			me.catalog = all_catalogs[me.id] ;
			me.entries = {} ;
			me.mnm2wd = '' ;
			
			var running = 2 ;
			function fin () {
				running-- ;
				if ( running > 0 ) return ;
				me.loaded = true ;
			}
			
			me.checkDoubleWD ( fin ) ;
			
			$.get ( api , { query:'get_sync' , catalog:me.id } , function ( d ) {
				me.data = d.data ;

				var mnm2wd = [] ;
				$.each ( me.data.mm_no_wd , function ( k , v ) {
					mnm2wd.push ( 'Q'+v[0]+"\tP"+me.catalog.wd_prop+"\t"+'"'+decodeURIComponent(escape(v[1]))+'"' ) ;
					if ( mnm2wd.length > 20000 ) return false ; // MAX 20K
				} ) ;
				if ( mnm2wd.length > 0 ) me.mnm2wd = mnm2wd.join ( "\n" ) ;

			 ids = [] ;
				$.each ( (me.data.mm_double||{}) , function ( q , v ) {
					$.each ( v , function ( k1 , v1 ) { ids.push ( v1 ) } ) ;
				} ) ;
				if ( ids.length > 0 ) {
					$.post ( api , { query:'get_entry', entry:ids.join(',') } , function ( d ) {
//						me.entries = d.data.entries ;
						$.each ( d.data.entries , function ( k , v ) {
							me.entries[k] = v ;
							if ( typeof d.data.users[v.user] == 'undefined' ) return ;
							d.data.entries[k].username = d.data.users[v.user].name
						} ) ;
						
					} )
					.always ( function () { fin() } ) ;
				} else {
					fin() ;
				}
			} ) ;
		}
	}
} ) ;

var router ;
var app ;
let wd = new WikiData() ;


// Load all catalog data first

function loadOverview ( callback ) {
	$.get ( 'https://tools.wmflabs.org/mix-n-match/overview.json' , function ( d ) {
		$.each ( d.data , function ( k , v ) { // Convert strings to proper numbers
			$.each ( ['total','manual','autoq','nowd','noq','na'] , function ( k2 , v2 ) {
				d.data[k][v2] = Number ( v[v2] ) ;
			} ) ;
			d.data[k].unmatched = v.total-v.manual-v.autoq-v.nowd-v.na ;
		} ) ;
		all_catalogs = d.data ;
		if ( typeof callback != 'undefined' ) callback() ;
	} , 'json' ) .fail(function() {
		setTimeout ( function () { loadOverview(callback) } , 200 )
	} ) ;
}

$(document).ready ( function () {

    vue_components.toolname = 'mix-n-match' ;
//    vue_components.components_base_url = 'https://tools.wmflabs.org/magnustools/resources/vue/' ; // For testing; turn off to use tools-static
    Promise.all ( [
            vue_components.loadComponents ( ['tool-translate','wd-link','autodesc',//'wd-date','tool-navbar','commons-thumbnail',
                'vue-components/catalog_editor.html',
                'vue-components/search_page.html',
                ] ) ,
            new Promise(function(resolve, reject) { loadOverview ( resolve ) } )
    ] ) .then ( () => {
		current_language = tt.language ;
		wd_link_wd = wd ;
		tt.addILdropdown ( '#tooltranslate_wrapper' ) ;

		const routes = [
		  { path: '/', component: MainPage },
		  { path: '/group', component: CatalogGroup , props:true },
		  { path: '/group/:key', component: CatalogGroup , props:true },
		  { path: '/group/:key/:order', component: CatalogGroup , props:true },
		  { path: '/main', component: MainPage },
		  { path: '/catalog/:id', component: CatalogDetails , props:true } ,
		  { path: '/list/:id/:mode', component: CatalogList , props:true } ,
		  { path: '/list/:id/:mode/:start', component: CatalogList , props:true } ,
		  { path: '/search', component: SearchPage , props:true } ,
		  { path: '/search/:query', component: SearchPage , props:true } ,
		  { path: '/search/:query/:excl', component: SearchPage , props:true } ,
		  { path: '/entry/:id', component: Entry , props:true } ,
		  { path: '/.2Fentry.2F:id', redirect:'/entry/:id' } ,
		  { path: '/rc', component: RecentChanges , props:true } ,
		  { path: '/rc/:catalog', component: RecentChanges , props:true } ,
		  { path: '/creation_candidates', component: CreationCandidates , props:true } ,
		  { path: '/creation_candidates/:mode', component: CreationCandidates , props:true } ,
		  { path: '/catalog_editor/:id', component: CatalogEditor , props:true } ,

		  { path: '/download', component: DownloadPage , props:true } ,
		  { path: '/download/:catalogs', component: DownloadPage , props:true } ,
		//  { path: '/candidates/', component: MainCandidatesPage , props: (route) => (route.query) },

		  { path: '/site_stats/:id', component: SiteStats , props:true } ,
		  { path: '/missing_articles/:id/:site', component: MissingArticles , props:true } ,
		  { path: '/missing_articles/:id/:site/:start', component: MissingArticles , props:true } ,
		  { path: '/random', component: RandomEntry , props:true } ,
		  { path: '/random/:id', component: RandomEntry , props:true } ,
		  { path: '/sync/:id', component: SyncCatalog , props:true } ,
		  { path: '/scraper/new', component: Scraper , props:true } ,
		  { path: '/sparql', component: SparqlList , props:true } ,
		  { path: '/sparql/:sparql', component: SparqlList , props:true } ,
		  { path: '/top_missing/', component: TopMissingEntries , props:true } ,
		  { path: '/top_missing/:catalogs', component: TopMissingEntries , props:true } ,
		] ;

		router = new VueRouter({routes}) ;
		app = new Vue ( { router } ) .$mount('#app') ;
    } ) ;

} ) ;
