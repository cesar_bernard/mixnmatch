#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$catalog = 238 ;
$filename = '/data/project/mix-n-match/scripts/catalog_specific/inaturalist_taxon_mapping.csv' ;

$mnm = new MixNMatch ;

if ( 0 ) { # Update from data file
	$handle = fopen($filename, "r") ;
	$header = fgetcsv($handle, 0, ",") ; # Header line
	while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
		$d = (object) [] ;
		foreach ( $header AS $k => $v ) $d->$v = $data[$k] ;

		# Find or create entry
		$o = '' ;
		$taxon_id = $mnm->escape($d->taxon_id) ;
		$result = $mnm->getSQL ( "SELECT * FROM entry WHERE catalog={$catalog} AND ext_id='{$taxon_id}'" ) ;
		if ( !($o = $result->fetch_object() ) ) {
			if ( $d->is_active == 'f' ) continue ; // No point adding inactive entries
			$no = (object) [
				'catalog' => $catalog ,
				'id' => $d->taxon_id ,
				'url' => 'http://www.inaturalist.org/taxa/'.$d->taxon_id ,
				'name' => $d->inat_name
			] ;
			$id = $mnm->addNewEntry ( $no ) ;
			$o = $mnm->getEntryObjectFromID ( $id ) ;
		}
		if ( !isset($o) or $o === null or $o == '' ) continue ; # Paranoia

		# Deactivate if necessary
		if ( $d->is_active == 'f' ) { #and (!isset($o->q) or $o->q === null or $o->q == '' or $o->user == 0 ) ) {
			$mnm->getSQL ( "UPDATE entry SET q=0,user=4,`timestamp`='" . $mnm->getCurrentTimestamp() . "' WHERE id={$o->id}" ) ;
			continue ;
		}

		$sql = "INSERT IGNORE INTO kv_entry (entry_id,kv_key,kv_value) VALUES ({$o->id},'".$mnm->escape($d->ext_source)."','".$mnm->escape($d->source_identifier)."')" ;
		$mnm->getSQL ( $sql ) ;
	}
	fclose ( $handle ) ;
}

if ( 1 ) { # Update aux from kv_entry
	$mappings = [ # String => property
#		'eBird/Clements Checklist 6.9' => 3444 , # Different IDs?
#		'Draft IUCN/SSC, 2013.1' => 627 , # Many not in external catalog
#		'Draft IUCN/SSC Amphibian Specialist Group, 2011' => 627 , # Many not in external catalog
#		'IUCN Red List of Threatened Species. Version 2012.2' => 627 , # Many not in external catalog
#		'IUCN Red List of Threatened Species. Version 2012.1' => 627 , # Many not in external catalog
#		'The Reptile Database' => 5473 , # Mismatching IDs, do not use
		'CONABIO' => 4902 ,
		'Calflora' => 3420 ,
		'GBIF' => 846 ,
	] ;
	foreach ( $mappings AS $key => $prop ) {
		$sql = "SELECT * FROM vw_kv WHERE catalog={$catalog} AND done=0 AND kv_key='" . $mnm->escape($key) . "'" ;
		$result = $mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) VALUES ({$o->id},$prop,'".$mnm->escape($o->kv_value)."')" ;
			$mnm->getSQL($sql) ; #print "{$sql}\n" ;
			$sql = "UPDATE kv_entry SET `done`=1 WHERE id={$o->kv_id}" ;
			$mnm->getSQL($sql) ; #print "{$sql}\n" ;
		}
	}
}

?>