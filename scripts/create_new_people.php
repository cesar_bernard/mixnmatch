#!/usr/bin/php
<?PHP

# https://tools.wmflabs.org/wikidata-todo/user_edits.php?sparql=&pattern=invoked+by+mixnmatch_people_creator%24&user=Reinheitsgebot&start=20180323&end=&doit=Do+it

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

$min_cnt = 3 ;
$min_commands = 7 ;
$min_catalogs = 3 ;
$toolname = 'mixnmatch_people_creator' ;
$bad_indicators = ['Q4504549','Q955464','Q1634473'] ;

# INCLUDES
require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;
require_once ( "/data/project/mix-n-match/manual_lists/large_catalogs/shared.php" ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;


function getQS () {
	global $toolname , $qs ;
	if ( isset($qs) ) return $qs ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/mix-n-match/reinheitsgebot.conf" ;
	$qs->toolname = $toolname ;
	$qs->sleep = 1 ;
	$qs->use_command_compression = true ;
	$qs->generateAndUseTemporaryBatchID() ;
	return $qs ;
}

// $commands = [] , one QS V1 command per element, no newlines
function runCommandsQS ( $commands ) {
	global $qs ;
	if ( count($commands) == 0 ) return ;
	$commands = implode ( "\n" , $commands ) ;
	$tmp_uncompressed = $qs->importData ( $commands , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp_uncompressed['data']['commands'] ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	if ( !isset($qs->last_item) ) return ;
	$last_item = 'Q' . preg_replace ( '/\D/' , '' , "{$qs->last_item}" ) ;
	return $last_item ;
}

function addIndicators ( &$indicators , $sparql , $prop ) {
	global $bad_indicators ;
	$j = getSPARQL ( $sparql ) ;
	foreach ( $j->results->bindings AS $b ) {
		$s = strtolower ( $b->label->value ) ;
		$q = preg_replace ( '/^.+\/Q/' , 'Q' , $b->q->value ) ;
		if ( in_array ( $q , $bad_indicators ) ) continue ; // Hardcoded bad values
		$v = "{$prop}\t{$q}" ;
		if ( isset($indicators[$s]) ) {
			if ( $indicators[$s] != $v ) $indicators[$s] = '' ; # Multiple meanings of same string => BAD
		} else $indicators[$s] = $v ;
	}
}

function loadIndicators () {
	$indicators = [] ;

	# Gender
	$sparql = 'SELECT ?q ?label { VALUES ?q { wd:Q6581097 wd:Q6581072 } . ?q rdfs:label ?label }' ;
	addIndicators ( $indicators , $sparql , 'P21' ) ;

	# Nationalities
	$sparql = 'SELECT ?q ?label { ?q wdt:P31 wd:Q3624078 ; wdt:P1549 ?label }' ;
	addIndicators ( $indicators , $sparql , 'P27' ) ;

	# Occupations
	$sparql = 'SELECT ?q ?label { ?q wdt:P31 wd:Q28640 ; rdfs:label ?label }' ;
	addIndicators ( $indicators , $sparql , 'P106' ) ;

	# Remove empty=bad indicators
	foreach ( $indicators AS $k => $v ) {
		if ( $v == '' ) unset ( $indicators[$k] ) ;
	}
	return $indicators ;
}

function formatDateProperty ( $date ) {
	if ( preg_match ( '/^\d+-\d{2}-\d{2}$/' , $date ) ) return "+{$date}T00:00:00Z/11" ;
	if ( preg_match ( '/^\d+-\d{2}$/' , $date ) ) return "+{$date}-01T00:00:00Z/10" ;
	if ( preg_match ( '/^\d+$/' , $date ) ) return "+{$date}-01-01T00:00:00Z/9" ;
}

function tryVIAF ( $name , $born , $died ) {
	return ; // VIAF search is ... problematic, thus deactivated for now
	$url = "https://viaf.org/viaf/search?query=local.names+all+%22" . urlencode("{$name} {$born} {$died}") . "%22&sortKeys=holdingscount&recordSchema=BriefVIAF" ;
	$x = file_get_contents($url) ;
	if ( !preg_match_all ( '/>(\d+)<\/v:viafID>/' , $x , $m ) ) return ;
	if ( count($m[1]) != 1 ) return ;
	return trim($m[1][0])*1 ;
}

function addVIAFandRelated ( $viaf , &$commands ) {
	global $lc ;
	if ( !isset($viaf) ) return ;
	$viaf = trim($viaf) ;
	if ( !preg_match ( '/^\d+$/' , $viaf ) ) return ; # Paranoia
	$commands[] = "LAST\tP214\t\"{$viaf}\"" ;
	$col2prop = [] ;
	foreach ( $lc->prop2field[2] AS $prop => $col ) $col2prop[$col] = $prop ;
	$sql = "SELECT * FROM viaf WHERE ext_id='{$viaf}'" ;
	$db = $lc->openDB() ;
	$result = getSQL ( $db , $sql ) ;
	if (!( $o = $result->fetch_object() ) ) return ;
	foreach ( $o AS $k => $v ) {
		if ( !isset($v) or $v == '' ) continue ;
		if ( $k == 'ext_id' ) continue ;
		if ( !isset($col2prop[$k]) ) continue ;
		$prop = $col2prop[$k] ;
		if ( $prop == 'P244' or $prop == 'P1207' ) $v = str_replace ( ' ' , '' , $v ) ; # Hardcoded LCCN hack
		$commands[] = "LAST\t{$prop}\t\"{$v}\"" ;
	}
}

function getEscapedNameVariations ( $name , $depth = 0 ) {
	global $mnm ;
	$name = str_replace ( '-' , ' ' , $name ) ;
	$ret = [] ;
	$p = explode ( ' ' , $name , 2 ) ;
	if ( count($p) == 1 ) {
		$ret = [ $name ] ;
	} else {
		$rest = getEscapedNameVariations ( $p[1] , $depth+1 ) ;
		foreach ( $rest AS $r ) {
			$ret[] .= $p[0] . ' ' . $r ;
			$ret[] .= $p[0] . '-' . $r ;
		}
	}
	if ( $depth == 0 ) {
		foreach ( $ret AS $k => $v ) $ret[$k] = $mnm->escape($v) ;
		return "'" . implode ( "','" , $ret ) . "'" ;
	}
	return $ret ;
}

function doesVIAFexist ( $viaf ) {
	if ( !isset($viaf) or $viaf == '' ) return false ;
	$viaf = trim($viaf) ;
	$sparql = "SELECT ?q { ?q wdt:P214 \"{$viaf}\" }" ;
	$items = getSPARQLitems ( $sparql ) ;
	return count($items) > 0 ;
}

function autoAssign ( $sql , $q ) {
	global $mnm , $catalogs ;
	if ( $q <= 0 ) return ;
#	print "$sql\n" ;
	$commands = [] ;
	$result = $mnm->getSQL ( $sql ) ;
	while ( $o = $result->fetch_object() ) {
		if ( isset($o->q) AND $o->user!=0 ) continue ;
		if ( $o->q < 0 ) continue ;
#		print_r ( $o ) ;
		$mnm->setMatchForEntryID ( $o->entry_id , $q , 4 , true ) ;
		if ( !isset($catalogs[$o->catalog]) ) continue ;
		$cat = (object) $catalogs[$o->catalog] ;
		if ( !isset($cat->wd_prop) or isset($cat->wd_qual) ) continue ;
		$sql = "SELECT ext_id FROM entry WHERE id={$o->entry_id}" ;
		$result2 = $mnm->getSQL ( $sql ) ;
		if (!( $o2 = $result2->fetch_object() )) continue ;
		$prop = 'P' . $cat->wd_prop ;
		$commands[] = "Q{$q}\t$prop\t\"{$o2->ext_id}\"" ;
	}
	runCommandsQS ( $commands ) ;
}

function checkAndCreate ( $name , $born , $died ) {
	global $mnm , $dbwd , $indicators , $catalogs , $min_commands , $min_catalogs ;

	if ( preg_match ( '/[\(\)]/' , $name ) ) {
		print "Bad name: {$name} ({$born}-{$died})\n" ;
		return ;
	}

	$name = preg_replace ( '/^(Hon\.) /' , '' , $name ) ;

	if ( strlen($born) == 0 or strlen($died) == 0 ) {
		print "$name: Dates are required\n" ;
		return ;
	}

	$search_results = $mnm->getSearchResults ( $name , 'P31' , 'Q5' ) ;
	if ( count($search_results) > 0 ) return ;

	$name_vars = getEscapedNameVariations($name) ;

	$auto_assign = [] ;
	$q4dates = [] ;
	$sql = "SELECT * FROM wb_terms WHERE term_text IN ({$name_vars}) AND term_entity_type='item' AND term_type IN ( 'label','alias')" ;
	$sql .= " AND EXISTS (SELECT * FROM page,pagelinks WHERE pl_from=page_id AND page_title=term_full_entity_id AND page_namespace=0 AND pl_namespace=0 AND pl_title='Q5')" ; # Human
	$result = getSQL ( $dbwd , $sql ) ;
	while ( ($o = $result->fetch_object()) ) {
		$q4dates[] = preg_replace ( '/\D/' , '' , $o->term_full_entity_id ) ;
	}
	if ( count($q4dates) > 0 ) {
		$hit = false ;
		$sql = "SELECT * FROM all_people_bd WHERE born={$born} AND died={$died} AND q IN (" . implode(',',$q4dates) . ")" ;
		$result = $mnm->getSQL ( $sql ) ;
		while ( ($o = $result->fetch_object()) ) {
			$auto_assign[] = $o->q ;
#			if ( $o->born == $born ) $hit = true ;
#			if ( $o->died == $died ) $hit = true ;
		}
		if ( count($auto_assign) != 1 and $hit ) {
			print "There are already items (without dates?) for label '{$name}' ({$born}-{$died}), skipping\n" ;
//			print_r ( $auto_assign ) ;
			return ;
		}
		// There is a name hit, but no date match, not even "just" born or died. Continue!
	}

	$do_not_add = [] ;
	$entries = [] ;
	$to_add = [] ;
	$sql = "SELECT * FROM vw_dates WHERE substr(born,1,".strlen($born).")='".$mnm->escape($born)."'" ;
	$sql .= " AND substr(died,1,".strlen($died).")='".$mnm->escape($died)."'" ;
	$sql .= " AND ext_name IN ({$name_vars})" ;

	if ( count($auto_assign) == 1 ) {
		$q = $auto_assign[0] ;
		print "Using {$q} for {$name} ({$born}-{$died})\n" ;
		autoAssign ( $sql , $q ) ;
		return ;
	}

	$result = $mnm->getSQL ( $sql ) ;
	while ( $o = $result->fetch_object() ) {
		if ( isset($o->q) AND $o->user!=0 ) {
			if ( $o->q <= 0 ) continue ; # N/A
#			print "There's already an item [Q{$o->q}] assigned to '{$name}' ({$born}-{$died})\n" ;
			autoAssign ( $sql , $o->q ) ;
#			print "$sql : {$o->q}\n" ;
			return ;
		}
		if ( isset($to_add[$o->catalog]) ) {
			print "Two or more entries with the same name '{$name}' in catalog #{$o->catalog}\n" ;
			$do_not_add[$o->catalog] = 1 ;
			continue ;
		}
		$to_add[$o->catalog] = $o ;
		$entries[$o->entry_id] = $o->entry_id ;
	}

	foreach ( $do_not_add AS $catalog => $one ) {
		unset ( $entries[$to_add[$catalog]->entry_id]) ;
		unset ( $to_add[$catalog] ) ;
	}

	if ( count($to_add) < $min_catalogs ) {
		print "Less than {$min_catalogs} catalogs for '$name'\n" ;
		return ;
	}

	$sql = "SELECT * FROM entry WHERE id IN (" . implode(',',$entries) . ")" ;
	$entries = [] ;
	$result = $mnm->getSQL ( $sql ) ;
	while ( $o = $result->fetch_object() ) $entries[$o->id] = $o ;

	$viaf = tryVIAF ( $name , $born , $died ) ;
	$additional = [] ;
	$assign_entries = [] ;
	$commands = [
		"LAST\tLen\t\"{$name}\"" ,
		"LAST\tP31\tQ5"
	] ;
	foreach ( $to_add AS $catalog_id => $o ) {
		if ( !isset($catalogs[$catalog_id]) ) continue ;
		$cat = (object) $catalogs[$catalog_id] ;

		$e = $entries[$o->entry_id] ;
		if ( !isset($e) ) continue ;

		$do_set_prop = isset($cat->wd_prop) and !isset($cat->wd_qual) ;
		if ( $do_set_prop ) {
			$commands[] = "LAST\tP{$cat->wd_prop}\t\"{$e->ext_id}\"" ;
		}

		$assign_entries[] = $o->entry_id ;
		if ( !isset($viaf) and preg_match ( '/VIAF\[(\d+)\]/' , $e->ext_desc , $m ) ) $viaf = $m[1] ;
		if ( preg_match ( '/LCAuth\[(\S+?)\]/' , $e->ext_desc , $m ) ) $additional['P244']['"'.$m[1].'"'] = 1 ;

		if ( strlen($o->born) > strlen($born) ) $born = $o->born ;
		if ( strlen($o->died) > strlen($died) ) $died = $o->died ;

		$lc_name = strtolower($name) ;
		$lc_desc = strtolower($e->ext_desc) ;
		foreach ( $indicators AS $k => $v ) {
			if ( @preg_match ( '/\b'.$k.'\b/' , $lc_name ) ) continue ;
			if ( !@preg_match ( '/\b'.$k.'\b/' , $lc_desc ) ) continue ;
			$p = explode ( "\t" , $v ) ;
			$additional[$p[0]][$p[1]] = 1 ;
		}
	}

	if ( doesVIAFexist ( $viaf ) ) {
		print "VIAF {$viaf} already in use: {$name} ({$born}-{$died})\n" ;
		return ;
	}
	addVIAFandRelated ( $viaf , $commands ) ;
	$commands[] = "LAST\tP569\t" . formatDateProperty ( $born ) ;
	$commands[] = "LAST\tP570\t" . formatDateProperty ( $died ) ;

	foreach ( $additional AS $prop => $values ) {
		foreach ( $values AS $v => $one ) {
			if ( $prop == 'P244' or $prop == 'P1207' ) $v = str_replace ( ' ' , '' , $v ) ; # Hardcoded LCCN hack
			$commands[] = "LAST\t{$prop}\t{$v}" ;
		}
	}

	if ( count($commands) < $min_commands ) {
		print "Less than {$min_commands} commands for '{$name}' ({$born}-{$died})\n" ;
		return ;
	}

	$commands = array_unique ( $commands ) ;
	array_unshift ( $commands , 'CREATE' ) ;
	$last_item = runCommandsQS ( $commands ) ;
	foreach ( $assign_entries AS $entry_id ) $mnm->setMatchForEntryID ( $entry_id , preg_replace('/\D/','',$last_item) , 4 ) ;
	print "CREATED NEW ITEM https://www.wikidata.org/wiki/{$last_item}\n" ;
}

$lc = new largeCatalog (2) ; # VIAF
$mnm = new MixNMatch ;
$dbwd = $mnm->tfc->openDB ( 'wikidata' , 'wikidata' , true , true ) ;
$qs = getQS() ;
$indicators = loadIndicators() ;

$catalogs = [] ;
$sql = "SELECT * FROM catalog WHERE `active`=1" ; // wd_prop IS NOT NULL and wd_qual IS NULL AND
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $catalogs[$o->id] = $o ;



$sql = "SELECT * FROM common_names_human WHERE cnt>={$min_cnt}" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	$name = $o->name ;
	list ( $born , $died ) = explode ( '-' , $o->dates ) ;
#	print "$name\t$born\t$died\n" ;
	checkAndCreate ( ucwords($name) , $born*1 , $died*1 ) ;
}

?>