#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

$min_aux_required = 0 ;
$date_required = 1 ;
$type = 'Q5' ;
$bad_name_patterns = [ '/\//' , '/[\(\)\[\]\{\}]/' , '/[°\?\!]/' , '/^\d+$/' , '/"/' ] ;

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;
require_once ( "/data/project/mix-n-match/manual_lists/large_catalogs/shared.php" ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

function getSearchQuery ( $entry ) {
	global $mnm ;
	$ret = $entry->ext_name ;
	if ( $entry->type == 'Q5' ) { # Person
		$ret = $mnm->sanitizePersonName ( $ret ) ;
		$ret .= ' haswbstatement:P31=Q5' ;
	}
	return $ret ;
}

if ( !isset($argv[1]) ) die ("USAGE: create_unmatched_entries_with_no_search_results.php CATALOG_ID [min_aux|0] [date_required(0/1/2|1)] [Qtype|Q5]\n" ) ;
$catalog = $argv[1] * 1 ;
if ( $catalog == 0 ) die ( "Bad catalog ID {$argv[1]}\n" ) ;
if ( isset($argv[2]) ) $min_aux_required = $argv[2]*1 ;
if ( isset($argv[3]) ) $date_required = $argv[3]*1 ;
if ( isset($argv[4]) ) $type = $argv[4] ;

$lc = new largeCatalog (2) ; # VIAF
$lc->prop2field[2]['P213'] = 'ISNI' ; // Activating for this specific purpose
$lc->prop2field[2]['P1006'] = 'NTA' ; // Activating for this specific purpose

$mnm = new MixNMatch ;

$mnm->tfc->getQS('mixnmatch:CreateNewItemsFromCatalogWithNoSearchResults from catalog '.$catalog,'',true) ;

$sql = "SELECT * FROM entry WHERE catalog={$catalog} AND (q IS NULL)" ;

$sql .= " AND id NOT IN (SELECT DISTINCT entry_id FROM multi_match WHERE catalog={$catalog})" ;
if ( $min_aux_required > 0 ) $sql .= " AND (SELECT count(*) FROM auxiliary WHERE entry_id=entry.id)>={$min_aux_required}" ;
if ( $date_required == 1 ) $sql .= " AND id IN (SELECT entry_id FROM vw_dates WHERE catalog={$catalog})" ;
if ( $date_required == 2 ) $sql .= " AND id IN (SELECT entry_id FROM vw_dates WHERE catalog={$catalog} AND born!='' AND died!='')" ;
if ( $type != '' ) $sql .= " AND `type`='{$type}'" ;
#$sql .= " and id=9306764" ; # TESTING

$result = $mnm->getSQL ( $sql ) ;
while ( $entry = $result->fetch_object() ) {
	// Paranoia
	if ( $entry->type == 'Q5' ) {
		$is_valid_name = true ;
		foreach ( $bad_name_patterns AS $pattern ) {
			if ( preg_match ( $pattern , $entry->ext_name ) ) $is_valid_name = false ;
		}
		if ( !$is_valid_name ) continue ;
	}

	// Try search
	$query = getSearchQuery ( $entry ) ;
	$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&format=json&srsearch=" . urlencode($query) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( count($j->query->search) > 0 ) {
		if ( count($j->query->search) == 1 ) {
			$q = $j->query->search[0]->title ;
			$mnm->setMatchForEntryID ( $entry->id , $q , 0 , true ) ;
		}
		continue ;
	}

	// Try aux data
	$proplist = str_replace ( 'P' , '' , implode ( ',' , array_keys($lc->prop2field[2]) ) ) ;
	$sql = "SELECT * FROM vw_aux WHERE id={$entry->id} AND aux_p IN ($proplist)" ;
	$sparql_parts = [] ;
	$result2 = $mnm->getSQL ( $sql ) ;
	while ( $o = $result2->fetch_object() ) {
		$prop = 'P' . $o->aux_p ;
		$sparql_parts [] = "{ ?q wdt:{$prop} '{$o->aux_name}'}" ;
	}
	if ( count($sparql_parts) > 0 ) {
		$sparql = "SELECT DISTINCT ?q { " . implode(' UNION ',$sparql_parts) . " }" ;
		$items = $mnm->tfc->getSPARQLitems ( $sparql ) ;
		if ( count($items) == 1 ) {
#			print "https://tools.wmflabs.org/mix-n-match/#/entry/{$entry->id} => https://www.wikidata.org/wiki/{$items[0]}\n" ;
			$mnm->setMatchForEntryID ( $entry->id , $items[0] , 4 , true , false ) ;
			continue ;
		} else if ( count($items) > 1 ) {
			print "https://tools.wmflabs.org/mix-n-match/#/entry/{$entry->id} could be " . json_encode($items) . "\n" ;
			continue ;
		}
	}

	// Create and run commands
	$commands = $mnm->getCreateItemForEntryCommands ( $entry , $lc ) ;
	$q = $mnm->tfc->runCommandsQS ( $commands ) ;
	if ( isset($q) ) $mnm->setMatchForEntryID ( $entry->id , $q , 4 , true ) ;
#	sleep(2);
}

?>
