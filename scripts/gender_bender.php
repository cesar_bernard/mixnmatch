#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;
require_once ( "/data/project/mix-n-match/manual_lists/large_catalogs/shared.php" ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

$lc_bnf = new largeCatalog (1) ;
$lc_gnd = new largeCatalog (3) ;
$mnm = new MixNMatch ;

$sparql = 'SELECT ?q ?bnf ?gnd { ?q wdt:P31 wd:Q5 . { { ?q wdt:P227 ?gnd } UNION { ?q wdt:P268 ?bnf } } MINUS { ?q wdt:P21 [] } }' ;
#$sparql .= ' LIMIT 20' ;
$j = $mnm->tfc->getSPARQL ( $sparql ) ;

$gnd2q = [] ;
$bnf2q = [] ;
foreach ( $j->results->bindings AS $b ) {
	$q = $mnm->tfc->parseItemFromURL ( $b->q->value ) ;
	if ( isset($b->bnf) ) $bnf2q[$b->bnf->value] = $q ;
	if ( isset($b->gnd) ) $gnd2q[$b->gnd->value] = $q ;
}

$commands = [] ;
$date_expression = $mnm->date2expression(date('Y-m-d')) ;
$had_that = [] ;

# BNF
if ( 1 ) {
	$sql = "SELECT ext_id,gender FROM bnf_person WHERE ext_id IN ('" . implode("','",array_keys($bnf2q)) . "') AND gender!=''" ;
	$result = $mnm->tfc->getSQL ( $lc_bnf->db , $sql ) ;
	while($o = $result->fetch_object()){
		if ( isset($bnf2q[$o->ext_id]) ) $q = $bnf2q[$o->ext_id] ;
		else continue ;
		if ( $o->gender == 'male' ) $gender_q = 'Q6581097' ;
		else if ( $o->gender == 'female' ) $gender_q = 'Q6581072' ;
		else continue ;
		if ( isset($had_that[$q]) ) {
			if ( $had_that[$q] != $gender_q ) unset($commands[$q]) ;
			continue ;
		}
		$commands[$q] = "{$q}\tP21\t{$gender_q}\tS248\tQ19938912\tS813\t{$date_expression}" ;
		$had_that[$q] = $gender_q ;
	}
}

# GND
if ( 1 ) {
	$sql = "SELECT ext_id,gender FROM gnd WHERE ext_id IN ('" . implode("','",array_keys($gnd2q)) . "') AND gender IN (41,43)" ; // 40=?, 41=M, 43=F
	$result = $mnm->tfc->getSQL ( $lc_gnd->db , $sql ) ;
	while($o = $result->fetch_object()){
		if ( isset($gnd2q[$o->ext_id]) ) $q = $gnd2q[$o->ext_id] ;
		else continue ;
		if ( $o->gender == 41 ) $gender_q = 'Q6581097' ;
		else if ( $o->gender == 43 ) $gender_q = 'Q6581072' ;
		else continue ;
		if ( isset($had_that[$q]) ) {
			if ( $had_that[$q] != $gender_q ) unset($commands[$q]) ;
			continue ;
		}
		$commands[$q] = "{$q}\tP21\t{$gender_q}\tS248\tQ36578\tS813\t{$date_expression}" ;
		$had_that[$q] = $gender_q ;
	}
}

# Check
foreach ( $commands AS $q => $dummy ) {
	if ( $lc_gnd->hasPropertyEverBeenRemovedFromItem($q,'P21') ) unset($commands[$q]) ; # Paranoia
}

$mnm->tfc->getQS('mixnmatch:genderImport') ;
$mnm->tfc->runCommandsQS ( $commands ) ;

?>