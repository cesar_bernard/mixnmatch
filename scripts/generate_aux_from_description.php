#!/usr/bin/php
<?PHP

require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ;
require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$mnm = new MixNMatch ;

if ( !isset($argv[1]) ) die ( "catalog required\n" ) ;
$catalog = $argv[1] * 1 ;
if ( $catalog == 0 ) dir ( "Bad catalog ID {$argv[1]}\n" ) ;

$p_nationality = 27 ;
$p_gender = 21 ;
$p_occupation = 106 ;
$q_male = 'Q6581097' ;
$q_female = 'Q6581072' ;
$q_artist = 'Q483501' ;

$lists = [
	'countries'=>['sparql'=>'SELECT DISTINCT ?q ?label { ?q wdt:P31 wd:Q7275 ; rdfs:label ?label }','property'=>'P27'] ,
	'occupations'=>['sparql'=>'SELECT DISTINCT ?q ?label { ?q wdt:P31|wdt:P279 wd:Q28640 ; rdfs:label ?label }','property'=>'P106']
] ;

function checkAgainstList ( $entry_id , $list , $text , $property = '' ) {
	global $lists , $labels , $mnm ;
	$text = trim ( strtolower ( $text ) ) ;
	if ( $property == '' ) $property = $lists[$list]['property'] ; # Default property
	if ( isset($labels[$list][$text]) and $labels[$list][$text] != '' ) {
#		print "$list: '{$text}' => {$property}:{$labels[$list][$text]} for #{$entry_id}\n" ;
		$mnm->setAux ( $entry_id , $property , $labels[$list][$text] ) ;
	}
}

# Load lists
$labels = [] ;
foreach ( $lists AS $group => $list_data ) {
	$labels[$group] = [] ;
	$j = $mnm->tfc->getSPARQL ( $list_data['sparql'] ) ;
	foreach ( $j->results->bindings AS $b ) {
		$label = strtolower ( $b->label->value ) ;
		$q = $mnm->tfc->parseItemFromURL ( $b->q->value ) ;
		if ( isset($labels[$group][$label]) and $labels[$group][$label] != $q ) $labels[$group][$label] = '' ;
		else $labels[$group][$label] = $q ;
	}
}

$sql = "SELECT * FROM entry WHERE catalog={$catalog}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){

	if ( $catalog == 553 ) {
		if ( !preg_match ( '/\(([0-9\.\-]+),([0-9\.\-]+)\)/' , $o->ext_desc , $m ) ) continue ;
		$sql = "INSERT IGNORE INTO location (entry,lat,lon) VALUES ({$o->id},{$m[1]},{$m[2]})" ;
		$mnm->getSQL ( $sql ) ;
	}

	if ( $catalog == 691 ) {
		if ( !preg_match ( '/lat:([0-9\.\-]+), lng:([0-9\.\-]+)/' , $o->ext_desc , $m ) ) continue ;
		$sql = "INSERT IGNORE INTO location (entry,lat,lon) VALUES ({$o->id},{$m[1]},{$m[2]})" ;
		$mnm->getSQL ( $sql ) ;
	}

	if ( $catalog == 1538 ) {
		if ( preg_match ( '/Country of origin:\s*(.+?)\s*,/i' , $o->ext_desc , $m ) ) {
			checkAgainstList ( $o->id , 'countries' , $m[1] ) ;
		}

		if ( preg_match ( '/Gender:\s*Male/i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_male ) ;
		if ( preg_match ( '/Gender:\s*Female/i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_female ) ;
		if ( !isset($o->q) or $o->q === null ) $mnm->setAux ( $o->id , $p_occupation , $q_artist ) ;
	}

	if ( $catalog == 1653 ) {
		if ( !preg_match ( '/; (.+)$/' , $o->ext_desc , $m ) ) continue ;
		$parts = explode ( ' ' , str_replace ( '/' , ' ' , $m[1] ) ) ;
		if ( count($parts) == 2 ) {
			checkAgainstList ( $o->id , 'countries' , $parts[0] ) ;
			checkAgainstList ( $o->id , 'occupations' , $parts[1] ) ;
		} else if ( count($parts) == 3 ) {
			checkAgainstList ( $o->id , 'countries' , $parts[0] ) ;
			checkAgainstList ( $o->id , 'occupations' , $parts[1].' '.$parts[2] ) ;
			checkAgainstList ( $o->id , 'countries' , $parts[0].' '.$parts[1] ) ;
			checkAgainstList ( $o->id , 'occupations' , $parts[2] ) ;
			if ( preg_match ( '/\//',$o->ext_desc) ) {
				checkAgainstList ( $o->id , 'occupations' , $parts[1] ) ;
			}
		} else if ( count($parts) == 4 ) {
			checkAgainstList ( $o->id , 'countries' , $parts[0].' '.$parts[1] ) ;
			checkAgainstList ( $o->id , 'occupations' , $parts[2].' '.$parts[3] ) ;
		}
	}

	if ( $catalog == 1674 ) {
		if ( preg_match ( '|\borcid (\d{4}-\d{4}-\d{4}-\S{4})\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 496 , $m[1] ) ;
		if ( preg_match ( '|\bidref (\S+)\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 269 , $m[1] ) ;
		if ( preg_match ( '|\bisni (\S+)\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 213 , $m[1] ) ;
		if ( preg_match ( '|\bviaf (\S+)\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 214 , $m[1] ) ;
		if ( preg_match ( '|\bresearcherid (\S+)\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 1053 , $m[1] ) ;
	}

	if ( $catalog == 1827 ) {
		if ( preg_match ( '|\bcomponist\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q36834' ) ;
		if ( preg_match ( '|\bregisseur\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q3455803' ) ;
		if ( preg_match ( '|\bschrijver\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q36180' ) ;
		if ( preg_match ( '|\bdirigent\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q158852' ) ;
		if ( preg_match ( '|\bacteur\b|i' , $o->ext_desc , $m ) ) { $mnm->setAux ( $o->id , 106 , 'Q33999' ) ; $mnm->setAux ( $o->id , 21 , 'Q6581097' ) ; }
		if ( preg_match ( '|\bactrice\b|i' , $o->ext_desc , $m ) ) { $mnm->setAux ( $o->id , 106 , 'Q33999' ) ; $mnm->setAux ( $o->id , 21 , 'Q6581072' ) ; }
	}

}

?>