#!/usr/bin/php
<?PHP

$toolname = 'inaturalist_sync_magnus' ;

require_once ( '/data/project/wikidata-todo/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/wikidata-todo/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;


$name2q = array (
	'cultivar' => 'Q4886' ,
	'species' => 'Q7432' ,
	'genus' => 'Q34740' ,
	'family' => 'Q35409' ,
	'order' => 'Q36602' ,
	'kingdom' => 'Q36732' ,
	'class' => 'Q37517' ,
	'phylum' => 'Q38348' ,
	'subspecies' => 'Q68947' ,
	'domain' => 'Q146481' ,
	'tribe' => 'Q227936' ,
	'form' => 'Q279749' ,
	'division' => 'Q334460' ,
	'subvariety' => 'Q630771' ,
	'cryptic species complex' => 'Q765940' ,
	'variety' => 'Q767728' ,
	'subphylum' => 'Q1153785' ,
	'nothospecies' => 'Q1306176' ,
	'superspecies' => 'Q1783100' ,
	'infraclass' => 'Q2007442' ,
	'superfamily' => 'Q2136103' ,
	'infraphylum' => 'Q2361851' ,
	'subfamily' => 'Q2455704' ,
	'subkingdom' => 'Q2752679' ,
	'infraorder' => 'Q2889003' ,
	'cohorte' => 'Q2981883' ,
	'series' => 'Q3025161' ,
	'infrakingdom' => 'Q3150876' ,
	'section' => 'Q3181348' ,
	'subgenus' => 'Q3238261' ,
	'branch' => 'Q3418438' ,
	'subdomain' => 'Q3491996' ,
	'subdivision' => 'Q3491997' ,
	'superclass' => 'Q3504061' ,
	'forma specialis' => 'Q3825509' ,
	'subtribe' => 'Q3965313' ,
	'superphylum' => 'Q3978005' ,
	'group' => 'Q4150646' ,
	'infracohort' => 'Q4226087' ,
	'form' => 'Q5469884' ,
	'infrafamily' => 'Q5481039' ,
	'subclass' => 'Q5867051' ,
	'suborder' => 'Q5867959' ,
	'superorder' => 'Q5868144' ,
	'subsection' => 'Q5998839' ,
	'nothogenus' => 'Q6045742' ,
	'magnorder' => 'Q6054237' ,
	'supercohort' => 'Q6054425' ,
	'infralegion' => 'Q6054535' ,
	'sublegion' => 'Q6054637' ,
	'superlegion' => 'Q6054795' ,
	'parvorder' => 'Q6311258' ,
	'grandorder' => 'Q6462265' ,
	'legion' => 'Q7504331' ,
	'mirorder' => 'Q7506274' ,
	'subcohorte' => 'Q7509617' ,
	'species group' => 'Q7574964' ,
	'epifamily' => 'Q10296147' ,
	'subsection' => 'Q10861375' ,
	'section' => 'Q10861426' ,
	'subseries' => 'Q13198444' ,
	'subform' => 'Q13202655' ,
	'supertribe' => 'Q14817220' ,
	'superkingdom' => 'Q19858692' ,
	'subterclass' => 'Q21061204' ,
	'hyporder' => 'Q21074316'
) ;

function getQS () {
	global $toolname ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/mix-n-match/reinheitsgebot.conf" ;
	$qs->toolname = $toolname ;
	$qs->sleep = 1 ;
	$qs->use_command_compression = true ;
	return $qs ;
}

function followURLandReturnHTML ( $url ) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
//	curl_setopt($ch, CURLOPT_HEADER, TRUE);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$a = curl_exec($ch);
	return $a ;
}

function logme ( $s ) {
	print "$s\n" ;
}

function readEntryFromINaturalist ( $id , &$tfc ) {
	if ( !preg_match ( '/^\d+$/' , $id ) ) return logme ( "$id is not numeric" ) ;
	$url = "https://www.inaturalist.org/taxa/$id" ;
	$html = followURLandReturnHTML ( $url ) ;
	if ( !preg_match ( '/\n\s*taxon:\s*(\{.+)\.results/' , $html , $m ) ) return logme ( "Can't parse HTML of $url" ) ;
	$j = json_decode ( $m[1] ) ;
	if ( $j == null ) return logme ( "Can't parse JSON in $url" ) ;
	if ( !isset($j->results) or count($j->results) == 0 ) return logme ( "No results in $url" ) ;
	$j = $j->results[0] ;
	if ( !$j->is_active ) return logme ( "$id is inactive" ) ;
	if ( $j->id != $id ) return logme ( "ID mismatch for $url" ) ;
	if ( $j->wikipedia_url != null ) return logme ( "$id has Wikipedia URL {$j->wikipedia_url}" ) ;
	if ( $j->current_synonymous_taxon_ids != null ) return logme ( "$id has synonymous taxa" ) ;
	if ( !isset($j->name) ) return logme ( "No name on $url" ) ;
	if ( !isset($j->parent_id) ) return logme ( "No parent ID on $url" ) ;
	if ( !isset($j->rank) ) return logme ( "No rank on $url" ) ;

	$keys = [] ;
	$values = [] ;

	$keys[] = 'id' ; $values[] = $tfc->dbt->real_escape_string ( $j->id ) ;
	$keys[] = 'parent_taxon' ; $values[] = $tfc->dbt->real_escape_string ( $j->parent_id ) ;
	$keys[] = 'name' ; $values[] = $tfc->dbt->real_escape_string ( $j->name ) ;
	$keys[] = 'rank' ; $values[] = $tfc->dbt->real_escape_string ( $j->rank ) ;
	$keys[] = 'extinct' ; $values[] = $j->extinct?1:0 ;
	if ( isset ( $j->preferred_common_name ) ) {
		$keys[] = 'common_name' ;
		$values[] = $tfc->dbt->real_escape_string ( $j->preferred_common_name ) ;
	}

	if ( preg_match ( '/\(also known as (.+?)\)/' , $html , $m ) ) {
		$list = explode ( ' and ' , $m[1] ) ;
		$alt = [] ;
		foreach ( $list AS $name ) {
			$name = trim ( str_replace ( ',' , '' , $name ) ) ;
			if ( strtolower($name) == strtolower($j->name) ) continue ;
			if ( strtolower($name) == strtolower($j->preferred_common_name) ) continue ;
			$alt[] = $name ;
		}
		$alt = json_encode ( $alt ) ;
		$keys[] = 'alternate_names' ;
		$values[] = $tfc->dbt->real_escape_string ( $alt ) ;
	}

	$sql = "INSERT IGNORE INTO inaturalist (`" . implode('`,`',$keys) . "`) VALUES ('" . implode("','",$values) . "')" ;
	$tfc->getSQL ( $tfc->dbt , $sql ) ;
}


// $commands = [] , one QS V1 command per element, no newlines
function runCommandsQS ( $commands ) {
	global $qs ;
	$commands = implode ( "\n" , $commands ) ;
	$tmp_uncompressed = $qs->importData ( $commands , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp_uncompressed['data']['commands'] ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	if ( !isset($qs->last_item) ) return ;
	$last_item = 'Q' . preg_replace ( '/\D/' , '' , "{$qs->last_item}" ) ;
	return $last_item ;
}


function initializeIN2Q ( &$in2q , &$tfc ) {
	$sparql = 'SELECT ?q ?v { ?q wdt:P3151 ?v }' ;
	$j = $tfc->getSPARQL ( $sparql ) ;
	foreach ( $j->results->bindings AS $b ) {
		$q = $tfc->parseItemFromURL ( $b->q->value ) ;
		$in = $b->v->value ;
		$in2q[$in] = $q ;
	}
}

function setQforID ( $q , $id , &$tfc ) {
	$q = preg_replace ( '/\D/' , '' , "$q" ) ;
	$sql = "UPDATE inaturalist SET q=$q WHERE id=$id" ;
	$tfc->getSQL ( $tfc->dbt , $sql ) ;
}

function createNewItem ( $o , &$in2q , &$tfc ) {
	global $name2q ;
	if ( isset($in2q[$o->id]) ) {
		setQforID ( $in2q[$o->id] , $o->id , $tfc ) ;
		return logme ( "{$o->id} already has an item on Wikidata" ) ;
	}
	if ( !isset($name2q[$o->rank]) ) return logme ( "Unknown rank '{$o->rank}'" ) ;
	if ( !isset($in2q[$o->parent_taxon]) ) {
		return logme ( "Unknown parent taxon http://www.inaturalist.org/taxa/{$o->parent_taxon}" ) ;
	}
	if ( $o->alternate_names!='' and $o->alternate_names!='[]' ) {
		return logme ( "{$o->id} has alternate names, skipping for now" ) ;
	}

	if ( preg_match ( '/\bsedis\b/' , $o->name ) ) return logme ( "Skipping 'sedis' {$o->id}") ;
	if ( $o->rank == 'species' AND !preg_match ( '/^\S+ \S+$/' , $o->name ) ) return logme ( "Bad name for {$o->rank}: {$o->name}" ) ;
	if ( $o->rank == 'subspecies' AND !preg_match ( '/^\S+ \S+ \S+$/' , $o->name ) ) return logme ( "Bad name for {$o->rank}: {$o->name}" ) ;
	if ( $o->rank != 'species' AND $o->rank != 'subspecies' AND !preg_match ( '/^\S+$/' , $o->name ) ) return logme ( "Bad name for {$o->rank}: {$o->name}" ) ;

	$items = $tfc->getSPARQLitems ( "SELECT ?q { ?q wdt:P225 '{$o->name}' }" , 'q' ) ;
	if ( count($items) > 0 ) {
		return logme ( "Possible items for {$o->id}: " . implode(', ',$items) ) ;
	}

	$parent_taxon_item = $in2q[$o->parent_taxon] ;
	$rank_item = $name2q[$o->rank] ;

	$label = $o->name ;
	if ( $o->common_name != '' ) $label = $o->common_name ;

	$commands = [] ;
	$commands[] = "CREATE" ;
	$commands[] = "LAST\tP31\tQ16521" ;
	$commands[] = "LAST\tLen\t\"$label\"" ;
	$commands[] = "LAST\tP225\t\"{$o->name}\"" ;
	$commands[] = "LAST\tP105\t$rank_item" ;
	$commands[] = "LAST\tP171\t{$parent_taxon_item}" ;
	$commands[] = "LAST\tP3151\t\"{$o->id}\"" ;

	$last_item = runCommandsQS ( $commands ) ;
	setQforID ( $last_item , $o->id , $tfc ) ;
	$in2q[$o->id] = $last_item ;

	$ts = date ( 'YmdHis' ) ;
	$sql = "UPDATE entry SET q=" . preg_replace('/\D/','',$last_item) . ",user=2,timestamp='$ts' WHERE catalog=238 AND ext_id='{$o->id}' AND (q IS NULL or user=0)" ; # $o->id is numeric as per DB
	$tfc->getSQL ( $tfc->dbt , $sql ) ;

	print "CREATED https://www.wikidata.org/wiki/$last_item for {$o->id}\n" ;
#	print "$last_item\n" ;
#	print_r ( $commands ) ; exit ( 0 ) ;
}

$tfc = new ToolforgeCommon ( $toolname ) ;
$tfc->dbt = $tfc->openDBtool ( 'mixnmatch_p' ) ;

if ( 0 ) { // Iterate over unmatched entries, and get info from website
	$sql = "SELECT ext_id FROM entry WHERE catalog=238 AND q IS NULL" ;
	$sql .= "  AND ext_id NOT IN (SELECT id FROM inaturalist)" ; // Incremental update
	$result = $tfc->getSQL ( $tfc->dbt , $sql ) ;
	while($o = $result->fetch_object()){
		readEntryFromINaturalist ( $o->ext_id , $tfc ) ;
	}
}

if ( 1 ) { // Create Wikidata entries
	$qs = getQS() ;
	$in2q = [] ;
	initializeIN2Q ( $in2q , $tfc ) ;
	$sql = "SELECT * FROM inaturalist WHERE q=0" ;
#	$sql .= " AND rank IN ('tribe','class','order','suborder','subgenus','order','family','subtribe','subfamily','genus','subclass')" ; # Fiddling
	$result = $tfc->getSQL ( $tfc->dbt , $sql ) ;
	while($o = $result->fetch_object()){
		createNewItem ( $o , $in2q , $tfc ) ;
	}
}

?>