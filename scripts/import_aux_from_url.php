#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

if ( !isset($argv[1]) ) die ( "CATALOG required\n" ) ;
$catalog = $argv[1] * 1 ;
if ( $catalog == 0 ) die ( "Bad catalog {$argv[1]}\n" ) ;

$mnm = new MixNMatch ;

$sql = "SELECT * FROM entry WHERE catalog={$catalog} AND ext_url!=''" ;
$sql .= " AND (user=0 or q is null)" ;
#$sql .= " AND id=58140150" ; # TESTING
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	$html = file_get_contents ( $o->ext_url ) ;
	$html = preg_replace ( '/\s+/' , ' ' , $html ) ; # Simple spaces

	if ( preg_match ( '/href=["\']https{0,1}:\/\/d-nb\.info\/gnd\/(\d+X{0,1})["\']/' , $html , $m ) ) { # GND
		$mnm->setAux ( $o->id , 227 , $m[1] ) ;
	}

	if ( preg_match ( '/href=["\']https{0,1}:\/\/viaf\.org\/viaf\/(\d+)/' , $html , $m ) ) { # VIAF
		$mnm->setAux ( $o->id , 214 , $m[1] ) ;
	}

	if ( preg_match ( '|<A TARGET="_blank" HREF="http://www.nga.gov/content/ngaweb/Collection/artist-info\.(\d+)\.html|' , $html , $m ) ) { #  National Gallery of Art artist ID (P2252) 
		$mnm->setAux ( $o->id , 2252 , $m[1] ) ;
	}
}

?>