#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( '/data/project/mix-n-match/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/mix-n-match/scripts/mixnmatch.php' ) ;

$year_is_enough = false ;
$catalog = $argv[1] ;

function findBirtdateMatch ( $entry_id , $born , $qs ) {
	global $mnm , $wil ;
	$wil->loadItems ( $qs ) ;
	$matches = [] ;
	$compare_birth = '+' . $born ;
	foreach ( $qs AS $q ) {
		$q = 'Q' . preg_replace ( '/\D/' , '' , "$q" ) ;
		$i = $wil->getItem ( $q ) ;
		if ( !isset($i) ) continue ;
		$claims = $i->getClaims('P569') ;
		foreach ( $claims AS $c ) {
			$time = $c->mainsnak->datavalue->value->time ;
			if ( substr($time,0,strlen($compare_birth)) == $compare_birth ) $matches[$q] = $q ;
		}
	}
	if ( count($matches) == 0 ) return ;
	if ( count($matches) > 1 ) {
		print "MULTIPLE CANDIDATES: https://tools.wmflabs.org/mix-n-match/#/entry/{$entry_id} / {$born} :\n" ;
		foreach ( $matches AS $q ) {
			print "* https://www.wikidata.org/wiki/$q\n" ;
		}
		return ;
	}
	$q = array_pop ( $matches ) ;
#	print "https://tools.wmflabs.org/mix-n-match/#/entry/{$entry_id} => https://www.wikidata.org/wiki/{$q}\n" ;
	$mnm->setMatchForEntryID ( $entry_id , $q , '4' , true , false ) ;
}

function matchAll ( $sql ) {
	global $mnm ;
	$sql .= " AND length(born)=10" ; # Only full days
	$sql .= " AND substr(born,1,4)*1>1900" ; # Only potentially living people
#	$sql .= " LIMIT 50" ; # TESTING
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		if ( $year_is_enough ) $o->born = substr ( $o->born , 0 , 4 ) ;
		findBirtdateMatch ( $o->entry_id , $o->born , explode (',',$o->qs) ) ;
	}
}

$mnm = new MixNMatch ;
$wil = new WikidataItemList ;

// MULTI-MATCH
$sql = "SELECT multi_match.entry_id,born,candidates AS qs FROM vw_dates,multi_match WHERE died='' AND (q IS NULL OR user=0) AND vw_dates.entry_id=multi_match.entry_id" ;
if ( isset($catalog) ) $sql .= " AND multi_match.catalog={$catalog}" ;
matchAll ( $sql ) ;

// AUTO-MATCH
$sql = "SELECT entry_id,born,q qs FROM vw_dates WHERE died='' AND (q is null or user=0)" ;
if ( isset($catalog) ) $sql .= " AND catalog={$catalog}" ;
matchAll ( $sql ) ;

?>