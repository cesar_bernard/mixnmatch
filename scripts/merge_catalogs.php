#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR); // 
ini_set('display_errors', 'On');

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$cat_source = $argv[1] * 1 ;
$cat_target = $argv[2] * 1 ;

if ( $cat_source * $cat_target == 0 ) die ( "USAGE: merge_catalogs.php SOURCE_CATALOG_ID TARGET_CATALOG_ID\n" ) ;

$mnm = new MixNMatch ;

function addMissingBlankEntries ( &$mnm , $cat_source , $cat_target ) {
	$source_ids = [] ;
	$result = $mnm->getSQL ( "SELECT ext_id FROM entry WHERE catalog={$cat_source}" ) ;
	while($o = $result->fetch_object()) $source_ids[$o->ext_id] = $o->ext_id ;

	$target_ids = [] ;
	$result = $mnm->getSQL ( "SELECT ext_id FROM entry WHERE catalog={$cat_target}" ) ;
	while($o = $result->fetch_object()) $target_ids[$o->ext_id] = $o->ext_id ;

	$add2noq = 0 ;
	foreach ( $source_ids AS $ext_id ) {
		if ( isset($target_ids[$ext_id]) ) continue ;
		$sql = "INSERT INTO entry (catalog,ext_id,ext_url,ext_name,ext_desc,q,user,`timestamp`,random,`type`) SELECT {$cat_target},ext_id,ext_url,ext_name,ext_desc,NULL,NULL,NULL,random,`type` FROM entry WHERE catalog={$cat_source} AND ext_id='" . $mnm->escape($ext_id) . "'" ;
		$mnm->getSQL ( $sql ) ;
		$add2noq++ ;
	}
	if ( $add2noq == 0 ) return ;
	$sql = "UPDATE overview SET noq=noq+{$add2noq} WHERE catalog={$cat_target}" ;
	$mnm->getSQL ( $sql ) ;
	$mnm->updateOverviewFile() ;
}

function updateEntriesInNewCatalog ( &$mnm , $cat_source , $cat_target ) {
	$sql = "SELECT e2.id AS new_id,e1.* FROM entry e1,entry e2 WHERE e1.catalog={$cat_source} AND e2.catalog={$cat_target} AND e1.ext_id=e2.ext_id AND e1.q!=e2.q AND e1.q IS NOT NULL AND e1.user>0 AND ((e1.user!=0 AND e2.user=0) OR e2.user IS NULL)" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		$mnm->setMatchForEntryID ( $o->new_id , $o->q , $o->user , true ) ;

		# Reset timestamp
		$sql = "UPDATE entry SET `timestamp`={$o->timestamp} WHERE id={$o->new_id}" ;
		$mnm->getSQL ( $sql ) ;
		print "{$o->new_id}\n" ;
	}
}


addMissingBlankEntries ( $mnm , $cat_source , $cat_target ) ;
updateEntriesInNewCatalog ( $mnm , $cat_source , $cat_target ) ;
$mnm->deactivateCatalog ( $cat_source ) ;

?>