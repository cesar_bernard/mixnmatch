<?PHP

require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ;
require_once ( '/data/project/mix-n-match/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/mix-n-match/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

class MixNMatch {
	public $dbm , $dbwd ;
	public $last_error = '' ;
	public $last_entry = [] ;
	public $testing = false ;
	public $tfc ;
	public $wil ;
	public $similar_languages = ['en','de','fr','es','it','pt','nl','gl'] ;

	private $overview_filename = '/data/project/mix-n-match/public_html/overview.json' ;
	private $user_cache = [] ;
	private $catalogs = [] ;
	
	function __construct() {
		$this->tfc = new ToolforgeCommon('mix-n-match') ;
		$this->tfc->use_db_cache = false ;
		$this->dbm = $this->openMixNMatchDB() ;
		$this->wil = new WikidataItemList ;
	}

	private function openMixNMatchDB () {
		$db = $this->tfc->openDBtool ( 'mixnmatch_p' , '' , '' , false ) ;
		if ( $db === false ) die ( "Cannot access DB: " . $o['msg'] ) ;
		$db->set_charset("utf8") ;
		return $db ;
	}

	private function logError ( $msg = 'Unspecified error' ) {
		$this->last_error = $msg ;
		return false ;
	}

	public function escape ( $s ) {
		return $this->dbm->real_escape_string ( $s ) ;
	}

	public function getSQL ( $sql ) {
		if ( !isset($this->dbm) ) $this->dbm = $this->openMixNMatchDB() ;
		return $this->tfc->getSQL ( $this->dbm , $sql , 5 ) ;
	}

	public function getEntryObjectFromID ( $entry_id ) {
		$sql = "SELECT * FROM entry WHERE id={$entry_id} LIMIT 1" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) return $o ;
	}

	public function getAllCatalogIDs () {
		$catalogs = [] ;
		$sql = "SELECT id from catalog" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $catalogs[] = $o->id ;
		return $catalogs ;
	}

	public function isAutoMatchUser ( $user_id ) {
		if ( $user_id == 0 ) return true ;
		if ( $user_id == 3 ) return true ;
		if ( $user_id == 4 ) return true ;
		return false ;
	}

	public function isUserBlocked ( $user_name ) {
		$uid = $this->getOrCreateUserID ( $user_name ) ;
		if ( $uid == -1 ) return false ; // Paranoia
		$user_key = $this->getUserKey ( $user_name ) ;
		if ( !isset($this->user_cache[$user_key]) ) return false ; // Paranoia
		if ( time() - $this->user_cache[$user_key]['last_block_check'] > 60*15 ) { // Cache block check for 15 min
			$this->user_cache[$user_key]['is_blocked'] = $this->checkUserBlock ( $user_name ) ;
		}
		return $this->user_cache[$user_key]['is_blocked'] ;
	}

	private function checkUserBlock ( $user_name ) {
		$url = "https://www.wikidata.org/w/api.php?action=query&list=users&ususers=".urlencode($user_name)."&usprop=blockinfo&format=json" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		return isset ( $j->query->users[0]->blockid ) ;
	}

	private function getUserKey ( $user_name ) {
		return str_replace ( ' ' , '_' , trim($user_name) ) ;
	}

	public function getOrCreateUserID ( $user_name_original ) {
		$user_key = $this->getUserKey ( $user_name_original ) ;
		if ( isset($this->user_cache[$user_key]) ) return $this->user_cache[$user_key]['id'] ;

		$user_name = $this->escape($user_name_original) ;
		$last_block_check = time() ;
		$sql = "INSERT IGNORE user (name,last_block_check) VALUES ('{$user_name}','{$last_block_check}')" ;
		$this->getSQL ( $sql ) ;
		$user_id = -1 ;
		$sql = "SELECT * FROM user WHERE name='{$user_name}'" ;
		$result = $this->getSQL ( $sql ) ;
		if ($o = $result->fetch_object()) {
			$user_id = $o->id ;
			$this->user_cache[$user_key] = array (
				"id" => $o->id ,
				"last_block_check" => $o->last_block_check ,
				"is_blocked" => $this->checkUserBlock ( $user_name_original )
			) ;
		}
		return $user_id ;
	}

	public function sanitizePersonName ( $name ) {
		$ret = $name ;
		$ret = preg_replace ( '/^(Sir|Mme|Dr|Mother|Father)\.{0,1} /' , '' , $ret ) ;
		$ret = preg_replace ( '/\b[A-Z]\. /' , ' ' , $ret ) ; // M. Y. Wiener
		$ret = preg_replace ( '/ (\&) /' , ' ' , $ret ) ;
		$ret = preg_replace ( '/\(.+?\)/' , ' ' , $ret ) ;
		$ret = trim ( preg_replace ( '/\s+/' , ' ' , $ret ) ) ;
		return $ret ;
	}


	public function getSearchResults ( $query , $property = '' , $value = '' ) {
		$ret = [] ;
		if ( $property != '' and $value != '' ) {
			if ( preg_match ( '/ /' , $value ) ) $value = '"' . $value . '"' ;
			$query .= " haswbstatement:{$property}={$value}" ;
		}
		$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
		$result = @file_get_contents ( $url ) ;
		if ( !isset($result) or $result === null ) {
			$this->logError ( "MixNMatch::getSearchResults : query failed: {$url}" ) ;
			return $ret ;
		}
		$j = @json_decode ( $result ) ;
		if ( !isset($j) or $j === null or !isset($j->query) or !isset($j->query->search) ) {
			$this->logError ( "MixNMatch::getSearchResults : bad JSON: {$url}" ) ;
			return $ret ;
		}
		return $j->query->search ;
	}

	# Optional; aux as array of arrays like ['P123','the value']
	# q can be a unset, a single value (then set as user=4), or an array (then added as multi_match)
	public function addNewEntry ( $o ) {
		if ( is_array($o) ) $o = (object) $o ;
		if ( !is_object($o) ) die ( "mnm::addNewEntry - not an object\n" ) ;
		foreach ( ['catalog','id','name'] AS $k ) {
			if ( !isset($o->$k) or trim($o->$k) == '' ) die ( "mnm::addNewEntry - object requires ->{$k}\n" ) ;
		}
		$q = 0 ;
		if ( isset($o->q) ) {
			if ( is_array($o->q) ) $q = $o->q ;
			else if ( preg_match('/^Q(\d+)$/i',$o->q,$m) ) $q = $m[1] * 1 ;
		}
		$sql = "INSERT IGNORE INTO entry (catalog,ext_id,ext_url,ext_name,ext_desc,`type`,random" ;
		if ( !is_array($q) and $q > 0 ) $sql .= ",`q`,`user`,`timestamp`" ;
		$sql .= ") VALUES (" ;
		$sql .= $this->escape(trim($o->catalog)) . "," ;
		$sql .= "'" . $this->escape(trim($o->id)) . "'," ;
		if ( isset($o->url) ) $sql .= "'" . $this->escape(trim($o->url)) . "'," ;
		else $sql .= "''," ;
		$sql .= "'" . $this->escape($o->name) . "'," ;
		if ( isset($o->desc) ) $sql .= "'" . $this->escape(substr(trim($o->desc),0,250)) . "'," ;
		else $sql .= "''," ;
		if ( isset($o->type) ) $sql .= "'" . $this->escape(trim($o->type)) . "'," ;
		else $sql .= "''," ;
		$sql .= "rand()" ;
		if ( !is_array($q) and $q > 0 ) $sql .= ",{$q},4,'" . $this->getCurrentTimestamp() . "'" ;
		$sql .= ")" ;
		$this->getSQL ( $sql ) ;
		$entry_id = $this->dbm->insert_id ;
		if ( isset($o->aux) ) {
			foreach ( $o->aux AS $a ) $this->setAux ( $entry_id , $a[0] , $a[1] ) ;
		}
		if ( is_array($q) ) {
			$qc = count ( $q ) ;
			$q = implode ( ',' , $q ) ;
			$q = preg_replace ( '/[^0-9,]/' , '' , $q ) ;
			$sql = "REPLACE INTO multi_match (entry_id,catalog,candidates,candidate_count) VALUES ({$entry_id},{$o->catalog},'$q',$qc)" ;
			$this->getSQL ( $sql ) ;
		}
		return $entry_id ;
	}

	public function setAux ( $entry_id , $property , $value ) {
		$entry_id *= 1 ;
		if ( $entry_id == 0 ) return ;
		$property = preg_replace ( '/\D/' , '' , "{$property}" ) ;
		if ( $property == '' ) return ;
		$value = trim ( $value ) ;
		if ( $value == '' ) return ;

		if ( $property == 213 and preg_match ( '|(\S{4})(\S{4})(\S{4})(\S{4})|' , $value , $m) ) $value = "{$m[1]} {$m[2]} {$m[3]} {$m[4]}" ; # ISNI WD peculiarity

		$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) VALUES ({$entry_id},{$property},'".$this->escape($value)."')" ;
		$this->getSQL ( $sql ) ;
	}

	public function getItemForEntryID ( $entry_id ) {
		$entry_id *= 1 ;
		$sql = "SELECT q FROM entry WHERE id={$entry_id}" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) return $o->q ;
	}

	public function avoidEntryAutomatch ( $entry_id , $q = -1 ) {
		$entry_id *= 1 ;
		$sql = "SELECT * FROM log WHERE entry={$entry_id}" ;
		if ( isset($q) and $q != -1 ) $sql .= " AND (q IS NULL OR q=".preg_replace('/\D/','',$q).")" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) return true ;
		return false ;
	}

	public function sanitizeQ ( &$q ) {
		if ( !isset($q) or $q === null ) return ;
		if ( preg_match ( '/^[PQ](\d+)$/' , $q , $m ) ) $q = $m[1] ;
		$q *= 1 ;
	}

	# Returns true if match was stored in database, false otherwise
	public function setMatchForEntryID ( $entry_id , $q , $user_id , $no_overwrite_manual = false , $allow_q_zero = true ) {
		$entry_id *= 1 ;
		$this->sanitizeQ ( $q ) ;
		$user_id *= 1 ;

		if ( $entry_id == 0 ) return $this->logError ( "Bad entry ID" ) ;
		if ( $q == 0 and !$allow_q_zero ) return $this->logError ( "Trying to set q to zero" ) ;

		// Get existing
		$entry = $this->getEntryObjectFromID ( $entry_id ) ;
		if ( !isset($entry) ) return $this->logError ( "Entry #{$entry_id} not found." ) ;

		if ( $this->isAutoMatchUser ( $user_id ) and $this->avoidEntryAutomatch ( $entry_id , $q ) ) return $this->logError ( "Entry #{$entry_id} was removed before." ) ;

		return $this->setMatchForEntryObject ( $entry , $q , $user_id , $no_overwrite_manual ) ;
	}

	# Returns true if match was stored in database, false otherwise
	public function setMatchForCatalogExtID ( $catalog , $ext_id , $q , $user_id , $no_overwrite_manual = false , $allow_q_zero = true ) {

		$catalog *= 1 ;
		$ext_id = $this->escape ( $ext_id ) ;
		$this->sanitizeQ ( $q ) ;
		$user_id *= 1 ;

		if ( $q == 0 and !$allow_q_zero ) return $this->logError ( "Trying to set q to zero" ) ;

		// Get existing
		$entry = '' ;
		$sql = "SELECT * FROM entry WHERE catalog={$catalog} AND ext_id='{$ext_id}' LIMIT 1" ; # By table constraints, there can only be one
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $entry = $o ;
		if ( $entry == '' ) return $this->logError ( "External ID '{$ext_id}' in catalog #{$catalog} not found." ) ;

		$this->last_entry = $entry ;

		return $this->setMatchForEntryObject ( $entry , $q , $user_id , $no_overwrite_manual ) ;
	}

	# Returns true if match was stored in database, false otherwise
	public function removeMatchForEntryID ( $entry_id ,  $user_id ) {
		$entry_id *= 1 ;
		$user_id *= 1 ;
		if ( $entry_id == 0 ) return $this->logError ( "Bad entry ID" ) ;

		$entry = $this->getEntryObjectFromID ( $entry_id ) ;
		$removed_q = $entry->q ;
		if ( !isset($entry) ) return $this->logError ( "Entry #{$entry_id} not found." ) ;
		if ( !$this->setMatchForEntryObject ( $entry , null , null ) ) return false ;
		if ( !$this->addUserLog ( 'remove_q' , $entry_id , $user_id , $removed_q ) ) return false ;
		return true ;
	}

	public function getCurrentTimestamp () {
		return date ( 'YmdHis' ) ;
	}

	# Returns true if match was stored in database, false otherwise
	public function addUserLog ( $action , $entry_id , $user_id , $q = -1 ) {
		$entry_id *= 1 ;
		$user_id *= 1 ;
		if ( $entry_id == 0 ) return $this->logError ( "Bad entry ID" ) ;

		if ( !isset($q) or $q < 1 ) $q = 'null' ;
		else $q = preg_replace ( '/\D/' , '' , "$q" ) ;

		$ts = $this->getCurrentTimestamp() ;
		$sql = "INSERT INTO log (`action`,`entry`,`user`,`timestamp`,`q`) VALUES ('".$this->escape($action)."',{$entry_id},{$user_id},'$ts',{$q})" ;
		$result = $this->getSQL ( $sql ) ;
		return true ;
	}

	# Returns true if match was stored in database, false otherwise
	# Private function, passed paramaters expected to be sanitized
	private function setMatchForEntryObject ( $entry , $q , $user_id , $no_overwrite_manual = false ) {
		$ts = date ( 'YmdHis' ) ;
		$this->sanitizeQ ( $q ) ;

		# Paranoia
		if ( $user_id === null and $q !== null ) return $this->logError ( "#{$entry->id}: Q is $q but user is null" ) ;
		if ( $user_id == 0 and $entry->user == 0 and $q == $entry->q ) return true ; # Auto-match replacing same auto-match, no need to update

		# Prepare update overview
		$add_column = '' ;
		if ( $user_id == 0 ) $add_column = 'autoq' ;
		else if ( $q === null ) $add_column = 'noq' ;
		else if ( $q == 0 ) $add_column = 'na' ;
		else if ( $q == -1 ) $add_column = 'nowd' ;
		else if ( $user_id > 0 ) $add_column = 'manual' ;

		$reduce_column = '' ;
		if ( (!isset($entry->q) or $entry->q === null) and (isset($q) and $q !== null) ) $reduce_column = 'noq' ;
		else if ( $entry->q == 0 ) $reduce_column = 'na' ;
		else if ( $entry->q == -1 ) $reduce_column = 'nowd' ;
		else if ( $entry->user == 0 ) $reduce_column = 'autoq' ;

		# Set match
		$q_sql = ($q===null)?'null':$q ;
		$user_sql = ($user_id===null)?'null':$user_id ;
		$ts_sql = ($q === null and $user_id === null) ?'null':"'$ts'" ;
		$sql = "UPDATE entry SET q={$q_sql},user={$user_sql},`timestamp`={$ts_sql} WHERE id={$entry->id}" ;
		if ( $no_overwrite_manual ) $sql .= " AND (user is null or user=0 or q=-1)" ;
		$this->getSQL ( $sql ) ;
		if ( $this->dbm->affected_rows == 0 ) return $this->logError ( "No changes written." ) ;

		# Clean up
		if ( $user_id !== 0 ) {
			$sql = "DELETE FROM multi_match WHERE entry_id={$entry->id}" ;
			$this->getSQL ( $sql ) ;

			$sql = "DELETE FROM potential_mismatch WHERE entry_id={$entry->id}" ;
			if ( $q != null ) $sql .= " AND q!={$q}" ;
			$this->getSQL ( $sql ) ;
		}

		# Update overview
		if ( $add_column == '' and $reduce_column == '' ) return true ; # No overview update, but match still took place
		$sql = "UPDATE overview SET " ;
		if ( $add_column != '' ) $sql .= " {$add_column}={$add_column}+1" ;
		if ( $add_column != '' and $reduce_column != '' ) $sql .= "," ;
		if ( $reduce_column != '' ) $sql .= " {$reduce_column}={$reduce_column}-1" ;
		$sql .= " WHERE catalog={$entry->catalog}" ;
		$this->getSQL ( $sql ) ;
		$this->updateOverviewFile() ;
		return true ;
	}

	public function updateOverviewFile () {
		$out = array ( 'status' => 'OK' , 'data' => array() ) ;
		$this->generateOverview ( $out ) ;
		$s = json_encode ( $out ) ;
		file_put_contents ( $this->overview_filename , $s ) ;
	}

	public function updateCatalogs ( $catalogs ) {
		foreach ( $catalogs AS $catalog ) $this->updateSingleCatalog ( $catalog , false ) ;
		$this->updateOverviewFile() ;
	}

	public function updateSingleCatalog ( $catalog , $update_overview_file = true ) {
		$catalog *= 1 ; # Paranoia
		$sql = "INSERT IGNORE INTO overview (catalog) VALUES ({$catalog})" ;
		$this->getSQL ( $sql ) ;

		$sql = "SELECT IFNULL(count(*),0) AS total,IFNULL(sum(q is null),0) AS q_null,IFNULL(sum(q=0),0) AS q_zero,IFNULL(sum(q=-1),0) AS q_minus_one,IFNULL(sum(user=0),0) AS u_zero,IFNULL(sum(q>0 and user>0),0) AS manual FROM entry where catalog={$catalog}" ;
		$result = $this->getSQL ( $sql ) ;
		$o = $result->fetch_object() ;

		$sql = "UPDATE overview SET " ;
		$sql .= "total = {$o->total}," ;
		$sql .= "noq = {$o->q_null}," ;
		$sql .= "autoq = {$o->u_zero}," ;
		$sql .= "na = {$o->q_zero}," ;
		$sql .= "manual = {$o->manual}," ;
		$sql .= "nowd = {$o->q_minus_one}," ;
		$sql .= "multi_match = (SELECT count(*) FROM multi_match WHERE multi_match.catalog={$catalog})," ;
		$sql .= "types = (SELECT group_concat(DISTINCT `type` separator '|') FROM entry where catalog={$catalog})" ;
		$sql .= " WHERE catalog={$catalog}" ;
		$this->getSQL ( $sql ) ;
		if ( $update_overview_file ) $this->updateOverviewFile() ;
	}

	public function activateCatalog ( $catalog , $mode = 1 ) {
		$catalog *= 1 ;
		$mode *= 1 ;
		$sql = "UPDATE catalog SET `active`={$mode} WHERE id={$catalog}" ;
		$this->getSQL ( $sql ) ;
		$this->updateOverviewFile() ;
	}

	public function deactivateCatalog ( $catalog ) {
		$this->activateCatalog ( $catalog , 0 ) ;
	}

	public function generateOverview ( &$out ) {
		$sql = "SELECT overview.* FROM overview,catalog WHERE catalog.id=overview.catalog and catalog.active>=1" ; // overview is "manually" updated, but fast; vw_overview is automatic, but slow
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			foreach ( $o AS $k => $v ) $out['data'][$o->catalog][$k] = $v ;
		}

		$sql = "SELECT * FROM catalog WHERE active>=1" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			foreach ( $o AS $k => $v ) $out['data'][$o->id][$k] = $v ;
		}
/*
		# Large catalogs
		$dbl = $this->tfc->openDBtool ( 'mixnmatch_large_catalogs_p' ) ;
		$sql = "SELECT * FROM catalog" ;
		$result = $this->tfc->getSQL ( $dbl , $sql ) ;
		while($o = $result->fetch_object()){
			$id = 'L' . $o->id ;
			$out['data'][$id] = [
				'catalog' => $id ,
				'name' => $o->name ,
				'total' => $o->entries ,
				'url' => $o->url ,
				'search_wp' => $o->language ,
				'wd_prop' => $o->property ,
				'wd_qual' => null ,
				'desc' => '' ,
				'type' => 'Large catalog' ,
				'owner' => 2 ,
				'source_item' => $o->subject ,
				'active' => 1
			] ;
		}
*/

		$sql = "SELECT user.name AS username,catalog.id from catalog,user where owner=user.id AND active>=1" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			foreach ( $o AS $k => $v ) $out['data'][$o->id][$k] = $v ;
		}
		
		$sql = "select catalog.id AS id,last_update from catalog,autoscrape WHERE active>=1 AND catalog.id=autoscrape.catalog and do_auto_update=1" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$out['data'][$o->id]['scrape_update'] = 1 ;
			$lu = $o->last_update ;
			$lu = substr($lu,0,4).'-'.substr($lu,4,2).'-'.substr($lu,6,2).' '.substr($lu,8,2).':'.substr($lu,10,2).':'.substr($lu,12,2) ;
			$out['data'][$o->id]['last_scrape'] = $lu ;
		}
	}

	public function isGNDundifferentiatedPerson ( $id ) {
		if ( is_array($id) ) {
			if ( count($id) == 1 ) return $this->isGNDundifferentiatedPerson ( $id[0] ) ;
			$this->logError ( "Mixnmatch::isGNDundifferentiatedPerson." . json_encode($id) ) ;
			return true ; // ????
		}
		$url = "http://d-nb.info/gnd/{$id}/about/lds" ;
		$rdf = file_get_contents ( $url ) ;
		return preg_match ( '/gndo:UndifferentiatedPerson/' , $rdf ) ;
	}

	public function hasPropertyEverEditedInItem ( $q , $prop ) {
		$this->wil->sanitizeQ ( $q ) ;
		$this->wil->sanitizeQ ( $prop ) ;
		$sql = "SELECT count(*) AS cnt from page,revision WHERE page_title='$q' AND page_namespace=0 AND page_id=rev_page AND rev_comment LIKE '%Property:{$prop}%' LIMIT 1" ;
		$this->openWikidataDB() ;
		$result = getSQL ( $this->dbwd , $sql ) ;
		if($o = $result->fetch_object()) return true ;
		return false ;
	}

	public function openWikidataDB () {
		if ( !isset($this->dbwd) ) $this->dbwd = $this->tfc->openDB ( 'wikidata' , 'wikidata' ) ;
		return $this->dbwd ;
	}


	public function loadCatalog ( $catalog ) {
		$catalog *= 1 ;
		if ( $catalog == 0 ) return ;
		if ( isset($this->catalog[$catalog]) ) return $this->catalogs[$catalog] ;
		$sql = "SELECT * FROM catalog WHERE id={$catalog}" ;
		$result = $this->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$this->catalogs[$catalog] = $o ;
		}
		return $this->catalogs[$catalog] ;
	}

	# Large catalogs value fix
	public function fixPropertyValueFromLCtoWikidata ( $prop , $value ) {
		if ( $prop == 'P1368' ) $value = 'LNC10-' . $value ;
		if ( $prop == 'P1207' ) $value = preg_replace ( '/\s/' , '' , $value ) ;
		if ( $prop == 'P244' ) $value = preg_replace ( '/\s/' , '' , $value ) ;
		if ( $prop == 'P213' and strlen($value) == 16 )  $value = substr($value,0,4).' '.substr($value,4,4).' '.substr($value,8,4).' '.substr($value,12,4) ;
		return $value ;
	}


	public function date2expression ( $d ) {
		if ( preg_match ( '/^\d+-\d{2}-\d{2}$/' , $d ) ) return "+{$d}T00:00:00Z/11" ;
		if ( preg_match ( '/^\d+-\d{2}$/' , $d ) ) return "+{$d}-01T00:00:00Z/10" ;
		if ( preg_match ( '/^\d+$/' , $d ) ) return "+{$d}-01-01T00:00:00Z/9" ;
		$this->logError ( "Bad date: {$d}" ) ;
	}

	public function fixStringForQS ( $s ) {
		if ( strlen($s) > 250 ) $s = substr ( $s , 0 , 250 ) ;
		return $s ;
	}

	public function getCreateItemForEntryCommands ( $entry , $lc = '' ) {
		$commands = [] ;

		$this->loadCatalog ( $entry->catalog ) ;

		$source = '' ;
		if ( $entry->ext_url != '' ) {
			$source = "\tS854\t\"{$entry->ext_url}\"" ;
			$source .= "\tS813\t" . $this->date2expression(date('Y-m-d')) ;
		}

		# P31
		if ( preg_match ( '/^Q\d+$/' , $entry->type ) ) $commands[] = "LAST\tP31\t{$entry->type}" ;

		# Label
		$lang = $this->catalogs[$entry->catalog]->search_wp ;
		$commands[] = "LAST\tL{$lang}\t\"" . $this->fixStringForQS($entry->ext_name) . '"' ;
		if ( $entry->type == 'Q5' and in_array ( $lang , $this->similar_languages ) ) {
			foreach ( $this->similar_languages AS $l ) {
				if ( $l != $lang ) $commands[] = "LAST\tL{$l}\t\"" . $this->fixStringForQS($entry->ext_name) . '"' ;
			}
		}

		# Description
		if ( $entry->ext_desc != '' ) {
			$commands[] = "LAST\tD{$lang}\t\"" . $this->fixStringForQS($entry->ext_desc) . '"' ;
		}

		# Location
		$sql = "SELECT * FROM location WHERE entry={$entry->id}" ;
		$result = $this->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$commands[] = "LAST\tP625\t@{$o->lat}/{$o->lon}" ;
		}

		# Dates
		if ( $entry->type == 'Q5' ) {
			$sql = "SELECT * FROM person_dates WHERE entry_id={$entry->id}" ;
			$result = $this->getSQL ( $sql ) ;
			while ( $o = $result->fetch_object() ) {
				if ( $o->born != '' ) {
					$de = $this->date2expression($o->born) ;
					if ( isset($de) ) $commands[] = "LAST\tP569\t{$de}{$source}" ;
				}
				if ( $o->died != '' ) {
					$de = $this->date2expression($o->died) ;
					if ( isset($de) ) $commands[] = "LAST\tP570\t{$de}{$source}" ;
				}
			}
		}

		# external IDs
		$external_ids = [] ;
		if ( isset($this->catalogs[$entry->catalog]->wd_prop) and !isset($this->catalogs[$entry->catalog]->wd_qual) ) {
			$prop = 'P' . $this->catalogs[$entry->catalog]->wd_prop ;
			$external_ids[$prop][$entry->ext_id] = [$entry->ext_id,$source] ;
		}

		# Aux
		$conditions = [] ;
		$sql = "SELECT * FROM auxiliary WHERE entry_id={$entry->id}" ;
		$result = $this->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$prop = 'P' . $o->aux_p ;
			$external_ids[$prop][$o->aux_name] = [$o->aux_name,$source] ;
			if ( !preg_match ( '/^Q\d+$/' , $o->aux_name ) ) { # String value HACKISH FIXME
				$value = $this->fixPropertyValueFromLCtoWikidata ( $prop , $o->aux_name ) ;
				if ( $lc != '' ) {
					if ( $prop == $lc->getMainProp() ) $conditions[] = "ext_id='".$this->escape($o->aux_name)."'" ;
					foreach ( $lc->prop2field[$lc->getCatalogID()] AS $p => $col ) {
						if ( $p != $prop ) continue ;
						$conditions[] = "$col='".$this->escape($o->aux_name)."'" ;
					}
				}
			}
		}

		# From large catalog
		if ( count($conditions) > 0 ) {
			$table = $lc->getCat()->table ;
			$conditions = array_unique ( $conditions ) ;
			$sql = "SELECT * FROM `$table` WHERE ((" . implode ( ") OR (" , $conditions ) . "))" ;
			$result = $this->tfc->getSQL ( $lc->db , $sql ) ;
			while ( $o = $result->fetch_object() ) {
				$prop = $lc->getMainProp() ;
				$value = $this->fixPropertyValueFromLCtoWikidata ( $prop , $o->ext_id ) ;
				$external_ids[$prop][$value] = [$value,$source] ;
				foreach ( $lc->prop2field[$lc->getCatalogID()] AS $prop => $col ) {
					$value = trim ( $o->$col ) ;
					if ( $value == '' ) continue ;
					$value = $this->fixPropertyValueFromLCtoWikidata ( $prop , $value ) ;
					$external_ids[$prop][$value] = [$value] ;
				}
			}
		}

		if ( isset($external_ids['P227']) ) {
			foreach ( $external_ids['P227'] AS $k => $gnd ) {
				if ( $this->isGNDundifferentiatedPerson($gnd) ) unset ( $external_ids['P227'][$k] ) ;
			}
			if ( count($external_ids['P227']) == 0 ) unset ( $external_ids['P227'] ) ;
		}

		# Check external IDs via SPARQL, convert to commands
		$this->wil->loadItems ( array_keys($external_ids) ) ;
		$sparql_conditions = [] ;
		foreach ( $external_ids AS $prop => $values ) { # $prop is Pxxx
			$i = $this->wil->getItem ( $prop ) ;
			if ( !isset($i) ) continue ; // Property does not exist on Wikidata??
			foreach ( $values AS $dummy => $v ) {
				$value = trim($v[0]) ;
				if ( $value == '' ) continue ;
				$s = isset($v[1]) ? $v[1] : '' ;
				$is_string_value = !($i->j->datatype=='wikibase-item') ;
				if ( $i->j->datatype == 'external-id' ) $sparql_conditions[] = "?q wdt:{$prop} '{$value}'" ;
				if ( $prop == 'P625' ) $commands[] = "LAST\t{$prop}\t@{$value}{$s}" ;
				else if ( $is_string_value ) $commands[] = "LAST\t{$prop}\t\"{$value}\"{$s}" ;
				else $commands[] = "LAST\t{$prop}\t{$value}{$s}" ;
			}
		}
		if ( count($sparql_conditions) > 0 ) {
			$sparql = "SELECT DISTINCT ?q { { " . implode ( " } UNION { " , $sparql_conditions ) . " } }" ;
			$items = $this->tfc->getSPARQLitems ( $sparql , 'q' ) ;
			if ( count($items) > 0 ) {
				$this->logError ( "https://tools.wmflabs.org/mix-n-match/#/entry/{$entry->id} might already exist as " . json_encode($items) ) ;
				return ;
			}
		}

		$commands = array_unique ( $commands ) ;
		array_unshift ( $commands , 'CREATE' ) ;
		return $commands ;
	}

} ;

?>