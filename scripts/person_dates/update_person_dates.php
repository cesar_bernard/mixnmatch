#!/usr/bin/php
<?PHP

require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ;
require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$month_list = 'january|jan|february|feb|march|mar|april|apr|may|june|jun|july|jul|august|aug|september|sep|october|oct|november|nov|december|dec' ;

$month_in_other_languages = [
	'januari' => 'jan' ,
	'februari' => 'feb' ,
	'maart' => 'mar' ,
	'april' => 'apr' ,
	'mei' => 'may' ,
	'juni' => 'jun' ,
	'juli' => 'jul' ,
	'augustus' => 'aug' ,
	'september' => 'sep' ,
	'oktober' => 'oct' ,
	'november' => 'nov' ,
	'december' => 'dec' ,

	'janvier' => 'jan' ,
	'février' => 'feb' ,
	'mars' => 'mar' ,
	'avril' => 'apr' ,
	'mai' => 'may' ,
	'juin' => 'jun' ,
	'juillet' => 'jul' ,
	'août' => 'aug' ,
	'septembre' => 'sep' ,
	'octobre' => 'oct' ,
	'novembre' => 'nov' ,
	'décembre' => 'dec' ,

	'jan' => 'jan' ,
	'febr' => 'feb' ,
	'márc' => 'mar' ,
	'ápr' => 'apr' ,
	'máj' => 'may' ,
	'jún' => 'jun' ,
	'júl' => 'jul' ,
	'aug' => 'aug' ,
	'szept' => 'sep' ,
	'okt' => 'oct' ,
	'nov' => 'nov' ,
	'dec' => 'dec' ,

	'enero' => 'jan' ,
	'febrero' => 'feb' ,
	'marzo' => 'mar' ,
	'abril' => 'apr' ,
	'mayo' => 'may' ,
	'junio' => 'jun' ,
	'julio' => 'jul' ,
	'agosto' => 'aug' ,
	'septiembre' => 'sep' ,
	'octubre' => 'oct' ,
	'noviembre' => 'nov' ,
	'diciembre' => 'dec' ,
	
	'mrz' => 'mar' ,
	'mai' => 'may' ,
	'maj' => 'may' ,
	'juni' => 'jun' ,
	'juli' => 'jul' ,
	'augusti' => 'aug' ,
	'oktober' => 'oct' ,
	'okt' => 'oct' ,
	'dez' => 'dec' ,

	'desember' => 'dec' ,

	'genn' => 'jan' ,
	'febbr' => 'feb' ,
	'febbraio' => 'feb' ,
	'marzo' => 'mar' ,
	'aprile' => 'apr' ,
	'maggio' => 'may' ,
	'giugno' => 'jun' ,
	'luglio' => 'jul' ,
	'ag' => 'aug' ,
	'sett' => 'sep' ,
	'settembre' => 'sep' ,
	'ott' => 'oct' ,
	'nov' => 'nov' ,
	'dic' => 'dec' ,
] ;

function ml ( $month ) {
	global $month_in_other_languages ;
	$month = strtolower(trim($month)) ;
	if ( isset($month_in_other_languages[$month]) ) return $month_in_other_languages[$month] ;
	return $month ;
}

function dp ( $d ) {
	if ( preg_match ( '/^\d+$/' , $d ) ) return $d ;
	if ( preg_match ( '/^(\d{1,2})\.\s*(\d{1,2})\.\s*(\d{3,4})$/' , $d , $m ) ) {
		$d = str_pad($m[3],4,'0',STR_PAD_LEFT) . '-' . str_pad($m[2],2,'0',STR_PAD_LEFT) . '-' . str_pad($m[1],2,'0',STR_PAD_LEFT) ;
	} else 	if ( preg_match ( '/^(\d{1,2})\.\s*(\d{3,4})$/' , $d , $m ) ) {
		$d = str_pad($m[2],4,'0',STR_PAD_LEFT) . '-' . str_pad($m[1],2,'0',STR_PAD_LEFT) ;
	}
	$d = date_parse ( $d ) ;
	while ( strlen($d['year']) < 4 ) $d['year'] = '0' . $d['year'] ;
	while ( strlen($d['month']) < 2 ) $d['month'] = '0' . $d['month'] ;
	while ( strlen($d['day']) < 2 ) $d['day'] = '0' . $d['day'] ;
	return $d['year'] . '-' . $d['month'] . '-' . $d['day'] ;
}

function fix_date_format ( &$d ) {
	$d = trim ( $d ) ;
	if ( $d == '' ) return ;
	$d = preg_replace ( '/^0+/' , '' , $d ) ; // Leading zeros
	if ( preg_match ( '/^(\d{3,4})-(\d)-(\d)$/' , $d , $m ) ) $d = $m[1].'-0'.$m[2].'-0'.$m[3] ;
	else if ( preg_match ( '/^(\d{3,4})-(\d\d)-(\d)$/' , $d , $m ) ) $d = $m[1].'-'.$m[2].'-0'.$m[3] ;
	else if ( preg_match ( '/^(\d{3,4})-(\d)-(\d\d)$/' , $d , $m ) ) $d = $m[1].'-0'.$m[2].'-'.$m[3] ;
	while ( preg_match ( '/^\d{1,3}-/' , $d ) ) $d = "0$d" ;
	while ( preg_match ( '/^\d{1,3}$/' , $d ) ) $d = "0$d" ;
	while ( preg_match ( '/^(.+)-00$/' , $d  , $m ) ) $d = $m[1] ;
}


$mnm = new MixNMatch ;

if ( !isset($argv[1]) ) die ( "Requires catalog number\n" ) ;
$catalog = $argv[1] * 1 ;
if ( $catalog == 323 ) die ( "This catalog has better dates on the website, which are already in the born/died columns\n" ) ;
if ( $catalog == 1640) die ( "This catalog has dates imported from large_catalogs.gnd\n" ) ;

$sql = "DELETE person_dates FROM person_dates INNER JOIN entry ON entry_id=entry.id WHERE catalog={$catalog}" ;
$mnm->getSQL ( $sql ) ;

$values = [] ;
$sql = "SELECT id,ext_desc FROM entry WHERE catalog=$catalog AND ext_desc!=''" ;
$sql .= " AND `type`='Q5'" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	
	$born = '' ;
	$died = '' ;
	
	switch ( $catalog ) {
		case 2:
			if ( preg_match ( '/^(\d{3,4})[\-\–](\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bb\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bd\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 13:
			if ( preg_match ( '/; (\d{4}[0-9-]*);[^;]*;{0,1} (\d{4}[0-9-]*)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			if ( preg_match ( '/^\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			if ( $born == '' and preg_match ( '/\bborn ([0-9-]+)/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( $born == '' and preg_match ( '/\bborn on ([0-9-]+)/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( $died == '' and preg_match ( '/\bdied ([0-9-]+)/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $died == '' and preg_match ( '/\bdied on ([0-9-]+)/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( preg_match ( '/^(\d{3,4}) - (\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ; # Newer entries from scrape

			if ( preg_match ( '/(\d{3,4}-\d{1,2}-\d{1,2});.+?(\d{3,4}-\d{1,2}-\d{1,2});/' , $o->ext_desc , $m ) ) {
				$b = dp($m[1]) ;
				$d = dp($m[2]) ;
				if ( $b*1 < $d*1 ) list($born,$died) = [ $b , $d ] ;
			}
			break ;
		
		case 15:
			if ( preg_match ( '/^\((\d{1,2}) ([a-z]+) (\d{3,4})\s*-\s*(\d{1,2}) ([a-z]+) (\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1].' '.ml($m[2]).' '.$m[3]) , dp($m[4].' '.ml($m[5]).' '.$m[6]) ] ;
			else if ( preg_match ( '/^\((\d{3,4})\s*-\s*(\d{1,2}) ([a-z]+) (\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[1] , dp($m[2].' '.ml($m[3]).' '.$m[4]) ] ;
			else if ( preg_match ( '/^\((\d{3,4})\s*-\s*(\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[1] , $m[2] ] ;
			else if ( preg_match ( '/^\([^)]*?(\d{3,4})[^)]+?(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[1] , $m[2] ] ;
			if ( strlen($born)<5 and preg_match ( '/^\((\d{1,2}) ([a-z]+) (\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = dp($m[1].' '.ml($m[2]).' '.$m[3]) ;
			break ;
		
		case 16:
			if ( preg_match ( '/^(\d{3,4})–(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\((\d{3,4})[\–\-](\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 17:
			if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/\d-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $born == '' ) $died = '' ; # Many wrong (death) dates
			if ( preg_match ( '/01$/' , $born ) and preg_match ( '/00$/' , $died ) ) { $born='' ; $died='' ; } ; # Many estimated dates
			break ;

		case 18:
			if ( preg_match ( '/^(\d{1,2}) de (\S+) de (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . ' ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1];
			if ( preg_match ( '/; (\d{1,2}) de (\S+) de (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . ' ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/; (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1];
			if ( $born == '1900' and $died == '1900' ) { $born = '' ; $died = '' ; }
			break ;
		
		case 20:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\s*d\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 21:
			if ( preg_match ( '/^\s*\((\d{3,4})\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			$age = $died - $born ;
			if ( $age == 98 ) { $born = '' ; $died = '' ; }
#			print "$born / $died / $age\n" ;
		break ;
		
		case 22:
			if ( preg_match ( '/^(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[3].'-'.$m[1].'-'.$m[2] ) ;
			else if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( $died == '' and preg_match ( '/^[0-9\/]+?\s*-\s*(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[3].'-'.$m[1].'-'.$m[2] ) ;
			if ( $died == '' and preg_match ( '/^[0-9\/]+?\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 25:
			if ( preg_match ( '/^\((\d{3,4}) *- *(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 27:
			if ( !preg_match ( '/(active|ca\.|before|after)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
				else if ( preg_match ( '/\bborn (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/\bdied (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;
		
		case 28:
			if ( preg_match ( '/^\((\d{3,4})-(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\((\d{3,4})\)$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;
		
		case 32:
			if ( preg_match ( '/;\s*(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 35:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 39:
			if ( preg_match ( '/^[^-]*?(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-.*?(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 46:
			if ( preg_match ( '/^(\d{3,4}-\d{1,2}-\d{1,2})\s*-\s*(\d{3,4}-\d{1,2}-\d{1,2})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/born (\d{3,4}-\d{1,2}-\d{1,2})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/died (\d{3,4}-\d{1,2}-\d{1,2})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 47:
			if ( preg_match ( '/^\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 52:
			if ( preg_match ( '/\b(active|about)\b/' , $o->ext_desc ) ) {} // Skip
			else if ( preg_match ( '/(\d{3,4})–(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\bb\. (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 53:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 54:
			if ( preg_match ( '/^\s*\(*(\d{3,4})\s*–\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 55:
			$parts = explode ( ';' , $o->ext_desc ) ;
			if ( count($parts) == 2 ) {
				if ( preg_match ( '/(\d{1,2}) (\S+?)\.{0,1} (\d{3,4})/' , $parts[0] , $m ) ) $born = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
				else if ( preg_match ( '/\b(nel|il) (\d{3,4})/' , $parts[0] , $m ) ) $born = dp ( $m[2] ) ;
				if ( preg_match ( '/(\d{1,2}) (\S+?)\.{0,1} (\d{3,4})/' , $parts[1] , $m ) ) $died = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
				else if ( preg_match ( '/\b(nel|il) (\d{3,4})/' , $parts[1] , $m ) ) $died = dp ( $m[2] ) ;
			}
			break ;

		case 58:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\s*(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 59:
			$o->ext_desc = preg_replace ( '/&#\d+;/' , ' ' , $o->ext_desc ) ;
			$o->ext_desc = preg_replace ( '/;.*$/' , '' , $o->ext_desc ) ;
			
			if ( preg_match ( '/(\d{3,4})\. (\S{3,5})\. (\d{1,2})\..*?-/' , $o->ext_desc , $m ) ) $born = dp ( $m[3].' '.ml($m[2]).' '.$m[1] ) ;
			else if ( preg_match ( '/(\d{3,4}).*?-/' , $o->ext_desc , $m ) ) $born = $m[1] ;

			if ( preg_match ( '/-.*?(\d{3,4})\. (\S{3,5})\. (\d{1,2})\./' , $o->ext_desc , $m ) ) $died = dp ( $m[3].' '.ml($m[2]).' '.$m[1] ) ;
			else if ( preg_match ( '/-.*?(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 60:
			if ( preg_match ( '/\((\d{3,4})\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\((\d{3,4})\s*\)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\(\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 61:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 63:
			if ( preg_match ( '/^\((\d{3,4})–(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 70:
			if ( preg_match ( '/^(\d{3,4}) ('.$month_list.') (\d{1,2})\s*-/i' , $o->ext_desc , $m ) ) $born = dp ( $m[3].' '.$m[2].' '.$m[1] ) ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^b\.\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/^\d*-(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/^d\.\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 71:
			if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( preg_match ( '/-(\d{3,4});/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 73:
			if ( preg_match ( '/^(\d{1,2})\.(\d{1,2})\.(\d{3,4}) [^,]+, (\d{1,2})\.(\d{1,2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[3].'-'.$m[2].'-'.$m[1] , $m[6].'-'.$m[5].'-'.$m[4] ] ;
			else if ( preg_match ( '/^(\d{1,2})\.(\d{1,2})\.(\d{3,4}) /' , $o->ext_desc , $m ) ) $born = $m[3].'-'.$m[2].'-'.$m[1] ;
			else if ( preg_match ( '/^(\d{3,4}) /' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;
		
		case 80:
			if ( preg_match ( '/\bborn ([0-9-]+)/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied ([0-9-]+)/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 83:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/, (\d{3,4})-(\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else {
				$o->ext_desc = preg_replace ( '/\bSt\. /' , 'St ' , $o->ext_desc ) ;
				if ( preg_match ( '/\bborn[^0-9.]* (\S+) (\d{1,2}), (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . ' ' . ml($m[1]) . ' ' . $m[3] ) ;
				else if ( preg_match ( '/\bborn[^0-9.]* (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;

				if ( preg_match ( '/\bdied[^0-9.]* (\S+) (\d{1,2}), (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . ' ' . ml($m[1]) . ' ' . $m[3] ) ;
				else if ( preg_match ( '/\bdied[^0-9.]* (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;

		case 84:
			if ( preg_match ( '/^(\S+) (\d{1,2}), (\d{3,4}) -/i' , $o->ext_desc , $m ) ) $born = dp ( $m[2].' '.$m[1].' '.$m[3] ) ;
			else if ( preg_match ( '/^(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\S+) (\d{1,2}), (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[2].' '.$m[1].' '.$m[3] ) ;
			else if ( preg_match ( '/- (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 85:
			if ( preg_match ( '/(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 86:
			if ( preg_match ( '/birth date:\s*(\d{3,4}-\d{1,2}-\d{1,2})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			if ( preg_match ( '/death date:\s*(\d{3,4}-\d{1,2}-\d{1,2})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			break ;
		
		case 89:
			if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})\s*/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 91:
			if ( preg_match ( '/Born:([^;]+?)([0-9-]+)\s*;/' , $o->ext_desc , $m ) ) {
				if ( !preg_match ( '/(\bfør\b|\befter\b)/' , $m[1] ) and !preg_match ( '/\d{3,4}-\d{3,4}/' , $m[2] ) ) $born = dp ( $m[2] ) ;
			}
			if ( preg_match ( '/Died:([^;]+?)([0-9-]+)\s*;/' , $o->ext_desc , $m ) ) {
				if ( !preg_match ( '/(\bfør\b|\befter\b)/' , $m[1] ) and !preg_match ( '/\d{3,4}-\d{3,4}/' , $m[2] ) )  $died = dp ( $m[2] ) ;
			}
			break ;
		
		case 95:
			if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 98:
			if ( preg_match ( '/^born\s*(\d{3,4});/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 101:
			if ( preg_match ( '/(\d{3,4})–(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			if ( preg_match ( '/born (\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 103:
			if ( preg_match ( '/\*\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] );
			else if ( preg_match ( '/\*\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1];
			if ( preg_match ( '/†\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] );
			else if ( preg_match ( '/†\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1];
			break ;
		
		case 107:
			if ( preg_match ( '/born in (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died in (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 110:
			if ( preg_match ( '/, (\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/died (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 113:
			if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 114:
			if ( preg_match ( '/^\s*(\d{3,4})\s*(\-|in |at |,)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) and !preg_match ( '/-\s*(\d{3,4})\s*(s|c\.|fl\.)/' , $o->ext_desc ) ) $died = $m[1] ;
			break ;
		
		case 115:
			if ( preg_match ( '/^b\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bd\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 118:
#			if ( !preg_match ( '/(actif)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/\((\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/-\s*(\d{3,4})[\),]/' , $o->ext_desc , $m ) ) $died = $m[1] ;
#			}
			break ;
		
		case 121:
			if ( preg_match ( '/^\*\s*(\d{1,2})\.(\d{1,2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[3].'-'.$m[2].'-'.$m[1] ) ;
			else if ( preg_match ( '/\*\s*(\d{1,2})\.(\d{1,2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[3].'-'.$m[2].'-'.$m[1] ) ;
			if ( preg_match ( '/\&\#134;\s*(\d{1,2})\.(\d{1,2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[3].'-'.$m[2].'-'.$m[1] ) ;
			else if ( preg_match ( '/\&\#134;\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $born == '' and preg_match ( '/^\s*\*\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 130:
			if ( preg_match ( '/^(\d{3,4})\s*[--]\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			if ( $born == $died ) continue ;
			break ;

		case 136:
			if ( preg_match ( '/(\d{1,2}) ([a-zéû]+) (\d{3,4})\s*-/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
			else if ( preg_match ( '/^(\d{1,2}) ([a-zéû]+) (\d{3,4})$/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
			else if ( preg_match ( '/\b(\d{3,4})\s*-/i' , $o->ext_desc , $m ) ) $born = $m[1] ;

			if ( preg_match ( '/-\s*(\d{1,2}) ([a-zéû]+) (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
			else if ( preg_match ( '/-\s*(\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 139:
			if ( preg_match ( '/\((\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 143:
			if ( !preg_match ( '/ c\./' , $o->ext_desc ) ) {
				if ( preg_match ( '/born (\d+ [a-z]+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
				else if ( preg_match ( '/born ([a-z]+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = substr ( dp ( '1 '.$m[1] ) , 0 , 7 ) ;
				else if ( preg_match ( '/born (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/died (\d+ [a-z]+ \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
				else if ( preg_match ( '/died ([a-z]+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = substr ( dp ( '1 '.$m[1] ) , 0 , 7 ) ;
				else if ( preg_match ( '/died (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;
		
		case 149:
			if ( preg_match ( '/\(b\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/d\.\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 150:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\s*birth\/death:\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 152:
			if ( preg_match ( '/, (\d{3,4}) - (\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/, (\d{3,4}) - \S+?, (\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 156:
			if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 158:
			if ( !preg_match ( '/\bca\./' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			}
			break ;

		case 163:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\((\d{3,4})\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 165:
			if ( preg_match ( '/[♀♂]{0,1}\s*\((.*?) – (.*?)\)/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/[♀♂]{0,1}\s*\((.*?)–\)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/[♀♂]{0,1}\s*\(–(.*?)\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 168:
			if ( preg_match ( '/born (\d{3,4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/born (\d{3,4}),/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/born (\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/dead (\d{3,4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/dead (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 169:
			if ( preg_match ( '/born (\S+?)\.{0,1} (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . ' ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/born (\d{3,4}) /' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\S+?)\.{0,1} (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . ' ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/died (\d{3,4}) /' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 171:
			if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 175:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 177:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 179:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-$/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;

		case 185:
			$o->ext_desc = str_replace ( '&ndash;' , '-' , $o->ext_desc ) ;
			if ( preg_match ( '/^(\d{3,4})\s*[–-]/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/[–-]\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;
		
		case 191:
			if ( !preg_match ( '/(fl\.|active)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/^(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/^(\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/- (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;
		
		case 203:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-$/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;

		case 206:
			if ( preg_match ( '/\((\d{1,2}) (\S+?) (\d{3,4})\s*[\)–,]/' , $o->ext_desc , $m ) ) $born = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
			else if ( preg_match ( '/\((\d{3,4})\s*[\)–,]/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/,\s*(\d{1,2}) (\S+?) (\d{3,4})\s*[\)–,]/' , $o->ext_desc , $m ) ) $born = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;

			if ( preg_match ( '/[,–]\s*(\d{1,2}) (\S+?) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
			else if ( preg_match ( '/–\s*(\d{3,4})\s*[,\)]/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 209:
			if ( preg_match ( '/^(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 212:
			if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 234:
			if ( preg_match ( '/born (\d{3,4}) ([a-z]+) (\d{1,2})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[3].' '.$m[2].' '.$m[1] ) ;
			else if ( preg_match ( '/born (\d{3,4});/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\d{3,4}) ([a-z]+) (\d{1,2})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[3].' '.$m[2].' '.$m[1] ) ;
			else if ( preg_match ( '/died (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $died == '2017' ) $died = '' ; # Upstream/import bug
			break ;
		
		case 239:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4}):/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-:/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;

		case 251:
			if ( preg_match ( '|geboren op (\d{1,2})/(\d{2})/(\d{4})|i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . '. ' . $m[2] . '. ' . $m[3] ) ;
			if ( preg_match ( '|gestorven op (\d{1,2})/(\d{2})/(\d{4})|i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . '. ' . $m[2] . '. ' . $m[3] ) ;
			break ;
			
		case 257:
			if ( preg_match ( '/\b(\(|parti|grundat)\b/' , $o->ext_desc ) ) continue ; // Bands, mostly
			if ( preg_match ( '/(\d{3,4})–(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/(\d{3,4})–(\d{2})\D/' , $o->ext_desc , $m ) ) {
				$born = $m[1] ;
				$died = substr($m[1],0,2) . $m[2] ;
			} else {
				if ( preg_match ( '/född (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
				else if ( preg_match ( '/född (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/död (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
				else if ( preg_match ( '/död (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;

		case 293:
			if ( preg_match ( '|born:(\d{2}\.\d{2}\.\d{4})|i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			else if ( preg_match ( '|born:(\d{4})|i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '|died:(\d{2}\.\d{2}\.\d{4})|i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			else if ( preg_match ( '|died:(\d{4})|i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 311:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 312: // Deactivated on behalf on Andrew Gray (apparently, years active, not birth/death)
/*			if ( preg_match ( '/^\((\d{3,4})-(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\(0-(\d{3,4})\)$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/^\((\d{3,4})-0\)$/' , $o->ext_desc , $m ) ) $born = $m[1] ;*/
			break ;

		case 320:
			if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/–\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 323: # using from URL desc import for full dates
#			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 344:
			if ( preg_match ( '/Birth\s*(\d{3,4}-\d{1,2}-\d{1,2})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			else if ( preg_match ( '/Birth\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Death\s*(\d{3,4}-\d{1,2}-\d{1,2})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			else if ( preg_match ( '/Death\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 360:
			if ( preg_match ( '/\(b\. ([A-Z][a-z]+ \d{1,2}, \d{4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			if ( preg_match ( '/ d\. ([A-Z][a-z]+ \d{1,2}, \d{4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			break ;

		case 362:
			if ( preg_match ( '/\((\d{1,2} [a-z]+ \d{3,4})-(\d{1,2} [a-z]+ \d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/\((\d{3,4})-(\d+ [a-z]+ \d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[1] , dp($m[2]) ] ;
			else if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[1] , $m[2] ] ;
			break ;
		
		case 407:
		case 408:
			if ( preg_match ( '/date of birth: (\d{2})\.(\d{2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[3].'-'.$m[2].'-'.$m[1] ;
			break ;
		
		case 410:
			if ( preg_match ( '/^\(\*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;
		
		case 431:
			if ( preg_match ( '/^\((\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^[ \(\d]+-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 439: // Complicated case, blame the French
			$d = preg_replace ( '/\s*Biographie\b.*$/i' , '' , $o->ext_desc ) ;
			if ( preg_match ( '/^(.*)\bMort\(e\)(.*)$/i' , $d , $m ) ) {
				$mort = $m[2] ;
				$d = $m[1] ;
				if ( preg_match ( '/(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $mort , $m ) ) $died = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
				else if ( preg_match ( '/(\d{1,2}) (\S+) (\d{3,4})/' , $mort , $m ) ) $died = dp($m[1].' '.ml($m[2]).' '.$m[3]) ;
				else if ( preg_match ( '/(\d{3,4})/' , $mort , $m ) ) $died = $m[1] ;
			}
			if ( preg_match ( '/\bNé\(e\)(.*)$/i' , $d , $m ) ) {
				$d = $m[1] ;
				if ( preg_match ( '/(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $d , $m ) ) $born = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
				else if ( preg_match ( '/(\d{1,2}) (\S+) (\d{3,4})/' , $d , $m ) ) $born = dp($m[1].' '.ml($m[2]).' '.$m[3]) ;
				else if ( preg_match ( '/(\d{3,4})/' , $d , $m ) ) $born = $m[1] ;
			}
			break ;
		
		case 442:
			if ( preg_match ( '/Né\(e\)[^;]+?(\d{1,2}) (\S+) (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[1].' '.ml($m[2]).' '.$m[3]) ;
			else if ( preg_match ( '/Né\(e\)[^;]+? (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Décédé\(e\)[^;]+?(\d{1,2}) (\S+) (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp($m[1].' '.ml($m[2]).' '.$m[3]) ;
			else if ( preg_match ( '/Décédé\(e\)[^;]+? (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 451:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)\s*$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 464:
			if ( preg_match ( '/\((\d{1,2} \S+ \d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			else if ( preg_match ( '/\((\d{1,2}-\S{3}-\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			else if ( preg_match ( '/\((\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{1,2} \S+ \d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			else if ( preg_match ( '/-\s*(\d{1,2}-\S{3}-\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			else if ( preg_match ( '/-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 467:
			if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/yearBorn"*:(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/yearDeceased"*:(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $born*1 < 1850 ) { # https://www.wikidata.org/wiki/Topic:U1x1g1ydc8nmfzg2
				$born = '' ;
				$died = '' ;
			}
			break ;

		case 495: # Some ext_desc problem, using born-died only
#			if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
#			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( preg_match ( '/^(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 506:
			if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 510:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)\s*$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 515:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;
		
		case 523:
			if ( !preg_match ( '/(before|after|established|circa|active)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{3,4})–(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
				else if ( preg_match ( '/, (\d{3,4})\)$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/–(\d{3,4})\)$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;

		case 528:
			if ( preg_match ( '/Född:(\d{3,4}[0-9-]*)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Död:(\d{3,4}[0-9-]*)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 529:
			if ( preg_match ( '/^artist \(([0-9-]+)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;
		
		case 545:
			if ( preg_match ( '/date of birth: (\d{2})\.(\d{2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[3].'-'.$m[2].'-'.$m[1] ;
			break ;

		case 547:
			if ( !preg_match ( '/(active|ca\.)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/\s(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			}
			break ;

		case 552:
			if ( preg_match ( '/ (\d{3,4})-(\d{3,4}):/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/ (\d{3,4})-:/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/ -(\d{3,4}):/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 564:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\((\d{3,4})-\)$/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;
		
		case 565:
			if ( preg_match ( '/Born:\s*(\d{1,2})\/(...)\/(\d{4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1].' '.$m[2].' '.$m[3] ) ;
			break ;
		
		case 576: // Unreliable dates, according to Sandra Fauconnier
//			if ( preg_match ( '/\(✝ (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 578:
			if ( preg_match ( '/^([0-9\.]+)–/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/–([0-9\.]+)$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;
		
		case 581:
			if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 582: // BAD DATES
//			if ( preg_match ( '/\[dates:(\d{3,4})-(\d{3,4})\]/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
//			else if ( preg_match ( '/\[dates:(\d{3,4})\]/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 602:
			if ( !preg_match ( '/active/' , $o->ext_desc , $m ) ) {
				if ( preg_match ( '/(\d{3,4})[--](\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
				if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;

		case 619:
			if ( preg_match ( '/^b\. ([0-9-]+)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/ d\. ([0-9-]+)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 627:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/, d\. (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/, d\. (\d{3,4}),/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 634:
			if ( preg_match ( '/Data de nascimento .*?: ([0-9-]+)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/Data de morte\s*([0-9-]+)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 640:
			if ( preg_match ( '/^\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\(b (\d{3,4})\)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/^\(d (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 651:
			if ( preg_match ( '/\b(c\.|s\. d\.)/i' , $o->ext_desc ) ) { // Ignore ca./before
			} else if ( preg_match ( '/(\d+\.[IVX]+\.\d{3,4})\D.+?(\d+\.[IVX]+\.\d{3,4})$/' , $o->ext_desc , $m ) ) {
				$born = dp($m[1]) ;
				$died = dp($m[2]) ;
			} else if ( preg_match ( '/\D(\d{3,4})\D.+?(\d+\.[IVX]+\.\d{3,4})$/' , $o->ext_desc , $m ) ) {
				$born = dp($m[1]) ;
				$died = dp($m[2]) ;
			} else if ( preg_match ( '/(\d+\.[IVX]+\.\d{3,4})\D.+?\D(\d{3,4})$/' , $o->ext_desc , $m ) ) {
				$born = dp($m[1]) ;
				$died = dp($m[2]) ;
			} else if ( preg_match ( '/\D(\d{3,4})\D.+\D(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
//			else if ( preg_match ( '/\D(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ; // Not sure if birth or death
			break ;

		case 655:
			if ( preg_match ( '/^\s*\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 675:
/* DEACTIVATED, BAD DATES, REPORT BY ANDY MABBETT
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\(b\. (\d{3,4})\)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\(d\. (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
*/
			break ;

		case 687:
			if ( preg_match ( '/\b(born in|né en) (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[2] ;
			else if ( preg_match ( '/\bborn on (\S+ \d{1,2}, \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\b(died in|mort en) (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[2] ;
			else if ( preg_match ( '/\b(died on|mort le) (\S+ \d{1,2}, \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp($m[2]) ;
			break ;

		case 692:
			if ( preg_match ( '/Life Dates:\s*(\d{1,2} \S+ \d{3,4})\s*[-–]\s*(\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/Life Dates:\s*(\d{3,4})\s*[-–]\s*(\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/Life Dates:\s*(\d{1,2} \S+ \d{3,4})\s*[-–]\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/Life Dates:\s*(\d{3,4})\s*[-–]\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/Life Dates:\s*\((\d{3,4})[-–](\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/Life Dates:[ b\.]*(\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/Life Dates:[ b\.]*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/Life Dates: d\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 694:
			if ( preg_match ( '/\b(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 695:
			if ( preg_match ( '/, b\. (\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/, b\. (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/, d\. (\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/, d\. (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
			
		case 698:
			if ( preg_match ( '/Né l[ea] (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/mort l[ea] (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			if ( preg_match ( '/^\(\s*(\d{3,4})\s*-\s*(\d{3,4})\s*\)/' , $o->ext_desc , $m ) ) {
				if ( $born == '' ) $born = $m[1] ;
				if ( $died == '' ) $died = $m[2] ;
			}
			if ( $born == '' and preg_match ( '/^\(\s*(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( $died == '' and preg_match ( '/^\(\s*-\s*(\d{3,4})\s*\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 714:
			if ( preg_match ( '/\bGeboren\s+(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bGestorben\s+(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 718:
			if ( preg_match ( '/\b(\d{3,4})\s*-.*?(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^[^\-]+?(\d{3,4})\s*\)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 723:
			if ( preg_match ( '/\((\d{4})-(\d{4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 732:
			if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 738:
			if ( preg_match ( '/^\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 745:
			if ( preg_match ( '/\(\*(\d\d\.\d\d.\d{4})\D/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			break ;

		case 751:
			if ( preg_match ( '/\((\d{3,4})–(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else {
				if ( preg_match ( '/\((\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})\s*–/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
				if ( preg_match ( '/–\s*(\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
				if ( $o->id == 28584504 ) print "$born/$died\n" ;
			}
			break ;

		case 768:
			if ( preg_match ( '/\bborn (\d{1,2} \S+ \d{3,4})\S/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/\bborn (\d{3,4})\S/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bdied (\d{1,2} \S+ \d{3,4})\S/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/\bdied (\d{3,4})\S/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 779: // Same as 774, which is deactivated
			if ( preg_match ( '/^\((\d{1,2} \S+ \d{3,4}) -/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/^\((\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/ - (\d{1,2} \S+ \d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/ - (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 784:
			if ( !preg_match ( '/\b(doc|act)\./' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{1,2}\.\d{1,3}\.\d{3,4})\s*[-,]/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
				else if ( preg_match ( '/(\d{3,4})\s*[-,]/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
				if ( preg_match ( '/\D+(\d{1,2}\.\d{1,3}\.\d{3,4})$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
				else if ( preg_match ( '/\D+(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			}
			break ;

		case 786:
			if ( preg_match ( '/(\d{3,4}) - (\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 794:
			if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 837:
			$d = trim ( $o->ext_desc ) ;
			if ( preg_match ( '/^[  ]*(\d{3,4})-(\d{3,4})/' , $d , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^[  ]*(\d{3,4})-\s/' , $d , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^[  ]*-(\d{3,4})/' , $d , $m ) ) $died = $m[1] ;
			break ;

		case 851:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;

		case 862:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;

		case 865:
			if ( preg_match ( '/\bborn ([^;]+)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bdied ([^;]+)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 866:
			if ( preg_match ( '/\b(né en) (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[2] ;
			else if ( preg_match ( '/\bné le (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\b(mort en) (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[2] ;
			else if ( preg_match ( '/\bmort le (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 868:
			if ( preg_match ( '/^; (\d{3,4}) ; (\d{3,4}) ;/i' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^; (\d{3,4}) ;/i' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			else if ( preg_match ( '/^; ; (\d{3,4}) ;/i' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 871:
			if ( preg_match ( '/; (\d{4})-(\d{2})-(\d{2}); (\d{4})-(\d{2})-(\d{2});$/' , $o->ext_desc , $m ) ) {
				if ( $m[2] == '00' ) $born = $m[1] ;
				else if ( $m[3] == '00' ) $born = dp($m[1].'-'.$m[2]) ;
				else $born = dp($m[1].'-'.$m[2].'-'.$m[3]) ;
				if ( $m[4] == '00' ) $died = $m[4] ;
				else if ( $m[5] == '00' ) $died = dp($m[4].'-'.$m[5]) ;
				else $died = dp($m[4].'-'.$m[5].'-'.$m[6]) ;
			} else if ( preg_match ( '/; (\d{4})-(\d{2})-(\d{2}); -;$/' , $o->ext_desc , $m ) ) {
				if ( $m[2] == '00' ) $born = $m[1] ;
				else if ( $m[3] == '00' ) $born = dp($m[1].'-'.$m[2]) ;
				else $born = dp($m[1].'-'.$m[2].'-'.$m[3]) ;
			}
			break ;

		case 874:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\((\d{3,4})-\?{0,1}\)/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			else if ( preg_match ( '/\(\?{0,1}-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 875:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 848:
			if ( preg_match ( '/\((\d{3,4})\D{1,5}(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 898:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-$/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			else if ( preg_match ( '/^-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 905:
			if ( preg_match ( '/\bgeb\. (\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1].ml($m[2]).$m[3]) ;
			else if ( preg_match ( '/\bgeb\. (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bgest\. (\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1].ml($m[2]).$m[3]) ;
			else if ( preg_match ( '/\bgest\. (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 906:
			if ( preg_match ( '/^Lebensdaten.*?\*\s*(\d{1,2}\.\d{1,2}\.\d{3,4}).*?\+\s*(\d{1,2}\.\d{1,2}\.\d{3,4}).*?Laufbahn/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			break ;

		case 913:
			if ( preg_match ( '/(\d{2}\.\d{2}\.\d{4}).*?-\s*(\d{2}\.\d{2}\.\d{4})/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			break ;

		case 917:
			if ( preg_match ( '/(\d{4}), (\d{1,2}) (\S+) -/' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . '. ' . ml($m[3]) . ' ' . $m[1] ) ;
			else if ( preg_match ( '/(\d{4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{4}), (\d{1,2}) (\S+)/' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . '. ' . ml($m[3]) . ' ' . $m[1] ) ;
			else if ( preg_match ( '/- (\d{4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 918:
			if ( preg_match ( '/(\d{4})\s*-\s*(\d{4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
			
		case 920:
			if ( preg_match ( '/^(\d{4})\s*~\s*(\d{4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 934:
			if ( preg_match ( '/\((\d{3,4})&ndash;(\d{3,4})\).*Biography/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
			
		case 948:
			if ( preg_match ( '/(\d{3,4})-/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 958:
			if ( !preg_match ( '/\b(fl\.|ca\.|c\.)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/^(\d{1,2} \S+ \d{3,4})\s*[-—]/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
				else if ( preg_match ( '/^(\d{3,4})\s*[-—]/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/^b\. (\d{1,2} \S+ \d{3,4})$/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
				if ( preg_match ( '/[-—]\s*(\d{1,2} \S+ \d{3,4})$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
				else if ( preg_match ( '/[-—]\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;

		case 978:
			if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 980:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else {
				if ( preg_match ( '/^Born:\s*(\d{4})[^\?]/' , $o->ext_desc.' ' , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/Died:\s*(\d{4})[^\?]/' , $o->ext_desc.' ' , $m ) ) $died = $m[1] ;
			}
			break ;

		case 1000:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			if ( preg_match ( '/\bb\.(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/\bd\.(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 1016:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^b\. (\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
		break ;

		case 1022:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1024:
			if ( preg_match ( '/^(\d{3,4})–(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1038:
			if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/– (\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
		break ;

		case 1041:
		case 1042:
			if ( preg_match ( '/\*\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/\*\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/†\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/†\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			if ( $born == '' and $died == '' ) {
				if ( preg_match ( '/^(\d{3,4}) bis (\d{3,4})\|/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			}
		break ;

		case 1046:
			if ( preg_match ( '/\((\d{1,2}) (\S+) (\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . ' ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/\((\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{1,2}) (\S+) (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . ' ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/- (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1056:
			if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] * 1 ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] * 1 ;
			if ( $died-$born < 30 or $died-$born>100 ) continue ; # Paranoia
		break ;

		case 1073:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1084:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1091:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\bd\.(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1101:
			if ( preg_match ( '/\bborn (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bborn in (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bborn on (\S+) (\d{1,2}), (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . '. ' . ml($m[1]) . ' ' . $m[3] ) ;
			if ( preg_match ( '/\bdied (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/\bdied in (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/\bdied on (\S+) (\d{1,2}), (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . '. ' . ml($m[1]) . ' ' . $m[3] ) ;
			if ( preg_match ( '/^[^.]+, (\d{3,4})-(\d{3,4})\./' , $o->ext_desc , $m) ) {
				if ( $born == '' ) $born = $m[1] ;
				if ( $died == '' ) $died = $m[2] ;
			}
		break ;

		case 1103:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})\s*,/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else {
				if ( preg_match ('/^\s*(\d{1,2}) (\S+) (\d{3,4})–/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
				else if ( preg_match ('/^\s*(\d{3,4})–/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ('/–\s*(\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
				else if ( preg_match ('/–\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
		break ;

		case 1139:
			if ( preg_match ( '/\((\d{3,4})–(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1140:
			if ( !preg_match ( '/circa/i' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
				else if ( preg_match ( '/(\d{4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			}
		break ;

		case 1155:
			if ( !preg_match ( '/active/i' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/born[^,0-9]+(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
		break ;

		case 1172:
			if ( preg_match ( '/\((\d{1,2}) (\S+) (\d{4}) - (\d{1,2}) (\S+) (\d{4})/' , $o->ext_desc , $m ) ) {
				 $born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
				 $died = dp ( $m[4] . '. ' . ml($m[5]) . ' ' . $m[6] ) ;
			} else if ( preg_match ( '/\((\d{4}) - (\d{4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\(b\. (\d{4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\(b\. (\d{1,2}) (\S+) (\d{4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;

		break ;

		case 1177:
			if ( preg_match ( '/\((\d{3,4})[-–](\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1187:
			if ( preg_match ( '/^(\d{1,2})\. (\S+) (\d{3,4})\D+?(\d{1,2})\. (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) {
				$born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
				$died = dp ( $m[4] . '. ' . ml($m[5]) . ' ' . $m[6] ) ;
			}
		break ;

		case 1192:
			if ( preg_match ( '/^Født (\d{1,2})\. (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/^Født (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bDød (\d{1,2})\. (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/\bDød (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1196:
			if ( preg_match ( '/\bborn (\d{1,2}) (\S+) (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
			if ( preg_match ( '/\bdied (\d{1,2}) (\S+) (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
		break ;

		case 1223:
			if ( preg_match ( '/\bborn (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1248:
			if ( preg_match ( '/\bborn ([0-9-]+)/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied ([0-9-]+)/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $died == '2000' ) $died = '' ; # Bogus value from their SPARQL API
			if ( $born == '1900' ) $born = '' ; # Bogus value from their SPARQL API
		break ;

		case 1264:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^(\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
		break ;

		case 1268:
			if ( preg_match ( '/(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1269:
			if ( preg_match ( '/(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1270:
			if ( preg_match ( '/;\s*(\d{3,4})\s*;[^;]*$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/;\s*(\d{1,2}) (\S+) (\d{3,4})\s*;[^;]*$/' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/;\s*([a-z]+) (\d{3,4})\s*;[^;]*$/i' , $o->ext_desc , $m ) ) $born = dp('00. '.ml($m[1]).' '.$m[2]) ;
			if ( preg_match ( '/;\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/;\s*(\d{1,2}) (\S+) (\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/;\s*([a-z]+) (\d{3,4})\s*$/i' , $o->ext_desc , $m ) ) $died = dp('00. '.ml($m[1]).' '.$m[2]) ;
		break ;

		case 1271:
			if ( preg_match ( '/born:([0-9-]+)/' , $o->ext_desc , $m ) ) $born = ml($m[1]) ;
			if ( preg_match ( '/died:([0-9-]+)/' , $o->ext_desc , $m ) ) $died = ml($m[1]) ;
		break ;

		case 1309:
			if ( preg_match ( '/\((\d{3,4})\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 1310:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 1313:
			if ( preg_match ( '/(\d{3,4})\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/\b(b\.|born) (\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[2] ;
		break ;

		case 1317:
			if ( preg_match ( '/\(\*(\d{3,4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/†(\d{3,4}-\d{2}-\d{2})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1321:
			if ( !preg_match ( '/(\/|ca\.|\bc\.|active)/' , $o->ext_desc , $m ) ) {
				if ( preg_match ( '/(\d{3,4})-(\d{3,4})[,;]/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
				else if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
				if ( $born != '' and $died != '' and ($died*1)-($born*1) < 30 ) list($born,$died) = ['',''] ;
			}
		break ;

		case 1325:
			if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1330:
			if ( preg_match ( '/^\((\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^\([0-9\? ]+-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1336:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^b\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^d\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1341:
			if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1351:
			if ( preg_match ( '/\bb\. (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bd\. (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1352:
			if ( preg_match ( '/^[^\[]*\[(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^[^-]*-(\d{3,4})\]/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1367:
			if ( !preg_match ( '/\b(before|after|ca|ca\.|c\.)\b/' , $o->ext_desc ) ) {
				if ( preg_match ( '/\b(b|born) [^0-9]+(\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[2].'.'.ml($m[3]).'.'.$m[4]) ;
				else if ( preg_match ( '/\b(b|born) [^0-9]+(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[2] ;
				if ( preg_match ( '/\b(d|died) [^0-9]+(\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[2].'.'.ml($m[3]).'.'.$m[4]) ;
				else if ( preg_match ( '/\b(d|died) [^0-9]+(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[2] ;
			}
		break ;

		case 1368:
			if ( preg_match ( '/-.+-/' , $o->ext_desc ) ) continue ;
			if ( preg_match ( '/\b(before|after|ca|ca\.|c\.)\b/' , $o->ext_desc ) ) continue ;

			if ( preg_match ( '/(\d{1,2})\.(\S+)\.(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{1,2})\.(\S+)\.(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/- (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1378:
			if ( preg_match ( '/\bborn (\S+) (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[2].'. '.ml($m[1]).' '.$m[3]) ;
			else if ( preg_match ( '/\bborn (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied (\S+) (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[2].'. '.ml($m[1]).' '.$m[3]) ;
			else if ( preg_match ( '/\bdied (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1379:
			if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1399:
			if ( preg_match ( '/(\d{3,4})\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/\bb\.\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bd\.\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1506:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^\s*(\d{3,4})\s*-$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1509:
			if ( preg_match ( '/^\((\d{3,4})\s*-\s*(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 1513:
			if ( preg_match ( '/-\s+(\d{3,4}-\d{2}-\d{2})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/-\s+(\d{3,4}-\d{2})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/-\s+(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( preg_match ( '/\b(\d{3,4}-\d{2}-\d{2})[0-9 \-]+$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\b(\d{3,4})[0-9 \-]+$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
		break ;

		case 1538 :
			if ( preg_match ( '/^(\S+ \d+.., \d+)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/^(\d+)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^[^-]+-\s*(\S+ \d+.., \d+)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/^[^-]+-\s*(\d+)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1619:
			if ( preg_match ( '/^(\d{3,4}) bis (\d{3,4})/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/gestorben (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1640:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4});/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-;/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^-(\d{3,4});/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1653:
			if ( preg_match ( '/, (\d{3,4})-(\d{3,4})\b/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/\bborn (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bdied (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1656:
			if ( preg_match ( '/Naissance\s*:[^\.]+, (\d{1,2})\S* (\S+) (\d{3,4})\./' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/Naissance\s*:[^\.]+, (\d{3,4})\./' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Décès\s*:[^\.]+, (\d{1,2}) (\S+)\S* (\d{3,4})\./' , $o->ext_desc , $m ) ) $died = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/Décès\s*:[^\.]+, (\d{3,4})\./' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1657:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1658:
			if ( preg_match ( '/, (\d{3,4})\s*–\s*(\d{3,4})\b/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/\bb\. *(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bd\. *(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1679:
			if ( preg_match ( '/(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1681:
			if ( preg_match ( '/\bfl\./' , $o->ext_desc ) ) continue ; # Flourit
			if ( preg_match ( '/\[\s*(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})\s*\]/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1684:
			if ( preg_match ( '/^\* (\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/^\* (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\+ (\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/\+ (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1739:
			if ( preg_match ( '/birth date: (\d{4}-\d\d-\d\d)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/death date: (\d{4}-\d\d-\d\d)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			$born = preg_replace ( '|-00-00$|' , '' , $born ) ;
			$died = preg_replace ( '|-00-00$|' , '' , $died ) ;
		break ;

		case 1759:
			if ( preg_match ( '/born (\S+?)\.{0,1} (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . ' ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/born (\d{3,4}) /' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\S+?)\.{0,1} (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . ' ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/died (\d{3,4}) /' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 1779:
			if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1814:
			if ( preg_match ( '/\((\d{3,4})\s*–\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 1819:
			if ( preg_match ( '/Born:\s*(\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . '. ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/Born:\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Died:\s*(\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . '. ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/Died:\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1827:
			if ( preg_match ( '/\((\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/-\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		default:
			die ( "Catalog $catalog not supported\n" ) ;
	}
	
	fix_date_format ( $born ) ;
	fix_date_format ( $died ) ;
	
	# Year paranoia
	if ( $born*1 == $died*1 )  continue ;
	if ( preg_match ( '/^\d+$/' , $born) and $born*1>2050 ) continue ;
	if ( preg_match ( '/^\d+$/' , $died) and $died*1>2050 ) continue ;
	if ( preg_match ( '/^(\d+)/' , $born , $m ) and preg_match ( '/^(\d+)/' , $died , $n ) ) {
		if ( $n[1]*1 - $m[1]*1 > 120 ) continue ; // Older than 120
		if ( $m[1]*1 > $n[1]*1 ) continue ; // born after death
	}

	if ( $born . $died == '' ) continue ; // No need to update
	
	// Paranoia
	if ( preg_match ( '/-00-00$/' , $born ) or preg_match ( '/-00-00$/' , $died ) ) continue ;
	if ( $born != '' and !preg_match ( '/^[0-9]+-[0-9]{1,2}-[0-9]{1,2}$/' , $born ) and !preg_match ( '/^[0-9]+-[0-9]{1,2}$/' , $born ) and !preg_match ( '/^[0-9]+$/' , $born ) ) continue ; //die ( "Bad birth: '$born' for entry {$o->id} on {$o->ext_desc}\n" ) ;
	if ( $died != '' and !preg_match ( '/^[0-9]+-[0-9]{1,2}-[0-9]{1,2}$/' , $died ) and !preg_match ( '/^[0-9]+-[0-9]{1,2}$/' , $died ) and !preg_match ( '/^[0-9]+$/' , $died ) ) continue ; //die ( "Bad death: '$died' for entry {$o->id} on {$o->ext_desc}\n" ) ;
	
	$values[] = "({$o->id},'$born','$died')" ;
}

if ( count($values) > 0 ) {
	$sql = "INSERT IGNORE INTO person_dates (entry_id,born,died) VALUES " . implode ( ',' , $values ) ;
	$mnm->getSQL ( $sql ) ;
	$sql = "UPDATE catalog SET has_person_date='yes' WHERE id=$catalog AND has_person_date!='yes'" ;
	$mnm->getSQL ( $sql ) ;
}

?>