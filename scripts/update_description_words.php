#!/usr/bin/php
<?PHP

require_once ( '/data/project/mix-n-match/scripts/mixnmatch.php' ) ;

$mnm = new MixNMatch ;

$delete_previous_matches = false ;
$update_previous_matches = false ;
if ( isset($argv[1]) ) $catalog = $argv[1]*1 ;
else $catalog = 0 ;


function getWordID ( $word , $auto_create = true ) {
	global $word2id , $mnm ;
	if ( isset($word2id[$word]) ) return $word2id[$word] ;
	if ( !$auto_create ) return 0 ;
	$sql = "INSERT IGNORE INTO words (word) VALUES ('" . $mnm->escape($word) . "')" ;
	$result = getSQL ( $mnm->dbm , $sql ) ;
	$word2id[$word] = $mnm->dbm->insert_id ;
	return $word2id[$word] ;
}

function initWords () {
	global $mnm , $word2id , $skip_words , $word_id2json ;
	$word2id = [] ;
	$skip_words = [] ;
	$word_id2json = [] ;
	$sql = "SELECT * FROM words" ;
	$result = getSQL ( $mnm->dbm , $sql ) ;
	while($o = $result->fetch_object()) {
		if ( $o->skip == 1 ) $skip_words[] = $o->word ;
		else $word2id[$o->word] = $o->id ;
		if ( isset($o->json) ) $word_id2json[$o->id] = json_decode($o->json) ;
	}
}

function addImplicationToWord ( $word , $o ) {
	global $mnm , $word_id2json ;
	$word_id = getWordID ( $word , false ) ;
	if ( $word_id == 0 ) return ;
	if ( !isset($word_id2json[$word_id]) ) { // Assume it's not in DB either...
		$word_id2json[$word_id] = [] ;
	}

	$already_exists = false ;
	foreach ( $word_id2json[$word_id] AS $k => $v ) {
		if ( json_encode($v) != json_encode($o) ) continue ;
		$already_exists = true ;
	}
	if ( $already_exists ) return ;

	$word_id2json[$word_id][] = $o ;	

	$json = json_encode ( $word_id2json[$word_id] ) ;
	$sql = "UPDATE words SET json='" . $mnm->escape($json) . "' WHERE id=$word_id" ;
	getSQL ( $mnm->dbm , $sql ) ;
}

function processCatalogs () {
	global $mnm , $word2id , $skip_words , $catalog ;
	global $delete_previous_matches , $update_previous_matches ;

	if ( $delete_previous_matches ) {
		$sql = "DELETE FROM entry2word" ;
		if ( $catalog>0 ) $sql .= " WHERE entry_id IN (SELECT id FROM entry WHERE catalog=$catalog)" ;
		getSQL ( $mnm->dbm , $sql ) ;
	}

	$sql = "SELECT id,ext_desc FROM entry WHERE ext_desc!=''" ;
	if ( !$update_previous_matches and !$delete_previous_matches ) $sql .= " AND entry.id NOT IN (SELECT entry_id FROM entry2word)" ;
	if ( $catalog>0 ) $sql .= " AND catalog=$catalog" ;
	$result = getSQL ( $mnm->dbm , $sql ) ;
	while($o = $result->fetch_object()){
		$d = $o->ext_desc ;
		$d = preg_replace ( '/[\.\,\:\;\(\{\[\]\}\)]/' , ' ' , $d ) ;
		$d = explode ( ' ' , $d ) ;
		foreach ( $d AS $part ) {
			if ( strlen ( $part ) < 2 ) continue ; // No short words
			if ( preg_match ( '/\d/' , $part ) ) continue ; // No words with numbers
			$part = strtolower ( $part ) ;
			if ( in_array ( $part , $skip_words) ) continue ; // Skip 'and','the' etc.
			$word_id = getWordID ( $part ) ;
			$sql = "INSERT IGNORE INTO entry2word (entry_id,word_id) VALUES ({$o->id},$word_id)" ;
			getSQL ( $mnm->dbm , $sql ) ;
		}
	}

	// Final cleanup
	$sql = "DELETE FROM entry2word WHERE word_id IN (SELECT id FROM words WHERE skip=1)" ;
	getSQL ( $mnm->dbm , $sql ) ;
}

function SPARQL_label2q ( $b , $key , &$array ) { // Requires $b->q to be the item
	global $skip_words ;
	if ( $b->$key->type != 'literal' ) return ; // No label
	$word = strtolower ( $b->$key->value ) ;
	if ( in_array ( $word , $skip_words ) ) return ; // Ignore word
	if ( preg_match ( '/^q\d+$/' , $word ) ) return ; // No label
	if ( preg_match ( '/ /' , $word ) ) return ; // Single words only
	$q = preg_replace ( '/^.+Q/' , 'Q' , $b->q->value ) ;
	$array[$word][$q] = $q ;
}

function matchOccupations () {
	$sparql = 'SELECT ?q ?qLabel { ?q wdt:P31 wd:Q28640 . SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } }' ;
	$j = getSPARQL ( $sparql ) ;
	$candidates = [] ;
	foreach ( $j->results->bindings AS $b ) {
		SPARQL_label2q ( $b , 'qLabel' , $candidates ) ;
	}
	foreach ( $candidates AS $word => $qs ) {
		if ( count($qs) != 1 ) continue ; // Paranoia
		$q = array_pop ( $qs ) ;
		addImplicationToWord ( $word , [ 'prop'=>'P106' , 'q'=>$q ] ) ;
	}
}

function matchNationalities () {
	$sparql = 'SELECT ?q ?qLabel ?demonym { ?q wdt:P31 wd:Q6256 ; wdt:P1549 ?demonym SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } }' ;
	$j = getSPARQL ( $sparql ) ;
	$candidates = [] ;
	foreach ( $j->results->bindings AS $b ) {
		SPARQL_label2q ( $b , 'qLabel' , $candidates ) ;
		SPARQL_label2q ( $b , 'demonym' , $candidates ) ;
	}
	foreach ( $candidates AS $word => $qs ) {
		if ( count($qs) != 1 ) continue ; // Paranoia
		$q = array_pop ( $qs ) ;
		addImplicationToWord ( $word , [ 'prop'=>'P27' , 'q'=>$q ] ) ;
	}
}

//____________________________________________________________________________________________________________

initWords() ;

//matchOccupations () ;
//matchNationalities () ;
processCatalogs () ;

?>