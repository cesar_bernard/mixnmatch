#!/usr/bin/php
<?PHP

# THIS SCRIPT UPDATES MNM-INTERNAL MATCHES TO WIKIDATA BY FOLLOWING REDIRECTS

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$redirect_cache = [] ;
function getRedirectTarget ( $q ) { // $q is Qxxx!
	global $redirect_cache ;
	if ( isset($redirect_cache[$q]) ) return $redirect_cache[$q] ;
	$url = "https://www.wikidata.org/w/api.php?action=query&prop=links&plnamespace=0&titles={$q}&format=json";
	$j = json_decode ( file_get_contents ( $url ) ) ;
	foreach ( $j->query->pages AS $page ) {
		if ( !isset($page->links) or count($page->links) != 1 ) return ;
		$ret = $page->links[0]->title ;
		$redirect_cache[$q] = $ret ;
		return $ret ;
	}
}

$mnm = new MixNMatch ;
$dbwd = $mnm->tfc->openDB ( 'wikidata' , 'wikidata' , true , true ) ;

if  ( !isset($argv[1]) ) {
	$qlist = [] ;
	$sql = "SELECT DISTINCT q FROM entry WHERE q IS NOT NULL AND q>0" ; // AND user!=0" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $qlist[] = "Q{$o->q}" ;

	$fix = [] ;
	while ( count($qlist) > 0 ) {
		$ql2 = [] ;
		while ( count($qlist) > 0 and count($ql2) < 1000 ) $ql2[] = array_pop ( $qlist ) ;
		$ql2 = '"' . implode ( '","' , $ql2 ) . '"' ;
		$sql = "SELECT page_title,(SELECT pl_title FROM pagelinks WHERE page_id=pl_from and pl_namespace=0 limit 1) AS target FROM page WHERE page_namespace=0 AND page_is_redirect=1 AND page_title IN ($ql2)" ;
		try {
			$result = $mnm->tfc->getSQL ( $dbwd , $sql ) ;
			while($o = $result->fetch_object()){
				if ( !isset($o->target) or $o->target == null or $o->target == '' ) continue ;
				$fix[$o->page_title] = $o->target ;
			}
		} catch (Exception $e) {
			continue ;
		}
	}

	foreach ( $fix AS $from => $to ) {
		$from = preg_replace ( '/\D/' , '' , "$from" ) ;
		$to = preg_replace ( '/\D/' , '' , "$to" ) ;
		$sql = "UPDATE entry SET q=$to WHERE q=$from" ;
		$mnm->getSQL ( $sql ) ;
	}
	print count($fix) . " items redirected.\n" ;

} else {

	// Get last 500 REDIRECT edits in NS 0
	$url = 'https://www.wikidata.org/w/api.php?action=query&list=recentchanges&rcshow=redirect&rcnamespace=0&rclimit=500&format=json' ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	$qs = [] ;
	foreach ( $j->query->recentchanges AS $rc ) $qs[] = $rc->title ;

	// Find those in entry table
	$sql = "SELECT * FROM entry WHERE q IN (" . preg_replace ( '/Q/' , '' , implode ( ',' , $qs ) ) . ")" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		$new_q = getRedirectTarget ( 'Q'.$o->q ) ;
		if ( !isset($new_q) ) {
#			print "Q{$o->q} is not a redirect...\n" ;
			continue ;
		}
		$sql = "UPDATE entry SET q=" . preg_replace('/\D/','',$new_q) . " WHERE id={$o->id} AND q={$o->q}" ;
		$mnm->getSQL ( $sql ) ;
	}

}

?>