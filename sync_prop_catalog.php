#!/usr/bin/php
<?PHP

// This script compares Mix'n'match catalogs in the WIkidata properties, against the properties stored in the catalogs.
// It will send me an email if it finds something funny.
// REQUIRES FILE "email"

require_once ( 'public_html/php/common.php' ) ;
require_once ( 'public_html/php/wikidata.php' ) ;
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once ( 'scripts/mixnmatch.php' ) ;
require_once ( '../listeria/Chris-G-botclasses/botclasses.php' );

function pad ( $s ) {
	if ( preg_match ( '/^P/' , $s ) ) {
		while ( strlen($s) < 5 ) $s = " $s" ;
	} else {
		while ( strlen($s) < 3 ) $s = " $s" ;
	}
	return $s ;
}

$ignore_wdprops = [1415,345,804,236,590,5257]; # 1415 temp deact for Andrew Gray (?); 345=misc IMDB series; 5257=BirdLife, different IDs

$mnm = new MixNMatch () ;

$catalogs = [] ;
$wdprop2cat = [] ;
$sql = "SELECT * FROM catalog WHERE active=1" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$catalogs[$o->id] = $o ;
	if ( isset($o->wd_prop) and !isset($o->wd_qual) ) $wdprop2cat[$o->wd_prop][] = $o->id ;
}

$props = [] ;
$sparql = 'SELECT ?q ?qLabel ?mnm { ?q rdf:type wikibase:Property . OPTIONAL { ?q wdt:P2264 ?mnm } SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } } ORDER BY ?q' ;
$sparql .= ' #!!'.rand() ; // Try to force fresh cache, not sure if that works
$j = getSPARQL ( $sparql ) ;
foreach ( $j->results->bindings AS $v ) {
	$x = [] ;
	$x['q'] = preg_replace ( '/^.+\/[PQ]/' , '' , $v->q->value ) ;
	if ( isset($props[$x['q']]) ) {
		$props[$x['q']]['catalog'][] = $v->mnm->value ;
	} else {
		$x['label'] = $v->qLabel->value ;
		if ( isset($v->mnm) and $v->mnm !== null ) $x['catalog'] = [$v->mnm->value] ;
		$props[$x['q']] = $x ;
	}
}

$out = [] ;

// Properties that have mnm id but mnm doesn't have property
foreach ( $props AS $p ) {
	if ( !isset($p['catalog']) ) continue ;
	$cats = $p['catalog'] ;
	foreach ( $cats AS $c ) {
		if ( !isset($catalogs[$c]) ) {
			$s = pad("P{$p['q']}") . " has catalog ".pad($c).", but that is inactive/does not exist" ;
			if ( isset($wdprop2cat[$p['q']]) ) $s .= "; cantidate catalogs: " . implode ( ', ' , $wdprop2cat[$p['q']] ) ;
			$out[] = $s ;
			continue ;
		}
		$cat = $catalogs[$c] ;
		if ( !isset($cat->wd_prop) or $cat->wd_prop === null or $cat->wd_prop == '' or $cat->wd_prop == 0 ) {
			if ( !in_array($p['q'],$ignore_wdprops) ) $out[] = pad("P{$p['q']}")." has catalog ".pad($c).", but that catalog does not have the property" ;
			continue ;
		}
		if ( $cat->wd_prop != $p['q'] ) {
			$out[] = pad("P{$p['q']}") . " has catalog ".pad($c).", but that catalog has property " . pad("P{$cat->wd_prop}") ;
			continue ;
		}
	}
}

// Catalogs with property, but wd does not have the statement
foreach ( $catalogs AS $c ) {
	if ( !isset($c->wd_prop) ) continue ;
	if ( isset($c->wd_qual) ) continue ;
	if ( in_array ( $c->wd_prop , $ignore_wdprops ) ) continue ;
	if ( !isset($props[$c->wd_prop]) ) {
		$out[] = "Catalog ".pad($c->id)." has property ".pad("P{$c->wd_prop}").", but no such property exists" ;
		continue ;
	}
	$p = $props[$c->wd_prop] ;
	if ( !isset($p['catalog']) ) {
		$out[] = "Catalog ".pad($c->id)." has property ".pad("P{$c->wd_prop}").", but that property does not link back to the catalog ({$c->name} / {$p['label']})" ;
		continue ;
	}
	if ( !in_array ( $c->id , $p['catalog'] ) ) {
		$out[] = "Catalog ".pad($c->id)." has property ".pad("P{$c->wd_prop}").", but that property has catalogs " . implode ( ', ' , $p['catalog'] ) ;
		continue ;
	}
	// OK
}

foreach ( $wdprop2cat AS $p => $x ) {
	if ( count($x) == 1 ) continue ;
	if ( in_array ( $p , $ignore_wdprops ) ) continue ;
	if ( isset($props[$p]) and isset($props[$p]['catalog']) AND count($props[$p]['catalog']) > 1 ) continue ; // Not perfect, but...
	$out[] = "Property " . pad("P$p") . " is used " . count($x) . " times, in catalogs " . implode ( ', ' , $x ) ;
}

if ( count($out) == 0 ) exit ( 0 ) ;

$email = trim ( file_get_contents ( '/data/project/mix-n-match/email' ) ) ;
$out = join ( "\n" , $out ) ;
mail ( $email , "sync_prop_catalog update" , $out ) ;

?>
