#!/usr/bin/php
<?PHP

$catalogs = [] ; //array ( 780 , 755 , 648 , 169 , 589 , 560 , 558 , 540 , 505 , 500 , 502 , 392 , 314 , 193 , 231 , 255 ) ;

$use_desc = [ 827 ] ;

$ranks = array (
	'variety' => 'Q767728' ,
	'subspecies' => 'Q68947' ,
	'species' => 'Q7432' ,
	'superfamily' => 'Q2136103' ,
	'subfamily' => 'Q2455704' ,
	'class' => 'Q37517' ,
	'suborder' => 'Q5867959' ,
	'genus' => 'Q34740' ,
	'family' => 'Q35409' ,
	'order' => 'Q36602' ,
	'Q16521' => '' 
) ;

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$mnm = new MixNMatch ;
$db = $mnm->dbm ;

if ( isset($argv[1]) ) { // Use single command line parameter catalog
	$catalogs = explode ( ',' , $argv[1] ) ;
} else { // Use all catalogs with an entry type "taxon"
	$sql = "select distinct catalog from entry where `type`='Q16521'" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()) $catalogs[] = $o->catalog ;
}


$sql = "SELECT * FROM entry WHERE catalog IN (" . implode(',',$catalogs) . ") AND (q IS NULL OR user=0)" ;
$sql .= " AND `type` IN ('" . implode("','",array_values($ranks)) . "Q16521')" ;
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	if ( in_array($o->catalog,$use_desc) ) $taxon = $o->ext_desc ;
	else $taxon = $o->ext_name ;

	$type = $o->type ;
	if ( !isset($ranks[$type]) ) {
		foreach ( $ranks AS $k => $v ) {
			if ( $v == $type ) $type = $k ;
		}
	}

	if ( $o->catalog == 169 ) {
		if ( preg_match ( '/\[([a-z ]+)[,\]]/i' , $o->ext_desc , $m ) ) { $taxon = $m[1] ; # First in []
//		if ( preg_match ( '/\[.+, ([a-z ]+)]/i' , $o->ext_desc , $m ) ) { $taxon = $m[1] ; # Last in []
		} else {
			$type = '' ;
			foreach ( $ranks AS $k => $v ) {
				if ( preg_match ( '/ '.$k.'$/i' , $o->ext_desc ) ) $type = $k ;
				if ( preg_match ( '/^'.$k.' of /i' , $o->ext_desc ) ) $type = $k ;
			}
			if ( $type == '' ) continue;
		}
//		print "$taxon\t$type\n" ; continue ;
	}

	$taxon = preg_replace ( '/ ssp\. /' , ' subsp. ' , $taxon ) ;
//	if ( !isset($ranks[$type]) and $type != 'Q16521' ) continue ; // Q16521 = taxon
	$rank = $ranks[$type] ;
	$t2 = $mnm->escape($taxon) ;
	$sparql = "SELECT ?q { ?q wdt:P31/wdt:P279* wd:Q16521 . { { VALUES ?prop { wdt:P225 wdt:P1420 } . ?q ?prop '$t2' } " ; # taxon name/taxon synonym
	$sparql .= "UNION { ?q skos:altLabel '$t2'@en }" ;
	if ( preg_match ( '/^(\S+) (\S+) (\S+)$/' , $taxon , $m ) ) {
		$sparql .= " UNION { ?q skos:altLabel '{$m[1]} {$m[2]} var. {$m[3]}'@en }" ;
	}
	$sparql .= " } " ;
	if ( isset($rank) AND $rank != '' ) $sparql .= ". ?q wdt:P105 wd:$rank" ;
	$sparql .= " }" ;

	$items = getSPARQLitems ( $sparql ) ;
	if ( count($items) != 1 ) continue ;
	$q = $items[0] ;
	$mnm->setMatchForEntryID ( $o->id , $q , 4 , true , false ) ;
}

?>
